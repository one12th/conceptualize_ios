//
//  BSGlobalServices.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/9/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import ObjectMapper
import Quickblox
class BSGlobalServices: NSObject {
    static let shared = BSGlobalServices()
    private var defaults = UserDefaults.standard
    
    var objCurUser: BSUserModel?
    
    var curTopic: BSTopicType? = BSTopicType.automation {
        didSet {
            if let curTopic = curTopic {
                defaults.set(curTopic.rawValue, forKey: BSUserDefaultsKey.currentTopic)
                defaults.synchronize()
            }
        }
    }
    
    var aryPendingUsers: [String]? = []
    var aryCardProfiles: [BSUserModel] = []
    var blockList: QBPrivacyList?
    
    var aryChatDialogs: [QBChatDialog] = []
    var isNeedsToLoadMore: Bool = true
    
    func loadUserDefaults() {
        if let userData = defaults.value(forKey: BSUserDefaultsKey.currentUser) as? [String: Any] {
            objCurUser = Mapper<BSUserModel>().map(JSON: userData)
            objCurUser?.id = userData[BSQuickbloxProfileClassKeyName.userId] as? UInt
            objCurUser?.objectId = userData[BSQuickbloxProfileClassKeyName.id] as? String
        }
    }
    
    func saveCurrentUser() {
        if let objCurUser = objCurUser {
            var userData = objCurUser.toJSON()
            userData[BSQuickbloxProfileClassKeyName.userId] = objCurUser.id
            userData[BSQuickbloxProfileClassKeyName.id] = objCurUser.objectId
            defaults.set(userData, forKey: BSUserDefaultsKey.currentUser)
            defaults.synchronize()
        }
    }
    
    func deleteCurrentUser() {
        defaults.removeObject(forKey: BSUserDefaultsKey.currentUser)
        defaults.synchronize()
        
        aryCardProfiles.removeAll()
        aryChatDialogs.removeAll()
        aryPendingUsers?.removeAll()
    }
    
    func removeUserFromPendingList(userId: UInt) {
        
        if let pendingUsers = BSGlobalServices.shared.aryPendingUsers {
            var index = 0
            for user in pendingUsers {
                if UInt(user) == userId {
                    break
                }
                index += 1
            }
            
            BSGlobalServices.shared.aryPendingUsers?.remove(at: index)
        }
    }
}
