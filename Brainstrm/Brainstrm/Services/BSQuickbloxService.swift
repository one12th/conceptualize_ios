//
//  BSQuickbloxService.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/11/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import Quickblox
import QMServices

class BSQuickbloxService: NSObject {

    static let shared = BSQuickbloxService()
    var blockList: QBPrivacyList?
    
    func getPendingUsers(currentUser: BSUserModel,
                         pendingUsers: [String]?,
                         completion: @escaping((_ result: Bool, _ users: [QBCOCustomObject]?, _ error: String?) -> Void)) {
        
        guard let userId = currentUser.id else {
            completion(false, nil, "Please login again.")
            return
        }
        
        let dicParams: NSMutableDictionary = [:]
        
        
        dicParams[BSQuickbloxProfileClassKeyName.userId+"[ne]"] = userId
        
        if let userTopic = currentUser.topic {
            dicParams[BSQuickbloxProfileClassKeyName.userTopic+"[eq]"] = userTopic
        }
        
        if let pendingUsers = pendingUsers, pendingUsers.count > 0 {
            dicParams[BSQuickbloxProfileClassKeyName.userId+"[in]"] = pendingUsers.joined(separator: ",")
        }
        
        if let swiped = currentUser.swiped, swiped.count > 0 {
            let swipedString = swiped.map({return "\($0)"})
            dicParams[BSQuickbloxProfileClassKeyName.userId+"[nin]"] = swipedString.joined(separator: ",")
        }
        
        QBRequest.objects(withClassName: BSQuickbloxClassName.profile,
                          extendedRequest: dicParams,
                          successBlock: { (response, aryUsers, responsePage) in
                            
                            if let aryUsers = aryUsers {
                                completion(true, aryUsers, nil)
                                return
                            }
                            
                            completion(false, nil, response.getErrorMessage())
                            
        }) { (response) in
            completion(false, nil, response.getErrorMessage())
        }
    }
    
    
    func reportUser(reportUserId: UInt, completion: @escaping((_ result: Bool, _ error: String?) -> Void)) {
        if reportUserId == 0 {
            completion(false, "Please select a user who you want to report.")
            return
        }
        
        let reportUser = QBCOCustomObject()
        reportUser.className = BSQuickbloxClassName.reports
        reportUser.fields.setValue(reportUserId, forKey: BSQuickbloxReportsClassKeyName.reportedUser)
        
        QBRequest.createObject(reportUser,
                               successBlock: { (response, object) in
                                completion(response.isSuccess, response.getErrorMessage())
        }) { (response) in
            completion(false, response.getErrorMessage())
        }
    }
    
    func deleteAccount(userId: UInt, userObjectId: String, completion: @escaping((_ result: Bool, _ error: String?) -> Void)) {
        if userId == 0 {
            completion(false, "Please try again.")
            return
        }
        
        // Get all chat dialogs
        QMServicesManager.instance().chatService.allDialogs(withPageLimit: 0, extendedRequest: [:]) { (response, aryDialogs, participants, result) in
            if response.isSuccess, let aryDialogs = aryDialogs {
                
                var dialogIds: Set<String> = []
                for dialog in aryDialogs {
                    
                    // Block user
                    if let occupants = dialog.occupantIDs {
                        var opponentId: UInt = 0
                        for occupant in occupants {
                            if occupant.uintValue != QMServicesManager.instance().currentUser.id {
                                opponentId = occupant.uintValue
                                break
                            }
                        }
                        
                        if opponentId == 0 {
                            completion(false, "You can't delete the dialog")
                            return
                        }
                        
                        self.blockUser(userId: opponentId)
                    }
                    
                    if let dialogId = dialog.id {
                        dialogIds.insert(dialogId)
                    }
                }
                
                // Delete dialogs from quickblox
                if dialogIds.count > 0 {
                    QBRequest.deleteDialogs(withIDs: dialogIds,
                                            forAllUsers: true,
                                            successBlock: { (response, deletedIds, notFoundIds, wrongPermissionIds) in
                                                if response.isSuccess {
                                                    
                                                    // Delete profile data.
                                                    QBRequest.deleteObject(withID: userObjectId,
                                                                           className: BSQuickbloxClassName.profile,
                                                                           successBlock: { (response) in
                                                                            if response.isSuccess {
                                                                                
                                                                                // Delete current user.
                                                                                QBRequest.deleteCurrentUser(successBlock: { (response) in
                                                                                    completion(response.isSuccess, response.getErrorMessage())
                                                                                }) { (response) in
                                                                                    completion(false, response.getErrorMessage())
                                                                                }
                                                                                
                                                                            } else {
                                                                                print(response.getErrorMessage())
                                                                                completion(false, "Please try again.")
                                                                            }
                                                    }) { (response) in
                                                        print(response.getErrorMessage())
                                                        completion(false, "Please try again.")
                                                    }
                                                }
                    }, errorBlock: { (response) in
                        print(response.getErrorMessage())
                        completion(false, "Please try again.")
                    })
                } else {
                    // Delete profile data.
                    QBRequest.deleteObject(withID: userObjectId,
                                           className: BSQuickbloxClassName.profile,
                                           successBlock: { (response) in
                                            if response.isSuccess {
                                                
                                                // Delete current user.
                                                QBRequest.deleteCurrentUser(successBlock: { (response) in
                                                    completion(response.isSuccess, response.getErrorMessage())
                                                }) { (response) in
                                                    completion(false, response.getErrorMessage())
                                                }
                                                
                                            } else {
                                                print(response.getErrorMessage())
                                                completion(false, "Please try again.")
                                            }
                    }) { (response) in
                        print(response.getErrorMessage())
                        completion(false, "Please try again.")
                    }
                }
            }
        }
    }
    
    func updateProfile(completion: @escaping((_ result: Bool, _ profileData: QBCOCustomObject?, _ error: String?) -> Void)) {
        
        guard let objProfile = BSGlobalServices.shared.objCurUser?.getQuickbloxObject() else {
            completion(false, nil, "Please try again.")
            return
        }
        
        QBRequest.update(objProfile,
                         specialUpdateOperators: NSMutableDictionary(),
                         successBlock: { (response, profileData) in
            
                            completion(response.isSuccess, profileData, response.getErrorMessage())
        }) { (response) in
            completion(false, nil, response.getErrorMessage())
        }
        
    }
    
    func updatePassword(oldPassword: String, newPassword: String, completion: @escaping((_ result: Bool, _ error: String?) -> Void)) {
        
        let params = QBUpdateUserParameters()
        params.password = newPassword
        params.oldPassword = oldPassword
        QBRequest.updateCurrentUser(params, successBlock: { (response, user) in
            completion(response.isSuccess, response.getErrorMessage())
        }) { (response) in
            completion(false, response.getErrorMessage())
        }
    }
    
    func getMessages(skip: Int, limit: Int, completion: @escaping((_ result: Bool, _ needsLoadMore: Bool, _ chatDialogs: [QBChatDialog]?, _ participants: Set<NSNumber>?) -> Void)) {
        if QBChat.instance.isConnected == false {
            print("not connected")
            return
        }
        
        var request: [String: Any] = [:]
        request["sort_desc"] = "date_sent"
        request["skip"] = skip
        
        QMServicesManager.instance().chatService.allDialogs(withPageLimit: 20,
                                                            extendedRequest: request,
                                                            iterationBlock:
            { (response, aryChatDialogs, aryParticipants, result) in
                if response.isSuccess, let aryChatDialogs = aryChatDialogs {
                    
                    let needsLoadMore: Bool = aryChatDialogs.count >= limit
                    
                    completion(true, needsLoadMore, aryChatDialogs, aryParticipants)
                } else {
                    completion(false, false, nil, nil)
                }
        }) { (response) in
            print(response.getErrorMessage())
            completion(false, false, nil, nil)
        }
    }
    
    func getUnreadMessagesCount(dialogIds: Set<String>, completion: @escaping((_ result: Bool, _ count: UInt) -> Void)) {
        
        QBRequest.totalUnreadMessageCountForDialogs(withIDs: dialogIds, successBlock: { (response, unreadCounts, dialogs) in
            
            if response.isSuccess {
                completion(true, unreadCounts)
            } else {
                completion(false, 0)
            }
            
        }) { (response) in
            print(response.getErrorMessage())
            completion(false, 0)
        }
    }
    
    func blockUser(userId: UInt) {
        
        if BSGlobalServices.shared.blockList == nil {
            BSGlobalServices.shared.blockList = QBPrivacyList(name: BSQuickbloxInfo.privacyListForBlockUsers)
        }
        
        let item = QBPrivacyItem(privacyType: QBPrivacyType.userID, userID: userId, allow: false)
        item.mutualBlock = true
        BSGlobalServices.shared.blockList?.addObject(item)
        if let blockList = BSGlobalServices.shared.blockList {
            QBChat.instance.setPrivacyList(blockList)
        }
    }
    
    func deleteDialog(dialog: QBChatDialog, completion: @escaping((_ result: Bool, _ error: String?) -> Void)) {
        
        guard let dialogId = dialog.id, let occupants = dialog.occupantIDs else {
            completion(false, "You can't delete the dialog")
            return
        }
        
        var opponentId: UInt = 0
        for occupant in occupants {
            if occupant.uintValue != QMServicesManager.instance().currentUser.id {
                opponentId = occupant.uintValue
                break
            }
        }
        
        if opponentId == 0 {
            completion(false, "You can't delete the dialog")
            return
        }
        
        blockUser(userId: opponentId)
        QMServicesManager.instance().chatService.deleteDialog(withID: dialogId, completion: { (resposne) in
            if resposne.isSuccess {
                completion(true, resposne.getErrorMessage())
            } else {
                completion(false, resposne.getErrorMessage())
            }
        })
    }
}
