//
//  BSChatAttachmentIncomingCollectionViewCell.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/13/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import QMChatViewController

class BSChatAttachmentIncomingCollectionViewCell: QMChatAttachmentIncomingCell {
    static var nib: UINib {
        return UINib(nibName: String(describing: BSChatAttachmentIncomingCollectionViewCell.self), bundle: nil)
    }
    
    override class func layoutModel() -> QMChatCellLayoutModel {
        var defaultLayoutModel = super.layoutModel()
        defaultLayoutModel.avatarSize = CGSize.zero
        defaultLayoutModel.containerInsets = UIEdgeInsets.init(top: 10, left: 5, bottom: 5, right: 10)
        defaultLayoutModel.topLabelHeight = 0
        defaultLayoutModel.spaceBetweenTextViewAndBottomLabel = 0
        defaultLayoutModel.bottomLabelHeight = 14
        return defaultLayoutModel;
    }
}
