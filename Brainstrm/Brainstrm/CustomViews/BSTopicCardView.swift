//
//  BSTopicCardView.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/11/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit

class BSTopicCardView: UIView {

    // MARK: - Properties
    @IBOutlet weak var imgTopicPicture: UIImageView!
    @IBOutlet weak var lblTopicName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    // MARK: - Lifecycle
    
    // MARK: - Actions
    
    // MARK: - Functions
    func initWithData(userData: BSUserModel) {
        lblTopicName.text = userData.topic
        lblDescription.text = userData.bio
        
        var index = 0
        for topic in BSTopicType.names {
            if let userTopic = userData.topic, userTopic.compare(topic) == ComparisonResult.orderedSame {
                break
            }
            index += 1
        }
        
        imgTopicPicture.image = UIImage(named: BSTopicType.imageNames[index % BSTopicType.names.count])
        self.layer.cornerRadius = 5
    }
}
