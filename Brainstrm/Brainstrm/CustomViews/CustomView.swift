//
//  CustomView.swift
//
//  Created by jcooperation0137 on 11/15/16.
//  Copyright © 2016 jcooperation0137. All rights reserved.
//

import UIKit

@IBDesignable class CustomView: UIView {
    
    
    @IBInspectable var shadowColor: CGColor = UIColor.black.cgColor {
        didSet {
            drawShadow()
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0.5 {
        didSet {
            drawShadow()
        }
    }
    
    @IBInspectable var shadowOffset: CGSize = CGSize(width: 0, height: 1) {
        didSet {
            drawShadow()
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0 {
        didSet {
            drawShadow()
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    func drawShadow() {
        layer.masksToBounds = false
        layer.shadowColor = shadowColor
        layer.shadowOpacity = shadowOpacity
        layer.shadowOffset = shadowOffset
        layer.shadowRadius = shadowRadius
    }
}
