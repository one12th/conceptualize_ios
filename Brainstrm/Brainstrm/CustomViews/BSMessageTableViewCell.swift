//
//  BSMessageTableViewCell.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/13/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import Quickblox
class BSMessageTableViewCell: UITableViewCell {

    // MARK: - Properties
    @IBOutlet weak var imgAvatar: CustomImage!
    @IBOutlet weak var lblTopicName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblCreatedAt: UILabel!
    @IBOutlet weak var viewBadge: CustomView!
    @IBOutlet weak var lblBadge: UILabel!
    
    var objChatDialog: QBChatDialog?
    
    static var reuseIdentifier: String {
        return String(describing: BSMessageTableViewCell.self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: BSMessageTableViewCell.self), bundle: nil)
    }
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Actions
    
    // MARK: - Functions.
    func initViewWithData(chatDialog: QBChatDialog) {
        objChatDialog = chatDialog
        lblTopicName.text = chatDialog.name
        if let lastMessage = chatDialog.lastMessageText {
            lblMessage.text = lastMessage
            lblMessage.isHidden = false
        } else {
            lblMessage.isHidden = true
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        if let createdAt = chatDialog.lastMessageDate {
            lblCreatedAt.text = dateFormatter.string(from: createdAt)
            lblCreatedAt.isHidden = true
        } else {
            lblCreatedAt.isHidden = true
        }
        
        if chatDialog.unreadMessagesCount > 0 {
            viewBadge.isHidden = false
            lblBadge.text = "\(chatDialog.unreadMessagesCount)"
        } else {
            viewBadge.isHidden = true
        }
    }
}
