//
//  BSChatIncomingCollectionViewCell.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/13/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import QMChatViewController

class BSChatIncomingCollectionViewCell: QMChatIncomingCell {
    
    static var nib: UINib {
        return UINib(nibName: String(describing: BSChatIncomingCollectionViewCell.self), bundle: nil)
    }
    
    override class func layoutModel() -> QMChatCellLayoutModel {
        var defaultLayoutModel = super.layoutModel()
        defaultLayoutModel.avatarSize = CGSize.zero
        defaultLayoutModel.containerInsets = UIEdgeInsets.init(top: 15, left: 20, bottom: 10, right: 20)
        defaultLayoutModel.topLabelHeight = 0
        defaultLayoutModel.spaceBetweenTextViewAndBottomLabel = 0
        defaultLayoutModel.bottomLabelHeight = 14
        return defaultLayoutModel;
    }
}
