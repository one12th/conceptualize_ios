//
//  BSToolbarContentView.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/15/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import QMChatViewController

class BSToolbarContentView: QMToolbarContentView {
    
    @IBOutlet weak var attachmentButtonContainer: UIView!
    @IBOutlet weak var attachmentButton: UIButton!
    
    var didClickBtnAttachementCompletionHandler: (() -> ())?
    
    @IBAction func onClickBtnAttachment(_ sender: Any) {
        if let didClickBtnAttachementCompletionHandler = didClickBtnAttachementCompletionHandler {
            didClickBtnAttachementCompletionHandler()
        }
    }
    
    static var nib: String {
        return String(describing: BSToolbarContentView.self)
    }
}
