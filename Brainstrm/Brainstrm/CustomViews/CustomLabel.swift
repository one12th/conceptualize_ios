//
//  CustomLabel.swift
//  Plunk
//
//  Created by jcooperation0137 20/02/2018.
//  Copyright © 2018 jcooperation0137. All rights reserved.
//

import UIKit

@IBDesignable class CustomLabel: UILabel {
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.masksToBounds = true
    }
}
