//
//  CustomImage.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/8/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit

@IBDesignable class CustomImage: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            draw(frame)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            draw(frame)
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            draw(frame)
        }
    }
    
    @IBInspectable var overlayColor: UIColor = UIColor.clear {
        didSet {
            draw(frame)
        }
    }
    
//    @IBInspectable var shadownColor: UIColor = UIColor.clear {
//        didSet {
//            draw(frame)
//        }
//    }
//
//    @IBInspectable var shadowOffset: CGSize = CGSize(width: 0, height: 1) {
//        didSet {
//            draw(frame)
//        }
//    }
//
//    @IBInspectable var shadowOpacity: CGFloat {
//        didSet {
//            draw(frame)
//        }
//    }
//
//    @IBInspectable var shadowRadius: CGFloat {
//        didSet {
//            draw(frame)
//        }
//    }
    
    override func draw(_ rect: CGRect) {
        layer.masksToBounds = true
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        
        if overlayColor != UIColor.clear {
            self.image = self.image!.withRenderingMode(.alwaysTemplate)
            self.tintColor = overlayColor
        }
    }
}
