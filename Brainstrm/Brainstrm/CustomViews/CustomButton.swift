//
//  CustomButton.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/8/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit

@IBDesignable class CustomButton: UIButton {

    var startLoading = false {
        didSet {
            if startLoading {
                bringSubview(toFront: viewOverlay)
                viewOverlay.isHidden = false
                idcLoading.startAnimating()
            } else {
                sendSubview(toBack: viewOverlay)
                viewOverlay.isHidden = true
                idcLoading.stopAnimating()
            }
        }
    }
    
    private var viewOverlay: UIView!
    private var idcLoading: UIActivityIndicatorView!
    
    @IBInspectable var verticalAlign: Bool = false {
        didSet {
            setLayout()
        }
    }
    
    @IBInspectable var verticalSpace: CGFloat = 6.0 {
        didSet {
            setLayout()
        }
    }
    
    override var backgroundColor: UIColor? {
        didSet {
            if nil != viewOverlay {
                viewOverlay.backgroundColor = backgroundColor?.withAlphaComponent(1)
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    func setLayout () {
        if verticalAlign {
            let imageSize = imageView?.frame.size
            let titleSize = titleLabel?.frame.size
            let totalHeight = ((imageSize?.height)! + (titleSize?.height)! + verticalSpace);
            
            imageEdgeInsets = UIEdgeInsetsMake(-(totalHeight - (imageSize?.height)!),
                                                    0.0,
                                                    0.0,
                                                    -(titleSize?.width)!)
            
            titleEdgeInsets = UIEdgeInsetsMake(0.0,
                                                    -(imageSize?.width)!,
                                                    -(totalHeight - (titleSize?.height)!),
                                                    0.0)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.masksToBounds = true

        if nil == viewOverlay {
            viewOverlay = UIView(frame: .zero)
            viewOverlay.isHidden = true
            viewOverlay.backgroundColor = backgroundColor
            addSubview(viewOverlay)
            viewOverlay.translatesAutoresizingMaskIntoConstraints = false
            addConstraint(NSLayoutConstraint(item: viewOverlay, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0.0))
            addConstraint(NSLayoutConstraint(item: viewOverlay, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0.0))
            addConstraint(NSLayoutConstraint(item: viewOverlay, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1.0, constant: 0.0))
            addConstraint(NSLayoutConstraint(item: viewOverlay, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1.0, constant: 0.0))
        }
        
        if nil == idcLoading {
            idcLoading = UIActivityIndicatorView()
            idcLoading.activityIndicatorViewStyle = .white
            idcLoading.hidesWhenStopped = true
            idcLoading.isUserInteractionEnabled = false
            viewOverlay.addSubview(idcLoading)
            idcLoading.translatesAutoresizingMaskIntoConstraints = false
            addConstraint(NSLayoutConstraint(item: idcLoading, attribute: .centerY, relatedBy: .equal, toItem: viewOverlay, attribute: .centerY, multiplier: 1.0, constant: 0.0))
            addConstraint(NSLayoutConstraint(item: idcLoading, attribute: .centerX, relatedBy: .equal, toItem: viewOverlay, attribute: .centerX, multiplier: 1.0, constant: 0.0))
        }
    }

}
