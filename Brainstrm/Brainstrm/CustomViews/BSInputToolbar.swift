//
//  BSInputToolbar.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/15/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import QMChatViewController

class BSInputToolbar: QMInputToolbar {

    
    
    override func loadContentView() -> QMToolbarContentView {
        return Bundle.main.loadNibNamed(BSToolbarContentView.nib, owner: self, options: [:])?.first as! QMToolbarContentView
    }

}
