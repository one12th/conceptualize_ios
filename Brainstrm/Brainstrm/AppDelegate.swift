//
//  AppDelegate.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/8/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import Quickblox
import FBSDKCoreKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // 1. Quickblox setting.
        QBSettings.applicationID = BSQuickbloxInfo.applicationId
        QBSettings.authKey = BSQuickbloxInfo.authKey
        QBSettings.authSecret = BSQuickbloxInfo.authSecret
        QBSettings.accountKey = BSQuickbloxInfo.accountKey
        QBSettings.autoReconnectEnabled = true
        
        // 2. Facebook setting.
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // 3. Load userdefaults.
        BSGlobalServices.shared.loadUserDefaults()
        
        // 4. Set the first screen.
        if QBSession.current.currentUserID > 0 {
            goToMainScreen()
        }
        
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString
        
        let subscription = QBMSubscription()
        subscription.notificationChannel = QBMNotificationChannel.APNS
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = deviceToken
        
        QBRequest.createSubscription(subscription, successBlock: { (response, arySubscriptions) in
            if response.isSuccess {
                print("Successfully subscribed the notification")
            } else {
                print(response.getErrorMessage())
            }
        }) { (response) in
            print(response.getErrorMessage())
        }
    }
}

extension AppDelegate {
    func goToMainScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let mainNC: UINavigationController = storyboard.instantiateViewController(withIdentifier: "BSHomeNavigationController") as! UINavigationController
        if let rootViewController = window?.rootViewController, rootViewController.isKind(of: BSHomeNavigationController.self) == false{
            window?.rootViewController = mainNC
        }
    }
    
    func goToLoginScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let authNC: UINavigationController = storyboard.instantiateViewController(withIdentifier: "BSAuthNavigationController") as! UINavigationController
        
        if let rootViewController = window?.rootViewController, rootViewController.isKind(of: BSHomeNavigationController.self) {
            window?.rootViewController = authNC
        }
    }
    
    func registerForRemoteNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in
                guard granted else { return }
                
            }
            UIApplication.shared.registerForRemoteNotifications()
        }
        else {
            // iOS 8, 9 support
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
}

