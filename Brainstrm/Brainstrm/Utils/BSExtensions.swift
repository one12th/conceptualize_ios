//
//  BSExtensions.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/9/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftMessages
import QMServices

extension UIViewController {
    // MARK: - Public functions
    func showWaitHUD() {
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show(withStatus: "Please wait...")
    }
    
    func showMessage(_ message: String, messageType: BSMessageType = .info) {
        switch messageType {
        case .success:
            SVProgressHUD.showSuccess(withStatus: message)
        case .info:
            SVProgressHUD.showInfo(withStatus: message)
        case .error:
            SVProgressHUD.showError(withStatus: message)
        }
    }
    
    func dismissHUD() {
        SVProgressHUD.dismiss()
    }
    
    func showErrorMessage(title: String, body: String) {
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureContent(title: title, body: body)
        view.configureTheme(.error, iconStyle: .light)
        view.configureDropShadow()
        view.button?.isHidden = true
        
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        config.duration = .seconds(seconds: 3)
        
        SwiftMessages.show(config: config, view: view)
    }
    
    func showSuccessMessage(title: String, body: String) {
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureContent(title: title, body: body)
        view.configureTheme(.success, iconStyle: .light)
        view.configureDropShadow()
        view.button?.isHidden = true
        
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        config.duration = .seconds(seconds: 1.5)
        
        SwiftMessages.show(config: config, view: view)
    }
    
    func showAppStatus(message: String) {
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.id = BSAppStatusMessageId
        view.configureContent(body: message)
        view.configureTheme(.error)
        view.button?.isHidden = true
        
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        config.duration = .forever
        
        SwiftMessages.show(config: config, view: view)
    }
    
    func hideAppStatus() {
        SwiftMessages.hide(id: BSAppStatusMessageId)
    }
    
    func logout() {
        
        QMServicesManager.instance().logout {
            // TODO: Remove all delegate of chat service and auth service
            QBChat.instance.removeAllDelegates()
            
            // Remove current user from user defaults.
            BSGlobalServices.shared.deleteCurrentUser()
        }
    }
}

extension UIColor {
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        var hexString = hex
        let markIndex = hex.index(hex.startIndex, offsetBy: 2)
        if "0x" == String(hex.prefix(upTo: markIndex)).lowercased() {
            hexString = String(hexString.suffix(from: markIndex))
        }
        
        let scanner = Scanner(string: hexString)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff,
            alpha: alpha
        )
    }
    
    static let ttColorOrange = UIColor(hex: "0xaa80c0")
    static let ttColorTabOriginal = UIColor(hex: "0x96c080")
    static let ttColorTabSelected = UIColor(hex: "0x88b173")
    static let ttColorPurple = UIColor(hex: "0xaa80c0")
    static let ttColorGray = UIColor(hex: "0x8b8d8d")
    static let ttColorBlueDark = UIColor(hex: "0x2e5eb4")
}

extension QBResponse {
    func getErrorMessage() -> String {
        /*
        if let dicError = self.error?.reasons?.first?.value as? [String: Any],
            let error = dicError.first,
            let message = error.value as? [String] {
            return "\(error.key) \(message.first ?? "")"
        }*/
        return "Please try again"
    }
}

extension UIView {
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true, cornerRadius: CGFloat = 0) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.cornerRadius = cornerRadius
        
        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
