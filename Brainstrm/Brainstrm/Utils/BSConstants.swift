//
//  BSConstants.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/9/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit

struct BSQuickbloxInfo {
    static let applicationId: UInt = 69633
    static let authKey = "LpaNAR5-G2Fa8Ep"
    static let authSecret = "sSRxpOb-yvaLjpb"
    static let accountKey = "ypXLH9zN1xcTPhzMpcLP"
    
    static let privacyListForBlockUsers = "public"
}

struct BSQuickbloxClassName {
    static let profile = "profile"
    static let reports = "reports"
}

let BSAppStatusMessageId = "BSAppStatusMessageId"

struct BSApplicationInfo {
    static let name = "Brainstrm"
}

struct BSUserDefaultsKey {
    static let currentUser = "bs_currentUser"
    static let currentTopic = "bs_currentTopic"
}

struct BSQuickbloxProfileClassKeyName {
    static let id               = "_id"
    static let userId           = "user_id"
    static let name             = "name"
    static let age              = "age"
    static let bio              = "bio"
    static let userTopic        = "userTopic"
    static let gender           = "gender"
    static let images           = "images"
    static let likeLimitLifted  = "likeLimitLifted"
    static let liked            = "liked"
    static let location         = "location"
    static let matched          = "matched"
    static let pending          = "pending"
    static let swiped           = "swiped"
    static let createdAt        = "created_at"
    static let updatedAt        = "updated_at"
}

struct BSQuickbloxReportsClassKeyName {
    static let reportedUser     = "reported_user"
}
