//
//  BSEnumeraions.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/9/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit

enum BSMessageType {
    case success
    case error
    case info
}

enum BSTopicType: String {
    case automation = "Automation"
    case ai = "AI"
    case predictiveAnalytics = "Predictive Analytics"
    case naturalLanguageProcessing = "Natural language processing / Voice Control"
    case augmentedVirtualReality = "Augmented & Virtual Reality"
    case blockchain = "Blockchain"
    
    static let names = [
        BSTopicType.automation.rawValue,
        BSTopicType.ai.rawValue,
        BSTopicType.predictiveAnalytics.rawValue,
        BSTopicType.naturalLanguageProcessing.rawValue,
        BSTopicType.augmentedVirtualReality.rawValue,
        BSTopicType.blockchain.rawValue
    ]
    
    static let imageNames = [
        "ic_automation",
        "ic_ai",
        "ic_analysis",
        "ic_voice_control",
        "ic_ar_vr",
        "ic_blockchain"
    ]
}
