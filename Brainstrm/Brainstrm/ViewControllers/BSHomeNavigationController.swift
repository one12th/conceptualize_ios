//
//  BSHomeNavigationController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/10/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import LNSideMenu

class BSHomeNavigationController: LNSideMenuNavigationController {

    // MARK: - Properties
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Side menu configuration.
        if let leftSideMenuVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BSLeftSideMenuViewController.self)) {
            sideMenu = LNSideMenu(navigation: self, menuPosition: .left, customSideMenu: leftSideMenuVC)
            sideMenu?.enableDynamic = false
            sideMenu?.isNavbarHiddenOrTransparent = true
            sideMenu?.allowPanGesture = false
            sideMenu?.allowLeftSwipe = false
            sideMenu?.allowRightSwipe = false
            
            sideMenuAnimationType = .none
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    
    // MARK: - Functions

}
