//
//  BSEditProfileViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/12/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit

class BSEditProfileViewController: UIViewController {

    // MARK: - Properites
    @IBOutlet weak var txtBio: UITextView!
    var didClickBtnSaveCompletionHandler: (() -> Void)?
    var gestureDismiss: UITapGestureRecognizer!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let bio = BSGlobalServices.shared.objCurUser?.bio {
            txtBio.text = bio
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        gestureDismiss = UITapGestureRecognizer(target: self, action: #selector(onTapView(_:)))
        self.view.addGestureRecognizer(gestureDismiss)
        self.view.isUserInteractionEnabled = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        view.removeGestureRecognizer(gestureDismiss)
    }
    
    // MARK: - Actions
    @objc
    func onTapView(_ sender: Any) {
        view.endEditing(true)
    }
    
    // MARK: - Functions
    @IBAction func onClickBtnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickBtnSave(_ sender: Any) {
        view.endEditing(true)
        
        if let bio = txtBio.text {
            BSGlobalServices.shared.objCurUser?.bio = bio
        }
        
        showWaitHUD()
        BSQuickbloxService.shared.updateProfile { (success, profileData, error) in
            self.dismissHUD()
            if success {
                if let profileData = profileData {
                    BSGlobalServices.shared.objCurUser = BSUserModel(customData: profileData)
                    BSGlobalServices.shared.saveCurrentUser()
                    if let didClickBtnSaveCompletionHandler = self.didClickBtnSaveCompletionHandler {
                        didClickBtnSaveCompletionHandler()
                    }
                }
                self.showSuccessMessage(title: BSApplicationInfo.name, body: "Successfully updated.")
            } else {
                if let error = error {
                    print(error)
                    self.showErrorMessage(title: BSApplicationInfo.name, body: "Please try again")
                }
            }
        }
    }
}
