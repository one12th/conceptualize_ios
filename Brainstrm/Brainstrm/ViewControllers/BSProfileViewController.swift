//
//  BSProfileViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/12/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit

class BSProfileViewController: UIViewController {

    // MARK: - Properites
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var imgUserAvatar: CustomImage!
    @IBOutlet weak var lblTopic: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblBio: UILabel!
    @IBOutlet weak var btnReport: CustomButton!
    @IBOutlet weak var viewSettingPopover: UIView!
    @IBOutlet weak var viewMask: UIView!
    
    var objUser: BSUserModel?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUserInformation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @objc
    func onTapViewMask(_ sender: Any) {
        viewMask.isHidden = true
        viewSettingPopover.isHidden = true
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickBtnSetting(_ sender: Any) {
        viewMask.isHidden = false
        viewSettingPopover.isHidden = false
    }
    
    @IBAction func onClickBtnReport(_ sender: Any) {
        
        guard let userId = objUser?.id else {
            showErrorMessage(title: BSApplicationInfo.name, body: "Sorry, the user information is incorrect.")
            return
        }
        
        let alertController = UIAlertController(title: "Report the user",
                                                message: "Reporting this user will also block them from contacting you",
                                                preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes", style: .default) { (_) in
            
            UIView.animate(withDuration: 0,
                           animations: {
                alertController.dismiss(animated: true)
            }, completion: { (completed) in
                if completed {
                    self.showWaitHUD()
                    BSQuickbloxService.shared.reportUser(reportUserId: userId,
                                                         completion: { (success, error) in
                                                            self.dismissHUD()
                                                            if success == true {
                                                                self.showSuccessMessage(title: "User reported", body: "Thank you for your report")
                                                            } else if let error = error {
                                                                print(error)
                                                                self.showErrorMessage(title: BSApplicationInfo.name, body: "Please try again")
                                                            }
                    })
                }
            })
        }
        
        let noAction = UIAlertAction(title: "No", style: .default) { (_) in
            alertController.dismiss(animated: true)
        }
        
        alertController.addAction(okAction)
        alertController.addAction(noAction)
        present(alertController, animated: true)
    }
    
    @IBAction func onClickBtnEditProfile(_ sender: Any) {
        onTapViewMask(sender)
        
        if let editProfileVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BSEditProfileViewController.self)) as? BSEditProfileViewController {
            editProfileVC.didClickBtnSaveCompletionHandler = {
                self.objUser = BSGlobalServices.shared.objCurUser
                self.setUserInformation()
            }
            navigationController?.pushViewController(editProfileVC, animated: true)
        }
    }
    
    @IBAction func onClickBtnChangePassword(_ sender: Any) {
        onTapViewMask(sender)
        
        if let changePasswordVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BSChangePasswordViewController.self)) {
            navigationController?.pushViewController(changePasswordVC, animated: true)
        }
    }
    
    // MARK: - Functions
    func setUserInformation() {
        if let topic = objUser?.topic {
            lblTopic.text = topic
            lblTopic.isHidden = false
        } else {
            lblTopic.isHidden = true
        }
        
        if let bio = objUser?.bio {
            lblBio.text = bio
            lblBio.isHidden = false
        } else {
            lblBio.isHidden = true
        }
        
        viewSettingPopover.isHidden = true
        viewMask.isHidden = true
        
        let gestureReg = UITapGestureRecognizer(target: self, action: #selector(onTapViewMask(_:)))
        viewMask.addGestureRecognizer(gestureReg)
        
        if objUser?.id == BSGlobalServices.shared.objCurUser?.id {
            btnReport.isHidden = true
            btnSetting.isHidden = false
        } else {
            btnReport.isHidden = false
            btnSetting.isHidden = true
        }
    }
}
