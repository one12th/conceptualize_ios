//
//  BSLoginViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/9/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import QMServices
import FacebookCore
import FacebookLogin

class BSLoginViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func onClickBtnSigninWithFacebook(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile, .userFriends, .userBirthday],
                           viewController: self) { (loginResult) in
                            switch loginResult {
                            case .success(_ , _ , let token): do {
                                let serviceManager = QMServicesManager()
                                self.showWaitHUD()
                                serviceManager.authService.logIn(withFacebookSessionToken: token.authenticationToken) { (response, userData) in
                                    if response.isSuccess, let userData = userData {
                                        self.getUserProfile(userData: userData)
                                        return
                                    }
                                    
                                    self.dismissHUD()
                                    self.showErrorMessage(title: BSApplicationInfo.name, body: response.getErrorMessage())
                                }
                            }
                            case .cancelled:
                                self.showErrorMessage(title: BSApplicationInfo.name, body: "Cancelled by user")
                            case .failed(_):
                                self.showErrorMessage(title: BSApplicationInfo.name, body: "Failed to login with facebook")
                            }
        }
    }
    
    @IBAction func onClickBtnLoginWithEmail(_ sender: Any) {
        
        view.endEditing(true)
        
        // Validate the inputed information.
        guard let email = txtEmail.text, BSValidation.shared.isValidEmail(email) == true else {
            self.showErrorMessage(title: BSApplicationInfo.name, body: "Invalid email")
            return
        }
        
        guard let password = txtPassword.text, password.isEmpty == false else {
            self.showErrorMessage(title: BSApplicationInfo.name, body: "Password can not be empty")
            return
        }
        
        showWaitHUD()
        let user = QBUUser()
        user.login = email
        user.email = email
        user.password = password
        QMServicesManager.instance().logIn(with: user) { (success, error) in
            self.dismissHUD()
            if success {
                self.getUserProfile(userData: QMServicesManager.instance().currentUser)
            } else if let error = error {
                print(error)
                self.showErrorMessage(title: BSApplicationInfo.name, body: "Username/Password Incorrect.")
            }
        }
    }
    
    @IBAction func onClickBtnSignup(_ sender: Any) {
        if let registerVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BSRegisterViewController.self)) as? BSRegisterViewController {
            registerVC.didClickBtnCloseCompletionHandler = {
                
            }
            
            registerVC.didClickBtnRegisterCompletionHandler = {
                
            }
            
            self.navigationController?.present(registerVC, animated: true)
        }
    }
    
    @IBAction func onCickBtnRecoverPassword(_ sender: Any) {
        if let recoverPasswordVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BSRecoverPasswordViewController.self)) as? BSRecoverPasswordViewController {
            self.navigationController?.present(recoverPasswordVC, animated: true)
        }   
    }
    
    
    // MARK: - Functions
    
    // Get a user's profile (create or update)
    func getUserProfile(userData: QBUUser) {
        // Get a user profile info from quickblox
        QBRequest.objects(withClassName: BSQuickbloxClassName.profile,
                          extendedRequest: ["user_id": userData.id],
                          successBlock: { (response, aryProfiles, responsePage) in
                            
                            // If a user profile has been returned, save the info into user defaults.
                            if let profileData = aryProfiles?.first {
                                self.dismissHUD()
                                BSGlobalServices.shared.objCurUser = BSUserModel(customData: profileData)
                                BSGlobalServices.shared.saveCurrentUser()
                                self.goToMainScreen()
                            } else {
                                // If a user profile hasn't been saved on quickblox, save it.
                                let objProfile = QBCOCustomObject()
                                objProfile.className = BSQuickbloxClassName.profile
                                
                                if let objCurUser = BSGlobalServices.shared.objCurUser {
                                    objProfile.fields.setValue(objCurUser.topic, forKey: "userTopic")
                                    objProfile.fields.setValue(objCurUser.name, forKey: "name")
                                }
                                objProfile.userID = userData.id
                                
                                QBRequest.createObject(objProfile, successBlock: { (response, profileData) in
                                    self.dismissHUD()
                                    
                                    // If a user profile has been returned, save the info into user defaults.
                                    if let profileData = profileData {
                                        BSGlobalServices.shared.objCurUser = BSUserModel(customData: profileData)
                                        BSGlobalServices.shared.saveCurrentUser()
                                        self.goToMainScreen()
                                    } else {
                                        // TODO: logout
                                        if let error = response.error?.error {
                                            self.showErrorMessage(title: BSApplicationInfo.name, body: error.localizedDescription)
                                        }
                                    }
                                }, errorBlock: { (repsonse) in
                                    // TODO: logout
                                    self.dismissHUD()
                                    self.showErrorMessage(title: BSApplicationInfo.name, body: response.getErrorMessage())
                                })
                            }
                            
        }, errorBlock: { (response) in
            // TODO: logout
            self.dismissHUD()
            self.showErrorMessage(title: BSApplicationInfo.name, body: response.getErrorMessage())
        })
    }
    
    
    
    // Go to main screen.
    func goToMainScreen() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.goToMainScreen()
        }
    }
}
