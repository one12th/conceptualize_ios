//
//  BSRecoverPasswordViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/11/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import Quickblox

class BSRecoverPasswordViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var txtEmail: UITextField!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func onClickBtnClose(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func onClickBtnRecoverPassword(_ sender: Any) {
        
        // Validate the email address.
        guard let email = txtEmail.text, BSValidation.shared.isValidEmail(email) == true else {
            self.showErrorMessage(title: BSApplicationInfo.name, body: "Invalid email")
            return
        }
        
        showWaitHUD()
        QBRequest.resetUserPassword(withEmail: email,
                                    successBlock: { (response) in
            
                                        self.dismissHUD()
                                        if response.isSuccess {
                                            self.showSuccessMessage(title: BSApplicationInfo.name, body: "An email was sent!")
                                        } else {
                                            self.showErrorMessage(title: BSApplicationInfo.name, body: "Failed! Check your email address")
                                        }
        }) { (response) in
            self.dismissHUD()
            print(response.getErrorMessage())
            self.showErrorMessage(title: BSApplicationInfo.name, body: "Failed! Check your email address")
        }
    }
    
    // MARK: - Functions
    
}
