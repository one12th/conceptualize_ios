//
//  BSRegisterViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/10/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import QMServices

class BSRegisterViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnRegister: CustomButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtTopicName: UITextField!
    
    var didClickBtnRegisterCompletionHandler: (() -> ())?
    var didClickBtnCloseCompletionHandler: (() -> ())?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnConfirm.isHidden = true
        btnRegister.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func onClickBtnConfirm(_ sender: Any) {
    }
    
    @IBAction func onClickBtnClose(_ sender: Any) {
        if let didClickBtnCloseCompletionHandler = didClickBtnCloseCompletionHandler {
            didClickBtnCloseCompletionHandler()
        }
        dismiss(animated: true)
    }
    
    @IBAction func onClickBtnRegister(_ sender: Any) {
        
        // Validate the current values.
        guard let email = txtEmail.text, BSValidation.shared.isValidEmail(email) == true else {
            showErrorMessage(title: BSApplicationInfo.name, body: "Invalid email")
            return
        }
        
        guard let password = txtPassword.text, let confirmPassword = txtConfirmPassword.text, password.count >= 8 else {
            showErrorMessage(title: BSApplicationInfo.name, body: "Invalid password. Please use capital letters, numbers and symbols. The minimal password length is 8")
            return
        }
        
        guard password.compare(confirmPassword) == ComparisonResult.orderedSame else {
            showErrorMessage(title: BSApplicationInfo.name, body: "Passwords don't match")
            return
        }
        
        guard let fullName = txtFullName.text, fullName.count >= 3 else {
            showErrorMessage(title: BSApplicationInfo.name, body: "Please enter your full name")
            return
        }
        
        guard let topic = txtTopicName.text, topic.isEmpty == false else {
            showErrorMessage(title: BSApplicationInfo.name, body: "Please select a topic")
            return
        }
        
        let objUser = QBUUser()
        objUser.login = email
        objUser.email = email
        objUser.fullName = fullName
        objUser.password = password
        
        showWaitHUD()
        QBRequest.signUp(objUser,
                         successBlock: { (response, userData) in
                            self.dismissHUD()
                            if let login = userData.login, let password = userData.password {
                                QBRequest.logIn(withUserLogin: login,
                                                password: password,
                                                successBlock:
                                    { (response, loginUserData) in
                                                    
                                    if response.isSuccess {
                                        BSGlobalServices.shared.curTopic = BSTopicType(rawValue: topic)
                                        BSGlobalServices.shared.objCurUser?.id = loginUserData.id
                                        BSGlobalServices.shared.objCurUser?.name = loginUserData.fullName
                                        BSGlobalServices.shared.objCurUser?.topic = topic
                                        BSGlobalServices.shared.saveCurrentUser()
                                        
                                        if let didClickBtnRegisterCompletionHandler = self.didClickBtnRegisterCompletionHandler {
                                            didClickBtnRegisterCompletionHandler()
                                            self.dismiss(animated: true)
                                        }
                                        return
                                    }
                                }, errorBlock: { (response) in
                                    self.showErrorMessage(title: BSApplicationInfo.name, body: response.getErrorMessage())
                                })
                            }
                            
        }) { (response) in
            self.dismissHUD()
            self.showErrorMessage(title: BSApplicationInfo.name, body: response.getErrorMessage())
        }
    }
    
    @IBAction func onClickBtnSelectTopic(_ sender: Any) {
        ActionSheetStringPicker(title: "Select a topic",
                                rows: BSTopicType.names,
                                initialSelection: 0,
                                doneBlock: { (picker, rowIndex, name) in
            
                                    if let name = name as? String {
                                        self.txtTopicName.text = name
                                    }
                                    
        },
                                cancel: { (picker) in
            
        },
                                origin: txtTopicName)
            .show()
    }
    
    
    // MARK: - Functions

}
