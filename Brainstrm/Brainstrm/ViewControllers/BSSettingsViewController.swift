//
//  BSSettingsViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/12/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class BSSettingsViewController: UIViewController {

    // MARK: - Properites
    @IBOutlet weak var txtTopicName: UITextField!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let topicName = BSGlobalServices.shared.objCurUser?.topic {
            txtTopicName.text = topicName
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func onClickBtnBack(_ sender: Any) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickBtnSave(_ sender: Any) {
        view.endEditing(true)
        
        if let topicName = txtTopicName.text {
            BSGlobalServices.shared.objCurUser?.topic = topicName
        }
        
        showWaitHUD()
        BSQuickbloxService.shared.updateProfile { (success, profileData, error) in
            self.dismissHUD()
            if success {
                if let profileData = profileData {
                    BSGlobalServices.shared.objCurUser = BSUserModel(customData: profileData)
                    BSGlobalServices.shared.saveCurrentUser()
                    self.showSuccessMessage(title: BSApplicationInfo.name, body: "Successfully updated")
                }
            } else if let error = error {
                print(error)
                self.showErrorMessage(title: BSApplicationInfo.name, body: "Please try again")
            }
        }
    }
    
    @IBAction func onClickBtnLogout(_ sender: Any) {
        view.endEditing(true)
        
        let alertController = UIAlertController(title: "Logout", message: "Do you want to logout?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
            UIView.animate(withDuration: 0,
                           animations: {
                            alertController.dismiss(animated: true)
            }, completion: { (completed) in
                if completed {
                    self.logout()
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        appDelegate.goToLoginScreen()
                    }
                }
            })
        }
        
        let noThanksAction = UIAlertAction(title: "No thanks", style: .default) { (_) in
            alertController.dismiss(animated: true)
        }
        
        alertController.addAction(okAction)
        alertController.addAction(noThanksAction)
        self.present(alertController, animated: true)
    }
    
    @IBAction func onClickBtnDeleteAccount(_ sender: Any) {
        view.endEditing(true)
        let alertController = UIAlertController(title: "Warning", message: "Are you sure you want to delete your account?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Delete", style: .default) { (_) in
            UIView.animate(withDuration: 0,
                           animations: {
                            alertController.dismiss(animated: true)
            }, completion: { (completed) in
                if completed {
                    self.deleteAccount()
                }
            })
        }
        
        let noThanksAction = UIAlertAction(title: "No", style: .default) { (_) in
            alertController.dismiss(animated: true)
        }
        
        alertController.addAction(okAction)
        alertController.addAction(noThanksAction)
        self.present(alertController, animated: true)
    }
    
    @IBAction func onClickBtnSelectTopic(_ sender: Any) {
        
        ActionSheetStringPicker(title: "Select a topic",
                                rows: BSTopicType.names,
                                initialSelection: 0,
                                doneBlock: { (picker, rowIndex, name) in
                                    
                                    if let name = name as? String {
                                        self.txtTopicName.text = name
                                    }
        },
                                cancel: { (picker) in
                                    
        },
                                origin: txtTopicName)
            .show()
    }
    
    // MARK: - Functions
    func deleteAccount() {
        guard let userId = BSGlobalServices.shared.objCurUser?.id else {
            logout()
            return
        }
        
        guard let userObjectId = BSGlobalServices.shared.objCurUser?.objectId else {
            logout()
            return
        }
        
        showWaitHUD()
        BSQuickbloxService.shared.deleteAccount(userId: userId, userObjectId: userObjectId) { (success, error) in
            self.dismissHUD()
            if success {
                self.logout()
                UIView.animate(withDuration: 0,
                               animations: {
                    self.navigationController?.popToRootViewController(animated: false)
                }, completion: { (completed) in
                    if completed {
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.goToLoginScreen()
                        }
                    }
                })
                
                return
            }
            
            if let error = error {
                print(error)
                self.showErrorMessage(title: BSApplicationInfo.name, body: "Please try again")
            }
        }
    }

}
