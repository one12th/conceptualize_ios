//
//  BSChatViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/14/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import QMChatViewController
import QMServices
import CoreTelephony
import MobileCoreServices

let kChatPresenceTimeInterval:TimeInterval = 45
let kDialogsPageLimit:UInt = 100
let kMessageContainerWidthPadding:CGFloat = 40.0
let kMessageContainerHeightPadding: CGFloat = 20

var messageTimeDateFormatter: DateFormatter {
    struct Static {
        static let instance : DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm p"
            return formatter
        }()
    }
    
    return Static.instance
}


class BSChatViewController: QMChatViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    // MARK: - Properties
    @IBOutlet weak var imgAvatar: CustomImage!
    @IBOutlet weak var lblInformation: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var imgTopicPicture: UIImageView!
    @IBOutlet weak var lblTopicName: UILabel!
    @IBOutlet weak var lytInformationViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lytInformationViewHeightConstraint: NSLayoutConstraint!
    
    var shownInfoView: Bool = false
    var dialog: QBChatDialog!
    
    
    let maxCharactersNumber = 1024 // 0 - unlimited
    var failedDownloads: Set<String> = []
    var willResignActiveBlock: AnyObject?
    var attachmentCellsMap: NSMapTable<AnyObject, AnyObject>!
    var detailedCells: Set<String> = []
    var typingTimer: Timer?
    
    lazy var imagePickerViewController : UIImagePickerController = {
        let imagePickerViewController = UIImagePickerController()
        imagePickerViewController.delegate = self
        
        return imagePickerViewController
    }()
    
    var unreadMessages: [QBChatMessage]?
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        lblInformation.text = "YOU MATCHED " + dateFormatter.string(from: dialog.createdAt ?? Date())
        
        let gestureReg = UITapGestureRecognizer(target: self, action: #selector(onDidTapImgAvatar))
        imgAvatar.addGestureRecognizer(gestureReg)
        imgAvatar.isUserInteractionEnabled = true
        
        if let userData = BSGlobalServices.shared.objCurUser {
            var index = 0
            for topic in BSTopicType.names {
                if let userTopic = userData.topic, userTopic.compare(topic) == ComparisonResult.orderedSame {
                    break
                }
                index += 1
            }
            imgTopicPicture.image = UIImage(named: BSTopicType.imageNames[index % BSTopicType.names.count])
            lblTopicName.text = userData.topic
        }
        
        collectionView?.register(BSChatOutgoingCollectionViewCell.nib, forCellWithReuseIdentifier: BSChatOutgoingCollectionViewCell.cellReuseIdentifier())
        collectionView?.register(BSChatIncomingCollectionViewCell.nib, forCellWithReuseIdentifier: BSChatIncomingCollectionViewCell.cellReuseIdentifier())
        collectionView?.register(BSChatAttachmentOutgoingCollectionViewCell.nib, forCellWithReuseIdentifier: BSChatAttachmentOutgoingCollectionViewCell.cellReuseIdentifier())
        collectionView?.register(BSChatAttachmentIncomingCollectionViewCell.nib, forCellWithReuseIdentifier: BSChatAttachmentIncomingCollectionViewCell.cellReuseIdentifier())
        
        view.backgroundColor = UIColor.white
        
        
        if QMServicesManager.instance().isAuthorized {
            let currentUser:QBUUser = QMServicesManager.instance().currentUser
            self.senderID = currentUser.id
            self.senderDisplayName = currentUser.login!
            
            QMServicesManager.instance().chatService.addDelegate(self)
            QMServicesManager.instance().chatService.chatAttachmentService.addDelegate(self)
            
            self.updateTitle()
            
            self.inputToolbar?.contentView?.backgroundColor = UIColor.white
            self.inputToolbar?.contentView?.textView?.placeHolder = "Write your message"
            
            let btnCamera = UIButton(type: .custom)
            btnCamera.frame = CGRect(x: 0, y: 0, width: 38.0, height: 40.0)
            btnCamera.setImage(UIImage(named: "ic_camera"), for: .normal)
            inputToolbar?.contentView.leftBarButtonItem = btnCamera
            
            let btnRight = UIButton(type: .custom)
            btnRight.frame = CGRect(x: 0, y: 0, width: 38.0, height: 40.0)
            btnRight.setImage(UIImage(named: "ic_send"), for: .normal)
            inputToolbar?.contentView.rightBarButtonItem = btnRight
            
            (inputToolbar?.contentView as? BSToolbarContentView)?.didClickBtnAttachementCompletionHandler = {
                self.imagePickerViewController.sourceType = .photoLibrary
                self.present(self.imagePickerViewController, animated: true, completion: nil)
            }
            
            self.attachmentCellsMap = NSMapTable(keyOptions: NSPointerFunctions.Options.strongMemory, valueOptions: NSPointerFunctions.Options.weakMemory)
            
            if self.dialog.type == QBChatDialogType.private {
                
                self.dialog.onUserIsTyping = {
                    [weak self] (userID)-> Void in
                    
                    if QMServicesManager.instance().currentUser.id == userID {
                        return
                    }
                    
                    self?.title = "Typing..."
                }
                
                self.dialog.onUserStoppedTyping = {
                    [weak self] (userID)-> Void in
                    
                    if QMServicesManager.instance().currentUser.id == userID {
                        return
                    }
                    
                    self?.updateTitle()
                }
            }
            
            // Retrieving messages
            let messagesCount = self.storedMessages()?.count
            if (messagesCount == 0) {
                self.startSpinProgress()
            }
            else if (self.chatDataSource.messagesCount() == 0) {
                self.chatDataSource.add(self.storedMessages()!)
            }
            
            self.loadMessages()
            
            self.enableTextCheckingTypes = NSTextCheckingAllTypes
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override class func nib () -> UINib {
        return UINib(nibName: String(describing: BSChatViewController.self), bundle: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.queueManager().add(self)
        
        self.willResignActiveBlock = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillResignActive, object: nil, queue: nil) { [weak self] (notification) in
            
            self?.fireSendStopTypingIfNecessary()
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let willResignActive = self.willResignActiveBlock {
            NotificationCenter.default.removeObserver(willResignActive)
        }
        
        // clearing typing status blocks
        self.dialog.clearTypingStatusBlocks()
        
        self.queueManager().remove(self)
        
        QMServicesManager.instance().chatService.removeDelegate(self)
    }
    
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Update
    
    func updateTitle() {
        
        if self.dialog.type != QBChatDialogType.private {
            
            self.title = self.dialog.name
        }
        else {
            
            if let recipient = QMServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(self.dialog!.recipientID)) {
                
                self.title = recipient.login
            }
        }
    }
    
    func storedMessages() -> [QBChatMessage]? {
        return QMServicesManager.instance().chatService.messagesMemoryStorage.messages(withDialogID: self.dialog.id!)
    }
    
    func loadMessages() {
        // Retrieving messages for chat dialog ID.
        guard let currentDialogID = self.dialog.id else {
            print ("Current chat dialog is nil")
            return
        }
        
        QMServicesManager.instance().chatService.messages(withChatDialogID: currentDialogID, completion: {
            [weak self] (response, messages) -> Void in
            
            guard let strongSelf = self else { return }
            
            guard response.error == nil else {
                print(response.error?.error?.localizedDescription ?? "")
                self?.showErrorMessage(title: BSApplicationInfo.name, body: "Please try again")
                return
            }
            
            if messages?.count ?? 0 > 0 {
                if !(self?.progressView?.isHidden)! {
                    self?.stopSpinProgress()
                }
                strongSelf.chatDataSource.add(messages)
            }
            
//            SVProgressHUD.dismiss()
        })
    }
    
    func sendReadStatusForMessage(message: QBChatMessage) {
        
        guard QBSession.current.currentUser != nil else {
            return
        }
        guard message.senderID != QBSession.current.currentUser?.id else {
            return
        }
        
        if self.messageShouldBeRead(message: message) {
            QMServicesManager.instance().chatService.read(message, completion: { (error) -> Void in
                
                guard error == nil else {
                    print("Problems while marking message as read! Error: %@", error!)
                    return
                }
                
                if UIApplication.shared.applicationIconBadgeNumber > 0 {
                    let badgeNumber = UIApplication.shared.applicationIconBadgeNumber
                    UIApplication.shared.applicationIconBadgeNumber = badgeNumber - 1
                }
            })
        }
    }
    
    func messageShouldBeRead(message: QBChatMessage) -> Bool {
        
        let currentUserID = NSNumber(value: QBSession.current.currentUser!.id as UInt)
        
        return !message.isDateDividerMessage
            && message.senderID != self.senderID
            && !(message.readIDs?.contains(currentUserID))!
    }
    
    func readMessages(messages: [QBChatMessage]) {
        
        if QBChat.instance.isConnected {
            
            QMServicesManager.instance().chatService.read(messages, forDialogID: self.dialog.id!, completion: nil)
        }
        else {
            
            self.unreadMessages = messages
        }
        
        var messageIDs = [String]()
        
        for message in messages {
            messageIDs.append(message.id!)
        }
    }
    
    // MARK: Actions
    @objc
    func onDidTapImgAvatar() {
        if let opponentId = dialog.occupantIDs?.first(where: {$0.uintValue != QBSession.current.currentUser?.id}) {
            showWaitHUD()
            QBRequest.objects(withClassName: BSQuickbloxClassName.profile,
                              extendedRequest: ["user_id": opponentId],
                              successBlock: { (response, aryProfiles, responsePage) in
                                self.dismissHUD()
                                // If a user profile has been returned, save the info into user defaults.
                                if let profileData = aryProfiles?.first {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    if let profileVC = storyboard.instantiateViewController(withIdentifier: String(describing: BSProfileViewController.self)) as? BSProfileViewController {
                                        profileVC.objUser = BSUserModel(customData: profileData)
                                        self.navigationController?.pushViewController(profileVC, animated: true)
                                    }
                                    
                                }
            }, errorBlock: { (response) in
                // TODO: logout
                self.dismissHUD()
                self.showErrorMessage(title: BSApplicationInfo.name, body: response.getErrorMessage())
            })
        }
    }
    
    // Camera
    override func didPressAccessoryButton(_ sender: UIButton) {
        
        imagePickerViewController.sourceType = .camera
        present(imagePickerViewController, animated: true, completion: nil)
        
    }
    
    override func didPickAttachmentImage(_ image: UIImage) {
        
        let message = QBChatMessage()
        message.senderID = self.senderID
        message.dialogID = self.dialog.id
        message.dateSent = Date()
        
        DispatchQueue.global().async { [weak self] () -> Void in
            
            guard let strongSelf = self else { return }
            
            var newImage : UIImage! = image
            if strongSelf.imagePickerViewController.sourceType == UIImagePickerControllerSourceType.camera {
//                newImage = newImage.fixOrientation()
            }
            
            let largestSide = newImage.size.width > newImage.size.height ? newImage.size.width : newImage.size.height
            let scaleCoeficient = largestSide/560.0
            let newSize = CGSize(width: newImage.size.width/scaleCoeficient, height: newImage.size.height/scaleCoeficient)
            
            // create smaller image
            
            UIGraphicsBeginImageContext(newSize)
            
            newImage.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
            
            // Sending attachment.
            DispatchQueue.main.async(execute: {
                self?.chatDataSource.add(message)
                // sendAttachmentMessage method always firstly adds message to memory storage
                QMServicesManager.instance().chatService.sendAttachmentMessage(message, to: self!.dialog, withAttachmentImage: resizedImage!, completion: {
                    [weak self] (error) -> Void in
                    
                    self?.attachmentCellsMap.removeObject(forKey: message.id as AnyObject?)
                    
                    guard error != nil else { return }
                    
                    self?.chatDataSource.delete(message)
                })
            })
        }
    }
    
    override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: UInt, senderDisplayName: String, date: Date) {
        
        if !self.queueManager().shouldSendMessagesInDialog(withID: self.dialog.id!) {
            return
        }
        self.fireSendStopTypingIfNecessary()
        
        let message = QBChatMessage()
        message.text = text
        message.senderID = self.senderID
        message.deliveredIDs = [(NSNumber(value: self.senderID))]
        message.readIDs = [(NSNumber(value: self.senderID))]
        message.markable = true
        message.dateSent = date
        
        self.sendMessage(message: message)
    }
    
    override func didPressSend(_ button: UIButton, withTextAttachments textAttachments: [Any], senderId: UInt, senderDisplayName: String, date: Date) {
        
        if let attachment = textAttachments.first as? NSTextAttachment {
            
            if (attachment.image != nil) {
                let message = QBChatMessage()
                message.senderID = self.senderID
                message.dialogID = self.dialog.id
                message.dateSent = Date()
                QMServicesManager.instance().chatService.sendAttachmentMessage(message, to: self.dialog, withAttachmentImage: attachment.image!, completion: {
                    [weak self] (error: Error?) -> Void in
                    
                    self?.attachmentCellsMap.removeObject(forKey: message.id as AnyObject?)
                    
                    guard error != nil else { return }
                    
                    // perform local attachment message deleting if error
                    QMServicesManager.instance().chatService.deleteMessageLocally(message)
                    
                    self?.chatDataSource.delete(message)
                    
                })
                
                self.finishSendingMessage(animated: true)
            }
        }
    }
    
    func sendMessage(message: QBChatMessage) {
        
        // Sending message.
        QMServicesManager.instance().chatService.send(message, toDialogID: self.dialog.id!, saveToHistory: true, saveToStorage: true) { (error) ->
            Void in
            
            if error != nil {
                print(error?.localizedDescription ?? "")
                self.showErrorMessage(title: BSApplicationInfo.name, body: "Please try again")
            }
        }
        
        self.finishSendingMessage(animated: true)
    }
    
    // MARK: Helper
    func canMakeACall() -> Bool {
        
        var canMakeACall = false
        
        if (UIApplication.shared.canOpenURL(URL.init(string: "tel://")!)) {
            
            // Check if iOS Device supports phone calls
            let networkInfo = CTTelephonyNetworkInfo()
            let carrier = networkInfo.subscriberCellularProvider
            if carrier == nil {
                return false
            }
            let mnc = carrier?.mobileNetworkCode
            if mnc?.count == 0 {
                // Device cannot place a call at this time.  SIM might be removed.
            }
            else {
                // iOS Device is capable for making calls
                canMakeACall = true
            }
        }
        else {
            // iOS Device is not capable for making calls
        }
        
        return canMakeACall
    }
    
    func placeHolderTextView(_ textView: QMPlaceHolderTextView, shouldPasteWithSender sender: Any) -> Bool {
        
        if UIPasteboard.general.image != nil {
            
            let textAttachment = NSTextAttachment()
            textAttachment.image = UIPasteboard.general.image!
            textAttachment.bounds = CGRect(x: 0, y: 0, width: 100, height: 100)
            
            let attrStringWithImage = NSAttributedString.init(attachment: textAttachment)
            self.inputToolbar?.contentView.textView.attributedText = attrStringWithImage
            self.textViewDidChange((self.inputToolbar?.contentView.textView)!)
            
            return false
        }
        
        return true
    }
    
    func showCharactersNumberError() {
        let message = String(format: "The character limit is %lu.", maxCharactersNumber)
        self.showErrorMessage(title: BSApplicationInfo.name, body: message)
    }
    
    /**
     Builds a string
     Read: login1, login2, login3
     Delivered: login1, login3, @12345
     
     If user does not exist in usersMemoryStorage, then ID will be used instead of login
     
     - parameter message: QBChatMessage instance
     
     - returns: status string
     */
    func statusStringFromMessage(message: QBChatMessage) -> String {
        
        var statusString = ""
        
        let currentUserID = NSNumber(value:self.senderID)
        
        var readLogins: [String] = []
        
        if message.readIDs != nil {
            
            let messageReadIDs = message.readIDs!.filter { (element) -> Bool in
                
                return !element.isEqual(to: currentUserID)
            }
            
            if !messageReadIDs.isEmpty {
                for readID in messageReadIDs {
                    let user = QMServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(truncating: readID))
                    
                    guard let unwrappedUser = user else {
                        let unknownUserLogin = "@\(readID)"
                        readLogins.append(unknownUserLogin)
                        
                        continue
                    }
                    
                    readLogins.append(unwrappedUser.login!)
                }
                
                statusString += message.isMediaMessage() ? "Seen" : "Read"
                statusString += ": " + readLogins.joined(separator: ", ")
            }
        }
        
        if message.deliveredIDs != nil {
            var deliveredLogins: [String] = []
            
            let messageDeliveredIDs = message.deliveredIDs!.filter { (element) -> Bool in
                return !element.isEqual(to: currentUserID)
            }
            
            if !messageDeliveredIDs.isEmpty {
                for deliveredID in messageDeliveredIDs {
                    let user = QMServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(truncating: deliveredID))
                    
                    guard let unwrappedUser = user else {
                        let unknownUserLogin = "@\(deliveredID)"
                        deliveredLogins.append(unknownUserLogin)
                        
                        continue
                    }
                    
                    if readLogins.contains(unwrappedUser.login!) {
                        continue
                    }
                    
                    deliveredLogins.append(unwrappedUser.login!)
                    
                }
                
                if readLogins.count > 0 && deliveredLogins.count > 0 {
                    statusString += "\n"
                }
                
                if deliveredLogins.count > 0 {
                    statusString += "Delivered" + ": " + deliveredLogins.joined(separator: ", ")
                }
            }
        }
        
        if statusString.isEmpty {
            
            let messageStatus: QMMessageStatus = self.queueManager().status(for: message)
            
            switch messageStatus {
            case .sent:
                statusString = "Sent"
            case .sending:
                statusString = "Sending"
            case .notSent:
                statusString = "Didn't send"
            }
            
        }
        
        return statusString
    }
    
    // MARK: Override
    
    override func viewClass(forItem item: QBChatMessage) -> AnyClass {
        // TODO: check and add QMMessageType.AcceptContactRequest, QMMessageType.RejectContactRequest, QMMessageType.ContactRequest
        
        if item.isNotificationMessage() || item.isDateDividerMessage {
            return QMChatNotificationCell.self
        }
        
        if (item.senderID != self.senderID) {
            
            if (item.isMediaMessage() && item.attachmentStatus != QMMessageAttachmentStatus.error) {
                
                return BSChatAttachmentIncomingCollectionViewCell.self
                
            } else {
                
                return BSChatIncomingCollectionViewCell.self
            }
        } else {
            
            if (item.isMediaMessage() && item.attachmentStatus != QMMessageAttachmentStatus.error) {
                
                return BSChatAttachmentOutgoingCollectionViewCell.self
                
            } else {
                
                return BSChatOutgoingCollectionViewCell.self
            }
        }
    }
    
    // MARK: Strings builder
    
    override func attributedString(forItem messageItem: QBChatMessage) -> NSAttributedString? {
        
        guard messageItem.text != nil else {
            return nil
        }
        
        var textColor = messageItem.senderID == self.senderID ? UIColor(hex: "0x6f6f6f") : UIColor.white
        if messageItem.isNotificationMessage() || messageItem.isDateDividerMessage {
            textColor = UIColor.black
        }
        
        var attributes = Dictionary<NSAttributedStringKey, AnyObject>()
        attributes[NSAttributedStringKey.foregroundColor] = textColor
        attributes[NSAttributedStringKey.font] = UIFont.systemFont(ofSize: 15, weight: .bold)
        
        let attributedString = NSAttributedString(string: messageItem.text!, attributes: attributes)
        
        return attributedString
    }
    
    
    /**
     Creates top label attributed string from QBChatMessage
     
     - parameter messageItem: QBCHatMessage instance
     
     - returns: login string, example: @SwiftTestDevUser1
     */
    override func topLabelAttributedString(forItem messageItem: QBChatMessage) -> NSAttributedString? {
        
        guard messageItem.senderID != self.senderID else {
            return nil
        }
        
        guard self.dialog.type != QBChatDialogType.private else {
            return nil
        }
        
        let paragrpahStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragrpahStyle.lineBreakMode = NSLineBreakMode.byTruncatingTail
        var attributes = Dictionary<NSAttributedStringKey, AnyObject>()
        attributes[NSAttributedStringKey.foregroundColor] = UIColor(red: 11.0/255.0, green: 96.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        attributes[NSAttributedStringKey.font] = UIFont.systemFont(ofSize: 17)
        attributes[NSAttributedStringKey.paragraphStyle] = paragrpahStyle
        
        var topLabelAttributedString : NSAttributedString?
        
        if let topLabelText = QMServicesManager.instance().usersService.usersMemoryStorage.user(withID: messageItem.senderID)?.login {
            topLabelAttributedString = NSAttributedString(string: topLabelText, attributes: attributes)
        } else { // no user in memory storage
            topLabelAttributedString = NSAttributedString(string: "@\(messageItem.senderID)", attributes: attributes)
        }
        
        return topLabelAttributedString
    }
    
    /**
     Creates bottom label attributed string from QBChatMessage using self.statusStringFromMessage
     
     - parameter messageItem: QBChatMessage instance
     
     - returns: bottom label status string
     */
    override func bottomLabelAttributedString(forItem messageItem: QBChatMessage) -> NSAttributedString {
        
        let textColor = messageItem.senderID == self.senderID ? UIColor(hex: "0xe0e0e0") : UIColor.white
        
        let paragrpahStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragrpahStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        var attributes = Dictionary<NSAttributedStringKey, AnyObject>()
        attributes[NSAttributedStringKey.foregroundColor] = textColor
        attributes[NSAttributedStringKey.font] = UIFont.systemFont(ofSize: 11)
        attributes[NSAttributedStringKey.paragraphStyle] = paragrpahStyle
        
        let text = messageItem.dateSent != nil ? messageTimeDateFormatter.string(from: messageItem.dateSent!) : ""
        
        /*
        if messageItem.senderID == self.senderID {
            text = text + "\n" + self.statusStringFromMessage(message: messageItem)
        }*/
        
        let bottomLabelAttributedString = NSAttributedString(string: text, attributes: attributes)
        
        return bottomLabelAttributedString
    }
    
    // MARK: Collection View Datasource
    
    override func collectionView(_ collectionView: QMChatCollectionView!, dynamicSizeAt indexPath: IndexPath!, maxWidth: CGFloat) -> CGSize {
        
        var size = CGSize.zero
        
        guard let message = self.chatDataSource.message(for: indexPath) else {
            return size
        }
        
        let messageCellClass: AnyClass! = self.viewClass(forItem: message)
        
        if messageCellClass === BSChatAttachmentIncomingCollectionViewCell.self {
            
            size = CGSize(width: min(200, maxWidth), height: 200)
            size.height += kMessageContainerHeightPadding
        }
        else if messageCellClass === BSChatAttachmentOutgoingCollectionViewCell.self {
            
            let attributedString = self.bottomLabelAttributedString(forItem: message)
            
            let bottomLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSize(width: min(200, maxWidth), height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
            size = CGSize(width: min(200, maxWidth), height: 200 + ceil(bottomLabelSize.height))
            size.height += kMessageContainerHeightPadding
        }
        else if messageCellClass === QMChatNotificationCell.self {
            
            let attributedString = self.attributedString(forItem: message)
            size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
        }
        else {
            
            let attributedString = self.attributedString(forItem: message)
            
            size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
            size.height += kMessageContainerHeightPadding
        }
        
        return size
    }
    
    override func collectionView(_ collectionView: QMChatCollectionView!, minWidthAt indexPath: IndexPath!) -> CGFloat {
        
        var size = CGSize.zero
        
        guard let item = self.chatDataSource.message(for: indexPath) else {
            return 0
        }
        
        if self.detailedCells.contains(item.id!) {
            
            let str = self.bottomLabelAttributedString(forItem: item)
            let frameWidth = collectionView.frame.width
            let maxHeight = CGFloat.greatestFiniteMagnitude
            
            size = TTTAttributedLabel.sizeThatFitsAttributedString(str, withConstraints: CGSize(width:frameWidth - kMessageContainerWidthPadding, height: maxHeight), limitedToNumberOfLines:0)
        }
        
        if self.dialog.type != QBChatDialogType.private {
            
            let topLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(self.topLabelAttributedString(forItem: item), withConstraints: CGSize(width: collectionView.frame.width - kMessageContainerWidthPadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:0)
            
            if topLabelSize.width > size.width {
                size = topLabelSize
            }
        } else {
            let bottomLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(self.bottomLabelAttributedString(forItem: item), withConstraints: CGSize(width: collectionView.frame.width - kMessageContainerWidthPadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:0)
            
            print("bottomLabelSize = " + bottomLabelSize.debugDescription)
            if bottomLabelSize.width > size.width {
                size = bottomLabelSize
            }
        }
        
        return size.width
    }
    
    override func collectionView(_ collectionView: QMChatCollectionView!, layoutModelAt indexPath: IndexPath!) -> QMChatCellLayoutModel {
        
        var layoutModel: QMChatCellLayoutModel = super.collectionView(collectionView, layoutModelAt: indexPath)
        
        layoutModel.avatarSize = CGSize(width: 0, height: 0)
        layoutModel.topLabelHeight = 0.0
        layoutModel.spaceBetweenTextViewAndBottomLabel = 5
        layoutModel.maxWidthMarginSpace = 20.0
        
        guard let item = self.chatDataSource.message(for: indexPath) else {
            return layoutModel
        }
        
        let viewClass: AnyClass = self.viewClass(forItem: item) as AnyClass
        
        if viewClass === BSChatIncomingCollectionViewCell.self || viewClass === BSChatAttachmentIncomingCollectionViewCell.self {
            
            if self.dialog.type != QBChatDialogType.private {
                let topAttributedString = self.topLabelAttributedString(forItem: item)
                let size = TTTAttributedLabel.sizeThatFitsAttributedString(topAttributedString, withConstraints: CGSize(width: collectionView.frame.width - kMessageContainerWidthPadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:1)
                layoutModel.topLabelHeight = size.height
            }
            
            layoutModel.spaceBetweenTopLabelAndTextView = 5
        }
        
        var size = CGSize.zero
        
        let bottomAttributedString = self.bottomLabelAttributedString(forItem: item)
        size = TTTAttributedLabel.sizeThatFitsAttributedString(bottomAttributedString, withConstraints: CGSize(width: collectionView.frame.width - kMessageContainerWidthPadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:0)
        
        layoutModel.bottomLabelHeight = floor(size.height)
        
        
        return layoutModel
    }
    
    override func collectionView(_ collectionView: QMChatCollectionView, configureCell cell: UICollectionViewCell, for indexPath: IndexPath) {
        
        super.collectionView(collectionView, configureCell: cell, for: indexPath)
        
        // subscribing to cell delegate
        let chatCell = cell as! QMChatCell
        
        chatCell.delegate = self
        
        let message = self.chatDataSource.message(for: indexPath)
        
        if let attachmentCell = cell as? QMChatAttachmentCell {
            
            if attachmentCell is BSChatAttachmentIncomingCollectionViewCell {
                chatCell.containerView?.bgColor = UIColor(red: 226.0/255.0, green: 226.0/255.0, blue: 226.0/255.0, alpha: 1.0)
            }
            else if attachmentCell is BSChatAttachmentOutgoingCollectionViewCell {
                chatCell.containerView?.bgColor = UIColor.white
            }
            
            if let attachment = message?.attachments?.first {
                
                var keysToRemove: [String] = []
                
                let enumerator = self.attachmentCellsMap.keyEnumerator()
                
                while let existingAttachmentID = enumerator.nextObject() as? String {
                    let cachedCell = self.attachmentCellsMap.object(forKey: existingAttachmentID as AnyObject?)
                    if cachedCell === cell {
                        keysToRemove.append(existingAttachmentID)
                    }
                }
                
                for key in keysToRemove {
                    self.attachmentCellsMap.removeObject(forKey: key as AnyObject?)
                }
                
                if let attachmentID = attachment.id {
                    if self.failedDownloads.contains(attachmentID) {
                        attachmentCell.setAttachmentImage(UIImage(named:"error_image"))
                        return
                    }
                }
                
                self.attachmentCellsMap.setObject(attachmentCell, forKey: attachment.id as AnyObject?)
                
                attachmentCell.attachmentID = attachment.id
                
                // Getting image from chat attachment cache.
                
                QMServicesManager.instance().chatService.chatAttachmentService.image(forAttachmentMessage: message!, completion: { [weak self] (error, image) in
                    
                    guard attachmentCell.attachmentID == attachment.id else {
                        return
                    }
                    
                    self?.attachmentCellsMap.removeObject(forKey: attachment.id as AnyObject?)
                    
                    guard error == nil else {
                        if (error! as NSError).code == 404 {
                            self?.failedDownloads.insert(attachment.id!)
                            
                            attachmentCell.setAttachmentImage(UIImage(named:"error_image"))
                        }
                        print("Error downloading image from server: \(error!.localizedDescription)")
                        return
                    }
                    
                    if image == nil {
                        print("Image is nil")
                    }
                    
                    attachmentCell.setAttachmentImage(image)
                    cell.updateConstraints()
                })
            }
            
        }
        else if cell is BSChatIncomingCollectionViewCell || cell is BSChatAttachmentIncomingCollectionViewCell {
            chatCell.containerView?.bgColor = UIColor(hex: "0x1479cd")
        } else if cell is BSChatOutgoingCollectionViewCell {
            
            let status: QMMessageStatus = self.queueManager().status(for: message!)

            switch status {
            case .sent:
                chatCell.containerView?.bgColor = UIColor.white
            case .sending:
                chatCell.containerView?.bgColor = UIColor.white
            case .notSent:
                chatCell.containerView?.bgColor = UIColor.white
            }
        }
        else if cell is BSChatAttachmentOutgoingCollectionViewCell {
            chatCell.containerView?.bgColor = UIColor.white
        }
        else if cell is QMChatNotificationCell {
            cell.isUserInteractionEnabled = false
            chatCell.containerView?.bgColor = self.collectionView?.backgroundColor
        }
    }
    
    /**
     Allows to copy text from BSChatIncomingCollectionViewCell and BSChatOutgoingCollectionViewCell
     */
    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        
        guard let item = self.chatDataSource.message(for: indexPath) else {
            return false
        }
        
        let viewClass: AnyClass = self.viewClass(forItem: item) as AnyClass
        
        if  viewClass === QMChatNotificationCell.self ||
            viewClass === QMChatContactRequestCell.self {
            return false
        }
        
        return super.collectionView(collectionView, canPerformAction: action, forItemAt: indexPath, withSender: sender)
    }
    
    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        
        let item = self.chatDataSource.message(for: indexPath)
        
        if (item?.isMediaMessage())! {
            QMServicesManager.instance().chatService.chatAttachmentService.localImage(forAttachmentMessage: item!, completion: { (image) in
                
                if image != nil {
                    guard let imageData = UIImageJPEGRepresentation(image!, 1) else { return }
                    
                    let pasteboard = UIPasteboard.general
                    pasteboard.setValue(imageData, forPasteboardType:kUTTypeJPEG as String)
                }
            })
        }
        else {
            UIPasteboard.general.string = item?.text
        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let lastSection = self.collectionView!.numberOfSections - 1
        
        if (indexPath.section == lastSection && indexPath.item == (self.collectionView?.numberOfItems(inSection: lastSection))! - 1) {
            // the very first message
            // load more if exists
            // Getting earlier messages for chat dialog identifier.
            
            guard let dialogID = self.dialog.id else {
                print("DialogID is nil")
                return super.collectionView(collectionView, cellForItemAt: indexPath)
            }
            
            QMServicesManager.instance().chatService.loadEarlierMessages(withChatDialogID: dialogID).continueWith(block: {[weak self](task) -> Any? in
                
                guard let strongSelf = self else { return nil }
                
                if (task.result?.count ?? 0 > 0) {
                    
                    strongSelf.chatDataSource.add(task.result as! [QBChatMessage])
                }
                
                return nil
            })
        }
        
        // marking message as read if needed
        if let message = self.chatDataSource.message(for: indexPath) {
            self.sendReadStatusForMessage(message: message)
        }
        
        if let messageItem = self.chatDataSource.message(for: indexPath) {
            if viewClass(forItem: messageItem) == BSChatOutgoingCollectionViewCell.self {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BSChatOutgoingCollectionViewCell.cellReuseIdentifier(), for: indexPath) as? BSChatOutgoingCollectionViewCell {
                    
                    cell.topLabel.attributedText = self.topLabelAttributedString(forItem: messageItem)
                    cell.textView.attributedText = self.attributedString(forItem: messageItem)
                    cell.bottomLabel.attributedText = self.bottomLabelAttributedString(forItem: messageItem)
                    
                    cell.containerView.backgroundColor = UIColor.white
                    cell.containerView.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: 0, height: 1), radius: 1, scale: true, cornerRadius: 4)
                    
                    return cell
                }
            } else if viewClass(forItem: messageItem) == BSChatIncomingCollectionViewCell.self {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BSChatIncomingCollectionViewCell.cellReuseIdentifier(), for: indexPath) as? BSChatIncomingCollectionViewCell {
                    
                    cell.topLabel.attributedText = self.topLabelAttributedString(forItem: messageItem)
                    cell.textView.attributedText = self.attributedString(forItem: messageItem)
                    cell.bottomLabel.attributedText = self.bottomLabelAttributedString(forItem: messageItem)
                    
                    cell.containerView.backgroundColor = UIColor(hex: "0x1479cd")
                    cell.containerView.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: 0, height: 1), radius: 1, scale: true, cornerRadius: 4)
                    
                    return cell
                }
            } else if viewClass(forItem: messageItem) == BSChatAttachmentOutgoingCollectionViewCell.self {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BSChatAttachmentOutgoingCollectionViewCell.cellReuseIdentifier(), for: indexPath) as? BSChatAttachmentOutgoingCollectionViewCell {
                    
                    cell.containerView.backgroundColor = UIColor.white
                    cell.containerView.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: 0, height: 1), radius: 1, scale: true, cornerRadius: 4)

                    self.collectionView(self.collectionView!, configureCell: cell, for: indexPath)
                    
                    return cell
                }
            } else if viewClass(forItem: messageItem) == BSChatAttachmentIncomingCollectionViewCell.self {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BSChatAttachmentIncomingCollectionViewCell.cellReuseIdentifier(), for: indexPath) as? BSChatAttachmentIncomingCollectionViewCell {
                    
                    cell.containerView.backgroundColor = UIColor(hex: "0x1479cd")
                    cell.containerView.dropShadow(color: UIColor.black, opacity: 0.2, offSet: CGSize(width: 0, height: 1), radius: 1, scale: true, cornerRadius: 4)
                    
                    self.collectionView(self.collectionView!, configureCell: cell, for: indexPath)
                    
                    return cell
                }
            }
            
        }
        
        return super.collectionView(collectionView, cellForItemAt
            : indexPath)
    }
    
    // MARK: UITextViewDelegate
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Prevent crashing undo bug
        let currentCharacterCount = textView.text?.count ?? 0
        
        if (range.length + range.location > currentCharacterCount) {
            return false
        }
        
        if !QBChat.instance.isConnected { return true }
        
        if let timer = self.typingTimer {
            timer.invalidate()
            self.typingTimer = nil
            
        } else {
            
            self.sendBeginTyping()
        }
        
        self.typingTimer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(fireSendStopTypingIfNecessary), userInfo: nil, repeats: false)
        
        if maxCharactersNumber > 0 {
            
            if currentCharacterCount >= maxCharactersNumber && text.count > 0 {
                
                self.showCharactersNumberError()
                return false
            }
            
            let newLength = currentCharacterCount + text.count - range.length
            
            if  newLength <= maxCharactersNumber || text.count == 0 {
                return true
            }
            
            let oldString = textView.text ?? ""
            
            let numberOfSymbolsToCut = maxCharactersNumber - oldString.count
            
            var stringRange = NSMakeRange(0, min(text.count, numberOfSymbolsToCut))
            
            
            // adjust the range to include dependent chars
            stringRange = (text as NSString).rangeOfComposedCharacterSequences(for: stringRange)
            
            // Now you can create the short string
            let shortString = (text as NSString).substring(with: stringRange)
            
            let newText = NSMutableString()
            newText.append(oldString)
            newText.insert(shortString, at: range.location)
            textView.text = newText as String
            
            self.showCharactersNumberError()
            
            self.textViewDidChange(textView)
            
            return false
        }
        
        return true
    }
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        
        super.textViewDidEndEditing(textView)
        
        self.fireSendStopTypingIfNecessary()
    }
    
    @objc func fireSendStopTypingIfNecessary() -> Void {
        
        if let timer = self.typingTimer {
            
            timer.invalidate()
        }
        
        self.typingTimer = nil
        self.sendStopTyping()
    }
    
    func sendBeginTyping() -> Void {
        self.dialog.sendUserIsTyping()
    }
    
    func sendStopTyping() -> Void {
        
        self.dialog.sendUserStoppedTyping()
    }
    
    
    // MARK: - Actions
    @IBAction func onClickBtnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickBtnDropdown(_ sender: Any) {
        shownInfoView = !shownInfoView
        if shownInfoView {
            UIView.animate(withDuration: 0.5) {
                self.lytInformationViewTopConstraint.constant = 0
                self.lytInformationViewHeightConstraint.constant = 94
                self.view.layoutIfNeeded()
                self.btnDropdown.transform = CGAffineTransform(rotationAngle: 3.14)
            }
        } else {
            UIView.animate(withDuration: 0.5) {
                self.lytInformationViewTopConstraint.constant = -44
                self.lytInformationViewHeightConstraint.constant = 44
                self.view.layoutIfNeeded()
                self.btnDropdown.transform = CGAffineTransform(rotationAngle: 0)
            }
        }
    }
    
    // MARK: - Functions
}

// MARK: - QMChatAttachmentServiceDelegate
extension BSChatViewController: QMChatAttachmentServiceDelegate {
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChange status: QMMessageAttachmentStatus, for message: QBChatMessage) {
        
        if status != QMMessageAttachmentStatus.notLoaded {
            
            if message.dialogID == self.dialog.id {
                self.chatDataSource.update(message)
            }
        }
    }
    
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChangeLoadingProgress progress: CGFloat, for attachment: QBChatAttachment) {
        
        if let attachmentCell = self.attachmentCellsMap.object(forKey: attachment.id! as AnyObject?) {
            attachmentCell.updateLoadingProgress(progress)
        }
    }
    
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChangeUploadingProgress progress: CGFloat, for message: QBChatMessage) {
        
        guard message.dialogID == self.dialog.id else {
            return
        }
        var cell = self.attachmentCellsMap.object(forKey: message.id as AnyObject?)
        
        if cell == nil && progress < 1.0 {
            
            if let indexPath = self.chatDataSource.indexPath(for: message) {
                cell = self.collectionView?.cellForItem(at: indexPath) as? QMChatAttachmentCell
                self.attachmentCellsMap.setObject(cell, forKey: message.id as AnyObject?)
            }
        }
        
        cell?.updateLoadingProgress(progress)
    }
}

// MARK: - QMDeferredQueueManager
extension BSChatViewController: QMDeferredQueueManagerDelegate {
    func deferredQueueManager(_ queueManager: QMDeferredQueueManager, didAddMessageLocally addedMessage: QBChatMessage) {
        
        if addedMessage.dialogID == self.dialog.id {
            self.chatDataSource.add(addedMessage)
        }
    }
    
    func deferredQueueManager(_ queueManager: QMDeferredQueueManager, didUpdateMessageLocally addedMessage: QBChatMessage) {
        
        if addedMessage.dialogID == self.dialog.id {
            self.chatDataSource.update(addedMessage)
        }
    }
}

// MARK: - QMChatCellDelegate
extension BSChatViewController: QMChatCellDelegate {
    /**
     Removes size from cache for item to allow cell expand and show read/delivered IDS or unexpand cell
     */
    func chatCellDidTapContainer(_ cell: QMChatCell!) {
        let indexPath = self.collectionView?.indexPath(for: cell)
        
        guard let currentMessage = self.chatDataSource.message(for: indexPath) else {
            return
        }
        
        let messageStatus: QMMessageStatus = self.queueManager().status(for: currentMessage)
        
        if messageStatus == .notSent {
            self.handleNotSentMessage(currentMessage, forCell:cell)
            return
        }
        
        if self.detailedCells.contains(currentMessage.id!) {
            self.detailedCells.remove(currentMessage.id!)
        } else {
            self.detailedCells.insert(currentMessage.id!)
        }
        
        self.collectionView?.collectionViewLayout.removeSizeFromCache(forItemID: currentMessage.id)
        self.collectionView?.performBatchUpdates(nil, completion: nil)
        
    }
    
    func chatCell(_ cell: QMChatCell!, didTapAtPosition position: CGPoint) {}
    
    func chatCell(_ cell: QMChatCell!, didPerformAction action: Selector!, withSender sender: Any!) {}
    
    func chatCell(_ cell: QMChatCell!, didTapOn result: NSTextCheckingResult) {
        
        switch result.resultType {
            
        case NSTextCheckingResult.CheckingType.link:
            
            let strUrl : String = (result.url?.absoluteString)!
            
            if UIApplication.shared.canOpenURL(URL(string: strUrl)!) {
                UIApplication.shared.open(URL(string: strUrl)!, options: [:], completionHandler: nil)
            }
            
            break
            
        case NSTextCheckingResult.CheckingType.phoneNumber:
            
            if !self.canMakeACall() {
                showErrorMessage(title: BSApplicationInfo.name, body: "Your Device can't make a phone call")
                break
            }
            
            let urlString = String(format: "tel:%@",result.phoneNumber!)
            let url = URL(string: urlString)
            
            self.view.endEditing(true)
            
            let alertController = UIAlertController(title: "",
                                                    message: result.phoneNumber,
                                                    preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Call", style: .destructive) { (action) in
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            }
            alertController.addAction(openAction)
            
            self.present(alertController, animated: true) {
            }
            
            break
            
        default:
            break
        }
    }
    
    func chatCellDidTapAvatar(_ cell: QMChatCell!) {
    }
}

// MARK: - QMChatServiceDelegate
extension BSChatViewController: QMChatServiceDelegate {
    func chatService(_ chatService: QMChatService, didLoadMessagesFromCache messages: [QBChatMessage], forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            if !(self.progressView?.isHidden)! {
                self.stopSpinProgress()
            }
            self.chatDataSource.add(messages)
        }
    }
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            // Insert message received from XMPP or self sent
            if self.chatDataSource.messageExists(message) {
                
                self.chatDataSource.update(message)
            }
            else {
                
                self.chatDataSource.add(message)
            }
        }
    }
    
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        
        if self.dialog.type != QBChatDialogType.private && self.dialog.id == chatDialog.id {
            self.dialog = chatDialog
            self.title = self.dialog.name
        }
    }
    
    func chatService(_ chatService: QMChatService, didUpdate message: QBChatMessage, forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            self.chatDataSource.update(message)
        }
    }
    
    func chatService(_ chatService: QMChatService, didUpdate messages: [QBChatMessage], forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            self.chatDataSource.update(messages)
        }
    }
}

// MARK: - QMChatConnectionDelegate
extension BSChatViewController: QMChatConnectionDelegate {
    
    func refreshAndReadMessages() {
        showErrorMessage(title: BSApplicationInfo.name, body: "Loading messages")
        self.loadMessages()
        
        if let messagesToRead = self.unreadMessages {
            self.readMessages(messages: messagesToRead)
        }
        
        self.unreadMessages = nil
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
        
        self.refreshAndReadMessages()
    }
    
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
        
        self.refreshAndReadMessages()
    }
    
    func queueManager() -> QMDeferredQueueManager {
        return QMServicesManager.instance().chatService.deferredQueueManager
    }
    
    func handleNotSentMessage(_ message: QBChatMessage,
                              forCell cell: QMChatCell!) {
        
        let alertController = UIAlertController(title: "", message: "Message failed to send", preferredStyle:.actionSheet)
        
        let resend = UIAlertAction(title: "Try again", style: .default) { (action) in
            self.queueManager().perfromDefferedAction(for: message, withCompletion: nil)
        }
        alertController.addAction(resend)
        
        let delete = UIAlertAction(title: "Delete", style: .destructive) { (action) in
            self.queueManager().remove(message)
            self.chatDataSource.delete(message)
        }
        alertController.addAction(delete)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alertController.addAction(cancelAction)
        
        if alertController.popoverPresentationController != nil {
            self.view.endEditing(true)
            alertController.popoverPresentationController!.sourceView = cell.containerView
            alertController.popoverPresentationController!.sourceRect = cell.containerView.bounds
        }
        
        self.present(alertController, animated: true) {
        }
    }
}
