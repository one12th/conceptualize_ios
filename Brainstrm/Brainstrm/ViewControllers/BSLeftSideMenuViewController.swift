//
//  BSLeftSideMenuViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/10/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit

enum BSSideMenuType: Int {
    case home = 1
    case messages = 2
    case profile = 3
    case settings = 4
    case logout = 5
    
    static let resuableIdentifiers = ["BSMenuHomeTableViewCell",
                                      "BSMenuMessagesTableViewCell",
                                      "BSMenuProfileTableViewCell",
                                      "BSMenuSettingsTableViewCell",
                                      "BSMenuLogoutTableViewCell"]
}

class BSLeftSideMenuViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var tblContents: UITableView!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblTopicName: UILabel!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblContents.rowHeight = UITableViewAutomaticDimension
        tblContents.estimatedRowHeight = 44
        tblContents.separatorStyle = .none
        tblContents.delegate = self
        tblContents.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let userName = BSGlobalServices.shared.objCurUser?.name {
            lblTopicName.text = userName
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func onClickBtnSetting(_ sender: Any) {
        
    }
    
    // MARK: - Functions
}

// MARK: - UITableViewDelegate
extension BSLeftSideMenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let menuNC = sideMenuManager?.sideMenuController() as? UINavigationController else {
            return
        }
        
        switch BSSideMenuType(rawValue: indexPath.row + 1) {
        case .home?:
            UIView.animate(withDuration: 0,
                           animations: {
                            self.sideMenuManager?.hideSideMenuView()
            }) { (completed) in
                if completed {
                    menuNC.popToRootViewController(animated: true)
                }
            }
        case .messages?:
            if let messageVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BSMessagesViewController.self)) {
                UIView.animate(withDuration: 0, animations: {
                    self.sideMenuManager?.hideSideMenuView()
                }) { (completed) in
                    if completed {
                        menuNC.pushViewController(messageVC, animated: true)
                    }
                }
            }
        case .profile?:
            if let profileVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BSProfileViewController.self)) as? BSProfileViewController {
                UIView.animate(withDuration: 0, animations: {
                    self.sideMenuManager?.hideSideMenuView()
                }) { (completed) in
                    if completed {
                        profileVC.objUser = BSGlobalServices.shared.objCurUser
                        menuNC.pushViewController(profileVC, animated: true)
                    }
                }
            }
        case .settings?:
            if let settingsVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BSSettingsViewController.self)) {
                    UIView.animate(withDuration: 0, animations: {
                        self.sideMenuManager?.hideSideMenuView()
                    }) { (completed) in
                        if completed {
                            menuNC.pushViewController(settingsVC, animated: true)
                        }
                    }
                }
        case .logout?:
            UIView.animate(withDuration: 0, animations: {
                self.sideMenuManager?.hideSideMenuView()
            }) { (completed) in
                if completed {
                    let alertController = UIAlertController(title: "Logout", message: "Do you want to logout?", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
                        UIView.animate(withDuration: 0,
                                       animations: {
                                        alertController.dismiss(animated: true)
                        }, completion: { (completed) in
                            if completed {
                                self.logout()
                                
                                UIView.animate(withDuration: 0,
                                               animations: {
                                                self.navigationController?.popToRootViewController(animated: false)
                                }, completion: { (completed) in
                                    if completed {
                                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                            appDelegate.goToLoginScreen()
                                        }
                                    }
                                })
                            }
                        })
                    }
                    
                    let noThanksAction = UIAlertAction(title: "No thanks", style: .default) { (_) in
                        alertController.dismiss(animated: true)
                    }
                    
                    alertController.addAction(okAction)
                    alertController.addAction(noThanksAction)
                    menuNC.present(alertController, animated: true)
                }
            }
            
        default:
            break
        }
    }
}

// MARK: - UITableViewDataSource
extension BSLeftSideMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: BSSideMenuType.resuableIdentifiers[indexPath.row],
                                                                  for: indexPath)
        cell.selectionStyle = .none
        return cell
    }
}
