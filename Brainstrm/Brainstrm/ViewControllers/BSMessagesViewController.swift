//
//  BSMessagesViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/12/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import QMServices

class BSMessagesViewController: UIViewController {

    // MARK: - Properites
    @IBOutlet weak var tblContents: UITableView!
    var loadedDialogsNum = 0
    var isMoreLoadable = false
    var aryDialogs: [QBChatDialog] = []
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Tableview Setting.
        tblContents.delegate = self
        tblContents.dataSource = self
        tblContents.estimatedRowHeight = 90
        tblContents.rowHeight = UITableViewAutomaticDimension
        tblContents.separatorStyle = .none
        
        tblContents.register(BSMessageTableViewCell.nib, forCellReuseIdentifier: BSMessageTableViewCell.reuseIdentifier)
        
        QMServicesManager.instance().chatService.addDelegate(self)
        QMServicesManager.instance().authService.add(self)
        
        createDialogs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func onClickBtnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Functions
    func getMessages() {
        if QBChat.instance.isConnected == false {
            print("not connected")
            return
        }
        
        var request: [String: Any] = [:]
        request["sort_desc"] = "date_sent"
        request["skip"] = loadedDialogsNum
        
        QMServicesManager.instance().chatService.allDialogs(withPageLimit: 20,
                                                            extendedRequest: request,
                                                            iterationBlock:
            { (response, aryChatDialogs, aryParticipants, result) in
                if response.isSuccess, let aryChatDialogs = aryChatDialogs {
                    if self.loadedDialogsNum == 0 {
                        self.aryDialogs = aryChatDialogs
                        self.isMoreLoadable = aryChatDialogs.count >= 20
                    } else {
                        aryChatDialogs.forEach({ (chatDialog) in
                            if self.aryDialogs.contains(chatDialog) == false {
                                self.aryDialogs.append(chatDialog)
                            }
                        })
                        
                        self.isMoreLoadable = aryChatDialogs.count >= 20
                    }
                    self.loadedDialogsNum = self.aryDialogs.count
                    self.tblContents.reloadData()
                }
        }) { (response) in
            print(response.getErrorMessage())
        }
    }
    
    func createDialogs() {
        DispatchQueue.main.async {
            if QBChat.instance.isConnected {
                if let blockList = BSGlobalServices.shared.blockList {
                    QBChat.instance.setPrivacyList(blockList)
                }
                if let contacts = QBChat.instance.contactList?.contacts {
                    for contact in contacts {
                        
                        // If the chat dialog has been removed from the list, don't allow to recreate.
                        if let _ = BSGlobalServices.shared.blockList?.privacyItems.first(where: {$0.userID == contact.userID && $0.isAllowed == false}) {
                            continue
                        }
                        
                        let newDialog = QBChatDialog(dialogID: nil, type: .private)
                        newDialog.name = BSGlobalServices.shared.objCurUser?.topic
                        newDialog.occupantIDs = [NSNumber(value: contact.userID)]
                        
                        QBRequest.createDialog(newDialog, successBlock: { (response, chatDialog) in
                            
                        }) { (response) in
                            print(response.getErrorMessage())
                        }
                        
                    }
                }
                self.isMoreLoadable = true
                self.tblContents.reloadData()
            } else {
                if QBChat.instance.isConnected == false, let password = QMServicesManager.instance().currentUser.password {
                    let userId = QMServicesManager.instance().currentUser.id
                    QBChat.instance.connect(withUserID: userId, password: password) { (error) in
                        if let error = error {
                            print(error.localizedDescription)
                            self.showErrorMessage(title: BSApplicationInfo.name, body: "Please try again")
                        } else {
                            self.createDialogs()
                        }
                    }
                }
            }
        }
    }
}

// MARK: - UITableViewDelegate
extension BSMessagesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let chatVC = BSChatViewController()
        chatVC.dialog = aryDialogs[indexPath.row]
        navigationController?.pushViewController(chatVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == UITableViewCellEditingStyle.delete else {
            return
        }
        
        let dialog = self.aryDialogs[indexPath.row]
        
        let alertVC = UIAlertController(title: "Warning", message: "Do you really want to delete selected dialog?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (_) in
            alertVC.dismiss(animated: true, completion: nil)
        }
        alertVC.addAction(cancelAction)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .default) { (_) in
            self.showWaitHUD()
            BSQuickbloxService.shared.deleteDialog(dialog: dialog, completion: { (result, error) in
                self.dismissHUD()
                if result {
                    self.showSuccessMessage(title: BSApplicationInfo.name, body: "Successfully delete dialog!")
                } else if let error = error {
                    print(error)
                    self.showErrorMessage(title: BSApplicationInfo.name, body: "Please try again")
                }
            })
        }
        alertVC.addAction(deleteAction)
        
        self.present(alertVC, animated: true, completion: nil)
    }
}

extension BSMessagesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isMoreLoadable ? aryDialogs.count + 1 : aryDialogs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        if indexPath.row == aryDialogs.count {
            // Load more cell
            cell = tblContents.dequeueReusableCell(withIdentifier: "BSLoadMoreTableViewCell")
            self.getMessages()
        } else {
            cell = tblContents.dequeueReusableCell(withIdentifier: BSMessageTableViewCell.reuseIdentifier, for: indexPath) as? BSMessageTableViewCell
            (cell as? BSMessageTableViewCell)?.initViewWithData(chatDialog: aryDialogs[indexPath.row])
        }
        
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
}

extension BSMessagesViewController: QMChatConnectionDelegate {
    
}

extension BSMessagesViewController: QMChatServiceDelegate {
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String) {
        
    }
    
    func chatService(_ chatService: QMChatService, didAddMessagesToMemoryStorage messages: [QBChatMessage], forDialogID dialogID: String) {
        
    }
    
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        
        print("unread count: \(chatDialog.unreadMessagesCount)")
        print("dialog id: \(chatDialog.id ?? "00")")
        
        var index = -1
        for dialog in aryDialogs {
            if dialog.id == chatDialog.id {
                index += 1
                break
            }
            index += 1
        }
        
        if index > -1, aryDialogs.count > index {
            aryDialogs[index] = chatDialog
        }
        
        tblContents.reloadData()
    }
    
    func chatService(_ chatService: QMChatService, didDeleteChatDialogWithIDFromMemoryStorage chatDialogID: String) {
        var index = 0
        for dialog in aryDialogs {
            if dialog.id?.compare(chatDialogID) == ComparisonResult.orderedSame {
                aryDialogs.remove(at: index)
                break
            }
            index += 1
        }
        
        tblContents.reloadData()
    }
}

extension BSMessagesViewController: QMAuthServiceDelegate {
    
}
