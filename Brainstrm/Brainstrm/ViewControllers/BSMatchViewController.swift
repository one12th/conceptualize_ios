//
//  BSMatchViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/15/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit

class BSMatchViewController: UIViewController {

    // MARK: - Preperties
    @IBOutlet weak var imgMyAvatar: CustomImage!
    @IBOutlet weak var imgOpponentAvatar: CustomImage!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func onClickBtnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickBtnSendMessage(_ sender: Any) {
        if let messagesVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BSMessagesViewController.self)) as? BSMessagesViewController {
            navigationController?.pushViewController(messagesVC, animated: true)
        }
    }
    
    @IBAction func onClickBtnKeepSwiping(_ sender: Any) {
        onClickBtnBack(sender)
    }
    
    // MARK: - Functions
    
}
