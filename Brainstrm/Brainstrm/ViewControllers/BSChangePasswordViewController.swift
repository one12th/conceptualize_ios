//
//  BSChangePasswordViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/12/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit

class BSChangePasswordViewController: UIViewController {

    // MARK: - Properites
    @IBOutlet weak var txtCurrentPassword: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func onClickBtnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickBtnSave(_ sender: Any) {
        
        guard let password = txtPassword.text, let confirmPassword = txtConfirmPassword.text, password.count >= 8 else {
            showErrorMessage(title: BSApplicationInfo.name, body: "Invalid password. Please use capital letters, numbers and symbols. The minimal password length is 8")
            return
        }
        
        guard password.compare(confirmPassword) == ComparisonResult.orderedSame else {
            showErrorMessage(title: BSApplicationInfo.name, body: "Passwords don't match")
            return
        }
        
        guard let currentPassword = txtCurrentPassword.text else {
            showErrorMessage(title: BSApplicationInfo.name, body: "Please input the current password.")
            return
        }
        
        showWaitHUD()
        BSQuickbloxService.shared.updatePassword(oldPassword: password,
                                                 newPassword: currentPassword) { (success, error) in
                                                    self.dismissHUD()
                                                    if success {
                                                        self.showSuccessMessage(title: BSApplicationInfo.name, body: "Password changed!")
                                                        return
                                                    } else {
                                                        print(error ?? "")
                                                        self.showErrorMessage(title: BSApplicationInfo.name, body: "Failed! Review your current password and retry")
                                                    }
                                                    
        }
    }
    
    // MARK: - Functions

}
