
//
//  BSHomeViewController.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/10/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import Koloda
import QMServices
import Pulsator

class BSHomeViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var viewKoloda: KolodaView!
    @IBOutlet weak var lblDescription: UILabel!
    
    let pulsator = Pulsator()
    var timerForLoadMore: Timer?
    var aryChatDialogs: [QBChatDialog] = []
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Koloda View setting.
        viewKoloda.delegate = self
        viewKoloda.dataSource = self
        
        // Add pulse effect on koloda view.
        lblDescription.layer.superlayer?.insertSublayer(pulsator, below: lblDescription.layer)
        pulsator.radius = 240.0
        pulsator.backgroundColor = UIColor(hex: "0x005DAA").cgColor
        pulsator.start()
        
        initControls()
        NotificationCenter.default.addObserver(self, selector: #selector(handleForAppActive(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getNewMessagesCount()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layer.layoutIfNeeded()
        pulsator.position = lblDescription.layer.position
    }
    
    deinit {
        timerForLoadMore?.invalidate()
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions
    @IBAction func onClickBtnMenu(_ sender: Any) {
        sideMenuManager?.toggleSideMenuView()
    }
    
    @IBAction func onClickBtnMessage(_ sender: Any) {
        
        if let messagesVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BSMessagesViewController.self)) as? BSMessagesViewController {
            navigationController?.pushViewController(messagesVC, animated: true)
        }
    }
    
    @IBAction func onClickBtnUnlike(_ sender: Any) {
        viewKoloda.swipe(SwipeResultDirection.left)
    }
    
    @IBAction func onClickBtnLike(_ sender: Any) {
        viewKoloda.swipe(SwipeResultDirection.right)
    }
    
    @objc
    func handleForAppActive(_ sender: Any) {
        if BSGlobalServices.shared.aryCardProfiles.count > 0 {
            BSGlobalServices.shared.aryCardProfiles.removeAll()
            initControls()
        }
    }
    
    // MARK: - Functions
    func initControls() {
        btnMessage.isEnabled = false
        
        // Add delegate
        QBChat.instance.addDelegate(self)
        QMServicesManager.instance().chatService.addDelegate(self)
        if QBChat.instance.isConnected == false, let password = QMServicesManager.instance().currentUser.password {
            let userId = QMServicesManager.instance().currentUser.id
            QBChat.instance.connect(withUserID: userId, password: password) { (error) in
                if let error = error {
                    self.showErrorMessage(title: BSApplicationInfo.name, body: error.localizedDescription)
                } else {
                    self.registerForRemoteNotifications()
                    self.syncContactList()
                    self.getUsers()
                    self.getNewMessagesCount()
                    self.btnMessage.isEnabled = true
                }
            }
        } else {
            registerForRemoteNotifications()
            syncContactList()
            getUsers()
            getNewMessagesCount()
            btnMessage.isEnabled = true
        }
    }
    
    func getUsers() {
        if pulsator.isPulsating == false {
            pulsator.start()
        }
        
        if QBSession.current.currentUserID == 0 {
            // Show login screen
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.goToLoginScreen()
            }
            return
        }
        
        if let curUser = BSGlobalServices.shared.objCurUser {
            BSQuickbloxService
                .shared
                .getPendingUsers(currentUser: curUser,
                                 pendingUsers: BSGlobalServices.shared.aryPendingUsers) {
                                    (result, response, error) in
                                    
                                    if result == false, let error = error {
                                        print(error)
                                        self.showErrorMessage(title: BSApplicationInfo.name, body: "Please try again")
                                        return
                                    }
                                    
                                    if let users = response {
                                        users.forEach({ (profileData) in
                                            BSGlobalServices.shared.aryCardProfiles.append(BSUserModel(customData: profileData))
                                        })
                                        
                                        self.viewKoloda.reloadData()
                                        if BSGlobalServices.shared.aryCardProfiles.count == 0 {
                                            if self.pulsator.isPulsating == false {
                                                self.pulsator.start()
                                            }
                                            
                                            self.lblDescription.text = "No more connections"
                                            self.timerForLoadMore = Timer.scheduledTimer(withTimeInterval: 60, repeats: false, block: { (timer) in
                                                self.getUsers()
                                            })
                                        } else {
                                            if self.pulsator.isPulsating {
                                                self.pulsator.stop()
                                            }
                                            self.lblDescription.text = "Searching for more connections"
                                            self.timerForLoadMore?.invalidate()
                                        }
                                    }
            }
        }
    }
    
    func likeUser(userData: BSUserModel) {
        
        // - Check pending list.
        if let userId = userData.id, let aryPendingUsers = BSGlobalServices.shared.aryPendingUsers,
            aryPendingUsers.first(where: {UInt($0) == userId}) != nil {
            
            // A: If the user is in the pending list
            //  a. Remove from the pending list
            BSGlobalServices.shared.removeUserFromPendingList(userId: userId)
            
            //  b. Add the user to my contact
            BSGlobalServices.shared.objCurUser?.matched?.append(userId)
            
            //  c. Add the user to the swiped list
            BSGlobalServices.shared.objCurUser?.swiped?.append(userId)
            
            //  d. Increase the number of likes
            BSGlobalServices.shared.objCurUser?.liked = (BSGlobalServices.shared.objCurUser?.liked ?? 0) + UInt(1)
            
            //  e. Update my profile.
            BSQuickbloxService.shared.updateProfile { (success, profileData, error) in
                if success, let profileData = profileData {
                    BSGlobalServices.shared.objCurUser = BSUserModel(customData: profileData)
                    BSGlobalServices.shared.saveCurrentUser()
                    
                    
                    //  f. Confirm Add contact request.
                    QBChat.instance.confirmAddContactRequest(userId) { (error) in
                        if let error = error {
                            print(error.localizedDescription)
                        } else {
                            //  g. Create a new dialog.
                            
                            //  h. Show match screen.
                            if let matchVC = self.storyboard?.instantiateViewController(withIdentifier: String(describing: BSMatchViewController.self)) as? BSMatchViewController {
                                self.navigationController?.pushViewController(matchVC, animated: true)
                            }
                        }
                    }
                    
                } else if let error = error {
                    print(error)
                }
            }
        } else if let userId = userData.id {
            // B: If the user is not in the pending list
            //  a. Send add contact request to user.
            QBChat.instance.addUser(toContactListRequest: userId) { (error) in
                if let error = error {
                    print(error.localizedDescription)
                }
            }
            
            //  b. Add the user to swiped list
            BSGlobalServices.shared.objCurUser?.swiped?.append(userId)
            
            //  d. Update my profile.
            BSQuickbloxService.shared.updateProfile { (success, profileData, error) in
                if success, let profileData = profileData {
                    BSGlobalServices.shared.objCurUser = BSUserModel(customData: profileData)
                    BSGlobalServices.shared.saveCurrentUser()
                } else if let error = error {
                    print(error)
                }
            }
        }
    }
    
    func unlikeUser(userData: BSUserModel) {
        
        // - Check pending list.
        if let userId = userData.id, let aryPendingUsers = BSGlobalServices.shared.aryPendingUsers,
            aryPendingUsers.first(where: {UInt($0) == userId}) != nil {
            // A: If the user is in the pending list
            
            //  a. Remove from the pending list.
            BSGlobalServices.shared.removeUserFromPendingList(userId: userId)
            
            //  b. Send reject contact request to user.
            QBChat.instance.rejectAddContactRequest(userId) { (error) in
                if let error = error {
                    print(error.localizedDescription)
                }
            }
            
            //  c. Add the user into the swiped list.
            BSGlobalServices.shared.objCurUser?.swiped?.append(userId)
            
            //  d. Update my profile.
            BSQuickbloxService.shared.updateProfile { (success, profileData, error) in
                if success, let profileData = profileData {
                    BSGlobalServices.shared.objCurUser = BSUserModel(customData: profileData)
                    BSGlobalServices.shared.saveCurrentUser()
                } else if let error = error {
                    print(error)
                }
            }
            
            //  *. Remove from card profile.
        } else if let userId = userData.id {
            // B: If the user is not in the pening list
            
            //  a. Add the user into the swiped list.
            BSGlobalServices.shared.objCurUser?.swiped?.append(userId)
            
            //  b. Update my profile.
            BSQuickbloxService.shared.updateProfile { (success, profileData, error) in
                if success, let profileData = profileData {
                    BSGlobalServices.shared.objCurUser = BSUserModel(customData: profileData)
                    BSGlobalServices.shared.saveCurrentUser()
                } else if let error = error {
                    print(error)
                }
            }
            
            //  *. Remove from card profile.
        }
    }
    
    func addUserToMatchList(userId: UInt) {
        if BSGlobalServices.shared.objCurUser?.matched?.first(where: {$0 == userId}) == nil {
            BSGlobalServices.shared.objCurUser?.matched?.append(userId)
            BSQuickbloxService.shared.updateProfile { (success, profileData, error) in
                if success, let profileData = profileData {
                    BSGlobalServices.shared.objCurUser = BSUserModel(customData: profileData)
                    BSGlobalServices.shared.saveCurrentUser()
                    
                    // 3. Show local notification about this.
                    self.showSuccessMessage(title: BSApplicationInfo.name, body: "You have a new match!")
                } else if let error = error {
                    print(error)
                }
            }
        }
    }
    
    func syncContactList() {
        if QBChat.instance.isConnected {
            
            QBChat.instance.retrievePrivacyList(withName: BSQuickbloxInfo.privacyListForBlockUsers)
            
            if let contacts = QBChat.instance.contactList?.contacts {
                for contactListItem in contacts {
                    if BSGlobalServices.shared.objCurUser?.matched?.first(where: {$0 == contactListItem.userID}) == nil {
                        BSGlobalServices.shared.objCurUser?.matched?.append(contactListItem.userID)
                    }
                    
                    if BSGlobalServices.shared.objCurUser?.swiped?.first(where: {$0 == contactListItem.userID}) == nil {
                        BSGlobalServices.shared.objCurUser?.swiped?.append(contactListItem.userID)
                    }
                }
            }
            
            if let currentPendingUsers = BSGlobalServices.shared.objCurUser?.pending {
                for userId in currentPendingUsers {
                    if BSGlobalServices.shared.aryPendingUsers?.first(where: {$0 == userId}) == nil && BSGlobalServices.shared.objCurUser?.matched?.first(where: {$0 == UInt(userId)}) == nil {
                        BSGlobalServices.shared.aryPendingUsers?.append("\(userId)")
                    }
                }
            }
            
            if let pendingApprovals = QBChat.instance.contactList?.pendingApproval {
                for contactListItem in pendingApprovals {
                    if BSGlobalServices.shared.aryPendingUsers?.first(where: {UInt($0) == contactListItem.userID}) == nil && BSGlobalServices.shared.objCurUser?.matched?.first(where: {$0 == contactListItem.userID}) == nil {
                        BSGlobalServices.shared.aryPendingUsers?.append("\(contactListItem.userID)")
                    }
                }
            }
            
            if let aryPendingUsers = BSGlobalServices.shared.aryPendingUsers, aryPendingUsers.count > 0 {
                BSGlobalServices.shared.objCurUser?.pending = aryPendingUsers
                BSGlobalServices.shared.saveCurrentUser()
            }
        }
    }
    
    func registerForRemoteNotifications() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.registerForRemoteNotifications()
        }
    }
    
    func getNewMessagesCount() {
        
        BSQuickbloxService.shared.getMessages(skip: 0, limit: 20) { (success, needsMoreLoad, chatDialogs, participants) in
            if success, let chatDialogs = chatDialogs {
                var dialogIds: [String] = []
                for chatDialog in chatDialogs {
                    if let id = chatDialog.id {
                        dialogIds.append(id)
                    }
                }
                
                BSQuickbloxService.shared.getUnreadMessagesCount(dialogIds: Set(dialogIds)) { (success, count) in
                    if success, count > 0 {
                        self.btnMessage.isSelected = true
                    } else {
                        self.btnMessage.isSelected = false
                    }
                }
            } else {
                self.btnMessage.isSelected = false
            }
        }
    }
}

// MARK: - KolodaViewDelegate
extension BSHomeViewController: KolodaViewDelegate {
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        // Go to profile screen.
        if BSGlobalServices.shared.aryCardProfiles.count > index {
            let objUser = BSGlobalServices.shared.aryCardProfiles[index]
            if let profileVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BSProfileViewController.self)) as? BSProfileViewController {
                profileVC.objUser = objUser
                navigationController?.pushViewController(profileVC, animated: true)
            }
        }
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        if direction == .right || direction == .topRight || direction == .bottomRight  {
            likeUser(userData: BSGlobalServices.shared.aryCardProfiles[index])
        } else {
            unlikeUser(userData: BSGlobalServices.shared.aryCardProfiles[index])
        }
        
        if (BSGlobalServices.shared.aryCardProfiles.count - 1) == index {
            getUsers()
        }
    }
}

// MARK: - KolodaViewDataSource
extension BSHomeViewController: KolodaViewDataSource {
    
    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
        return BSGlobalServices.shared.aryCardProfiles.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let view = Bundle.main.loadNibNamed(String(describing: BSTopicCardView.self), owner: nil, options: [:])?.first as! BSTopicCardView
        view.initWithData(userData: BSGlobalServices.shared.aryCardProfiles[index])
        return view
    }
}

// MARK: - QBChatDelegate
extension BSHomeViewController: QBChatDelegate {
    
    func chatDidReceiveContactAddRequest(fromUser userID: UInt) {
        if let _ = BSGlobalServices.shared.aryPendingUsers?.first(where: {UInt($0) == userID}) {
            
            // Remove the user from pending list.
            BSGlobalServices.shared.removeUserFromPendingList(userId: userID)
            
            // Add this user to profile contact list and update my profile.
            self.addUserToMatchList(userId: userID)
        } else {
            // Add this user to pending list.
            BSGlobalServices.shared.aryPendingUsers?.append("\(userID)")
        }
    }
    
    func chatDidReceiveAcceptContactRequest(fromUser userID: UInt) {
        // Add the user to profile contact list and update my profile.
        self.addUserToMatchList(userId: userID)
    }
    
    func chatDidReceiveRejectContactRequest(fromUser userID: UInt) {
        // TODO: 1. Remove this user from pening contact list and update my profile.
        
    }
    
    func chatDidReceive(_ message: QBChatMessage) {
        
    }
    
    func chatDidReceive(_ privacyList: QBPrivacyList) {
        BSGlobalServices.shared.blockList = privacyList
        QBChat.instance.setPrivacyList(BSGlobalServices.shared.blockList)
    }
    
    func chatDidNotReceivePrivacyList(withName name: String, error: Error) {
        
    }
}

// MARK: - QMChatServiceDelegate
extension BSHomeViewController: QMChatServiceDelegate {
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String) {
        getNewMessagesCount()
    }
}

// MARK: - QMChatConnectionDelegate
extension BSHomeViewController: QMChatConnectionDelegate {
    
}
