//
//  BSUserModel.swift
//  Brainstrm
//
//  Created by jcooperation0137 on 7/9/18.
//  Copyright © 2018 Zoar Technology LLC. All rights reserved.
//

import UIKit
import Quickblox
import ObjectMapper

class BSUserModel: Mappable {
    
    var id:                 UInt?
    var name:               String?
    var bio:                String?
    var matched:            [UInt]?
    var swiped:             [UInt]?
    var topic:              String?
    var gender:             UInt?
    var updatedAt:          Date?
    var age:                UInt?
    var images:             [UInt]?
    var likeLimitLifted:    Date?
    var liked:              UInt?
    var pending:            [String]?
    var objectId:           String?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name                <- map[BSQuickbloxProfileClassKeyName.name]
        bio                 <- map[BSQuickbloxProfileClassKeyName.bio]
        matched             <- map[BSQuickbloxProfileClassKeyName.matched]
        swiped              <- map[BSQuickbloxProfileClassKeyName.swiped]
        topic               <- map[BSQuickbloxProfileClassKeyName.userTopic]
        gender              <- map[BSQuickbloxProfileClassKeyName.gender]
        liked               <- map[BSQuickbloxProfileClassKeyName.liked]
        age                 <- map[BSQuickbloxProfileClassKeyName.age]
        images              <- map[BSQuickbloxProfileClassKeyName.images]
        likeLimitLifted     <- map[BSQuickbloxProfileClassKeyName.likeLimitLifted]
        liked               <- map[BSQuickbloxProfileClassKeyName.liked]
        pending             <- map[BSQuickbloxProfileClassKeyName.pending]
    }
    
    init(customData: QBCOCustomObject) {
        objectId = customData.id
        id = customData.userID
        
        let newSelf = Mapper<BSUserModel>().map(JSON: customData.fields as! [String : Any])
        name = newSelf?.name
        bio = newSelf?.bio
        matched = newSelf?.matched ?? []
        swiped = newSelf?.swiped ?? []
        topic = newSelf?.topic
        gender = newSelf?.gender
        age = newSelf?.age
        images = newSelf?.images ?? []
        likeLimitLifted = newSelf?.likeLimitLifted
        liked = newSelf?.liked
        pending = newSelf?.pending ?? []
        updatedAt = customData.updatedAt
    }
    
    func getQuickbloxObject() -> QBCOCustomObject {
        
        let objProfile = QBCOCustomObject()
        objProfile.className = BSQuickbloxClassName.profile
        
        if let objectId = objectId {
            objProfile.id = objectId
        }
        
        if let id = id {
            objProfile.userID = id
        }
        
        if let name = name {
            objProfile.fields.setValue(name, forKey: BSQuickbloxProfileClassKeyName.name)
        }
        
        if let bio = bio {
            objProfile.fields.setValue(bio, forKey: BSQuickbloxProfileClassKeyName.bio)
        }
        
        if let matched = matched {
            objProfile.fields.setValue(matched, forKey: BSQuickbloxProfileClassKeyName.matched)
        }
        
        if let swiped = swiped {
            objProfile.fields.setValue(swiped, forKey: BSQuickbloxProfileClassKeyName.swiped)
        }
        
        if let topic = topic {
            objProfile.fields.setValue(topic, forKey: BSQuickbloxProfileClassKeyName.userTopic)
        }
        
        if let gender = gender {
            objProfile.fields.setValue(gender, forKey: BSQuickbloxProfileClassKeyName.gender)
        }
        
        if let age = age {
            objProfile.fields.setValue(age, forKey: BSQuickbloxProfileClassKeyName.age)
        }
        
        if let images = images {
            objProfile.fields.setValue(images, forKey: BSQuickbloxProfileClassKeyName.images)
        }
        
        if let likeLimitLifted = likeLimitLifted {
            objProfile.fields.setValue(likeLimitLifted, forKey: BSQuickbloxProfileClassKeyName.likeLimitLifted)
        }
        
        if let liked = liked {
            objProfile.fields.setValue(liked, forKey: BSQuickbloxProfileClassKeyName.liked)
        }
        
        if let pending = pending {
            objProfile.fields.setValue(pending, forKey: BSQuickbloxProfileClassKeyName.pending)
        }
        
        if let updatedAt = updatedAt {
            objProfile.fields.setValue(updatedAt, forKey: BSQuickbloxProfileClassKeyName.updatedAt)
        }
        
        return objProfile
    }
}
