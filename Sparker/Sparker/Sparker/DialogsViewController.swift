//
//  DialogsViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 04/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class DialogsViewController: UITableViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        //self.tableView.layoutMargins = UIEdgeInsets(top: 0, left: 0.2*self.view.bounds.size.width, bottom: 0, right: 0)
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 100.0, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self.tableView, selector: #selector(UICollectionView.reloadData), name: NSNotification.Name(rawValue: "com.samuelharrison.utility.dialogsUpdatedNotification"), object: nil)
        
        var image = UIImage(named: AppConfig.chatsScreenNavigationBarTitleImage)
        if(AppConfig.chatsScreenNavigationBarTitleImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        self.navigationItem.titleView = UIImageView(image: image)
        self.navigationItem.titleView?.tintColor = AppConfig.chatsScreenNavigationBarTitleImageColor
        
        let backButton = UIBarButtonItem(image: UIImage(named: AppConfig.chatsScreenNavigationBarBackImage)?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), style: UIBarButtonItemStyle.plain, target: self, action: #selector(DialogsViewController.goBack))
        backButton.tintColor = AppConfig.chatsScreenNavigationBarBackColor
        self.navigationItem.leftBarButtonItem = backButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.set(Date(), forKey: "DialogsLastViewedDate")
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.set(Date(), forKey: "DialogsLastViewedDate")
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ChatService.sharedInstance.sortedDialogs.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "dialogCell") as? DialogCell
        if(cell == nil){
            cell = DialogCell(style: UITableViewCellStyle.value1, reuseIdentifier: "dialogCell")
        }
        let dialog = ChatService.sharedInstance.sortedDialogs[indexPath.row]
        let profile = ChatService.sharedInstance.getDialogOpponent(dialog)
        cell!.configureCell(profile, dialog: dialog)
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatViewController = ChatViewController(dialog: ChatService.sharedInstance.sortedDialogs[indexPath.row])
        self.navigationController?.pushViewController(chatViewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == UITableViewCellEditingStyle.delete){
            ChatService.sharedInstance.deleteDialog(ChatService.sharedInstance.sortedDialogs[indexPath.row])
        }
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
}
