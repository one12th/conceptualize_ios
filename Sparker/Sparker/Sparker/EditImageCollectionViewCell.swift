//
//  EditImageCollectionViewCell.swift
//  Sparker
//
//  Created by Samuel Harrison on 09/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class EditImageCollectionViewCell: UICollectionViewCell {
    
    var imageView = UIImageView()
    var imageButton = UIButton()
    var wobbling = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.imageView.frame = frame
        self.imageView.clipsToBounds = true
        self.imageView.contentMode = UIViewContentMode.scaleAspectFill
        self.contentView.addSubview(self.imageView)
        
        self.imageButton.layer.cornerRadius = 18
        self.imageButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.imageButton.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.imageButton.imageView?.clipsToBounds = false
        self.contentView.addSubview(self.imageButton)
        
        self.contentView.clipsToBounds = true
        
        self.backgroundColor = AppConfig.editProfileScreenBlankImageColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func configureCell(_ imageID:UInt?){
        if(imageID == nil){
            self.imageButton.backgroundColor = AppConfig.editProfileScreenAddImageButtonBackgroundColor
            var image = UIImage(named: AppConfig.editProfileScreenAddImageButtonImage)
            if(AppConfig.editProfileScreenAddImageButtonImageOverrideColor){
                image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            }
            self.imageButton.setImage(image, for: UIControlState())
            self.imageButton.tintColor = AppConfig.editProfileScreenAddImageButtonImageColor
            self.imageView.image = nil
        }else{
            self.imageButton.backgroundColor = AppConfig.editProfileScreenRemoveImageButtonBackgroundColor
            var image = UIImage(named: AppConfig.editProfileScreenRemoveImageButtonImage)
            if(AppConfig.editProfileScreenRemoveImageButtonImageOverrideColor){
                image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            }
            self.imageButton.setImage(image, for: UIControlState())
            self.imageButton.tintColor = AppConfig.editProfileScreenRemoveImageButtonImageColor
            if let urlString = QBCBlob.privateUrl(forID: imageID!){
                if let url = URL(string: urlString){
                    self.imageView.setImageWith(url, placeholderImage: UIImage(named: "PlaceholderImage"))
                }
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imageView.frame = self.bounds
        self.imageButton.frame = CGRect(x: frame.size.width - 30, y: frame.size.height - 30, width: 36, height: 36)
    }
    
    func startWobbling(){
        self.wobbling = true
        self.transform = CGAffineTransform.identity.rotated(by: -CGFloat(.pi / 50.0))
        UIView.animate(withDuration: 0.15, delay: 0.0, options: [UIViewAnimationOptions.allowUserInteraction, UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse], animations: { () -> Void in
            self.transform = CGAffineTransform.identity.rotated(by: CGFloat(CGFloat.pi / 50))
            }, completion: nil)
    }

    func stopWobbling(){
        self.wobbling = false
        UIView.animate(withDuration: 0.15, delay: 0.0, options: [UIViewAnimationOptions.allowUserInteraction, UIViewAnimationOptions.beginFromCurrentState, UIViewAnimationOptions.curveLinear], animations: { () -> Void in
            self.transform = CGAffineTransform.identity
            }, completion: nil)
    }
    
}
