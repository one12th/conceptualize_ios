//
//  DraggableView.swift
//  Sparker
//
//  Created by Samuel Harrison on 04/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

let ACTION_MARGIN:CGFloat = 120
let SCALE_STRENGTH:CGFloat = 4
let SCALE_MAX:CGFloat = 0.93
let ROTATION_MAX:CGFloat = 1
let ROTATION_STRENGTH:CGFloat = 320
let ROTATION_ANGLE:CGFloat = CGFloat(.pi/8.0)

import UIKit
import FBSDKCoreKit

protocol DraggableViewDelegate{
    func cardSwipedLeft(_ card:DraggableView)
    func cardSwipedRight(_ card:DraggableView)
    func didTapCard(_ card:DraggableView)
    func showLimitAlert()
}

class DraggableView: UIView {
    
    var containerView = UIView()
    var profile:UserProfile
    var delegate:DraggableViewDelegate!
    var overlayView: OverlayView!
    var tapGestureRecognizer: UITapGestureRecognizer!
    var panGestureRecognizer: UIPanGestureRecognizer!
    var information = UILabel()
    var xFromCenter:CGFloat = 0
    var yFromCenter:CGFloat = 0
    var originalPoint:CGPoint = CGPoint.zero
    var imageView = UIImageView()
    var nameAgeLabel = UILabel()
    var distanceLabel = UILabel()
    var lastActiveLabel = UILabel()
    var bioHeadingLabel = UILabel()
    var bioLabel = UILabel()
    
    init(frame:CGRect, profile:UserProfile, rewound:Bool){
        self.profile = profile
        super.init(frame: frame)
        
        self.addSubview(self.containerView)
        self.containerView.clipsToBounds = true
        
        self.imageView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height - 66)
        self.imageView.clipsToBounds = true
        self.imageView.contentMode = UIViewContentMode.scaleAspectFill
        
        print(profile.images.count)
        
        if(profile.images.count > 0){
            if let urlString = QBCBlob.privateUrl(forID: profile.images[0]){
                if let url = URL(string: urlString){
                    self.imageView.setImageWith(url, placeholderImage: UIImage(named: "PlaceholderImage"))
                }
            }
        }else{
            self.imageView.image = UIImage(named: "PlaceholderImage")
        }
        self.containerView.addSubview(self.imageView)
        
        //self.requestImage()
        
        self.nameAgeLabel.frame = CGRect(x: 16, y: frame.size.height - 66, width: frame.size.width, height: 66)
        self.nameAgeLabel.textColor = AppConfig.cardTextColor
        self.nameAgeLabel.font = AppConfig.cardFont
        
        //self.nameAgeLabel.text = "\(profile.userID) : "
        self.nameAgeLabel.text = ""
        if(profile.name != ""){
            self.nameAgeLabel.text! += profile.name+", "
        }
        self.nameAgeLabel.text! += "\(profile.age!)"
        self.containerView.addSubview(self.nameAgeLabel)
        
        self.backgroundColor = AppConfig.cardBackgroundColor
        self.containerView.backgroundColor = AppConfig.cardBackgroundColor
        
        self.overlayView = OverlayView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        self.overlayView.alpha = 0
        self.containerView.addSubview(self.overlayView)
        
        self.setupView()
        
        self.originalPoint = self.center
        
        self.panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(DraggableView.beingDragged(_:)))
        self.tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DraggableView.didTap(_:)))
        
        if(rewound){
            self.center = CGPoint(x: -600, y: self.center.y)
            //self.transform = CGAffineTransformMakeRotation(-1)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addGestureRecognizers(){
        self.addGestureRecognizer(self.tapGestureRecognizer)
        self.addGestureRecognizer(self.panGestureRecognizer)
    }
    
    func removeGestureRecognizers(){
        self.removeGestureRecognizer(self.tapGestureRecognizer)
        self.removeGestureRecognizer(self.panGestureRecognizer)
    }
    
    func setupView(){
        self.containerView.frame = self.bounds
        self.containerView.layer.cornerRadius = AppConfig.cardCornerRadius
        self.layer.borderColor = AppConfig.cardBorderColor.cgColor
        self.layer.borderWidth = AppConfig.cardBorderWidth
        self.layer.cornerRadius = AppConfig.cardCornerRadius
        self.layer.shadowRadius = 3.0
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        //self.layer.borderWidth = 1.0
        //self.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    @objc func didTap(_ gestureRecognizer:UITapGestureRecognizer){
        if(gestureRecognizer.state == UIGestureRecognizerState.ended){
            self.delegate.didTapCard(self)
        }
    }
    
    @objc func beingDragged(_ gestureRecognizer:UIPanGestureRecognizer){
        self.xFromCenter = gestureRecognizer.translation(in: self).x
        self.yFromCenter = gestureRecognizer.translation(in: self).y
        
        switch(gestureRecognizer.state){
        case UIGestureRecognizerState.began:
            self.originalPoint = self.center
            break
            
        case UIGestureRecognizerState.changed:
            let rotationStrength = min(xFromCenter/ROTATION_STRENGTH, ROTATION_MAX)
            let rotationAngle = ROTATION_ANGLE * rotationStrength
            let scale = max(1 - fabs(rotationStrength) / SCALE_STRENGTH, SCALE_MAX)
            
            self.center = CGPoint(x: self.originalPoint.x + xFromCenter, y: self.originalPoint.y + yFromCenter)
            
            let transform = CGAffineTransform(rotationAngle: rotationAngle)
            
            let scaleTransform = transform.scaledBy(x: scale, y: scale)
            
            self.transform = scaleTransform
            self.updateOverlay(xFromCenter)
            
            break
            
        case UIGestureRecognizerState.ended:
            self.afterSwipeAction()
            break
            
        default:
            break
        }
    }
    
    func updateOverlay(_ distance:CGFloat){
        if(distance > 0){
            self.overlayView.mode = CGOverlayViewMode.right
        }else{
            self.overlayView.mode = CGOverlayViewMode.left
        }
        overlayView.alpha = min(fabs(distance)/100, 0.8)
    }
    
    func afterSwipeAction(){
        if(self.xFromCenter > ACTION_MARGIN){
            self.rightAction()
        }else if(self.xFromCenter < -ACTION_MARGIN){
            self.leftAction()
        }else{
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.center = self.originalPoint
                self.transform = CGAffineTransform.identity
                self.overlayView.alpha = 0
            })
        }
    }
    
    func rightAction(){
        if(!Utility.sharedInstance.allowedToLike()){
            self.delegate.showLimitAlert()
            self.rewindAction()
            return
        }
        let finishPoint = CGPoint(x: 500, y: 2*self.yFromCenter + self.originalPoint.y)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.center = finishPoint
            }, completion: { (complete) -> Void in
                self.overlayView.alpha = 0
                self.removeFromSuperview()
        }) 
        self.delegate.cardSwipedRight(self)
    }
    
    func leftAction(){
        let finishPoint = CGPoint(x: -500, y: 2*self.yFromCenter + self.originalPoint.y)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.center = finishPoint
            }, completion: { (complete) -> Void in
                self.overlayView.alpha = 0
                self.removeFromSuperview()
        }) 
        self.delegate.cardSwipedLeft(self)
    }
    
    func rewindAction(){
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.center = self.originalPoint
            self.transform = CGAffineTransform.identity
            self.overlayView.alpha = 0
            }, completion: { (complete) -> Void in
                //self.removeFromSuperview()
        }) 
    }
    
    func rightClickAction(){
        if(!Utility.sharedInstance.allowedToLike()){
            self.delegate.showLimitAlert()
            return
        }
        let finishPoint = CGPoint(x: 600, y: self.center.y)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.center = finishPoint
            self.transform = CGAffineTransform(rotationAngle: 1)
            }, completion: { (complete) -> Void in
                self.overlayView.alpha = 0
                self.removeFromSuperview()
        }) 
        self.delegate.cardSwipedRight(self)
    }
    
    func leftClickAction(){
        let finishPoint = CGPoint(x: -600, y: self.center.y)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.center = finishPoint
            self.transform = CGAffineTransform(rotationAngle: -1)
            }, completion: { (complete) -> Void in
                self.overlayView.alpha = 0
                self.removeFromSuperview()
        }) 
        self.delegate.cardSwipedLeft(self)
    }
    
    /*func requestImage(){
        if(self.profile.images.count > 0 ){
            print(profile.images[0])
            let request = FBSDKGraphRequest(graphPath: "/\(profile.images[0])", parameters: ["fields":"id,images"], HTTPMethod: "GET")
            request.startWithCompletionHandler { (connection, result, error) -> Void in
                if(error == nil){
                    print("Success getting photo from FB")
                    if let result = result as? NSDictionary{
                        if let imageSrc = result["images"]![0]["source"] as? String{
                            self.imageView.setImageWithURL(NSURL(string: imageSrc)!, placeholderImage: UIImage())
                        }
                    }
                }else{
                    print("Error getting photo from FB")
                    //print(error)
                }
            }
        }
    }*/
}
