//
//  OverlayView.swift
//  Sparker
//
//  Created by Samuel Harrison on 04/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

enum CGOverlayViewMode{
    case left
    case right
}

import UIKit

class OverlayView: UIView {
    
    var overlayButtonView: UIButton!
    var imageView: UIImageView!
    var mode = CGOverlayViewMode.left {
        didSet{
            if(self.mode == CGOverlayViewMode.left){
                var image = UIImage(named: AppConfig.cardDislikeOverlayImage)
                if(AppConfig.cardDislikeOverlayImageOverrideColor){
                    image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                }
                self.overlayButtonView.setImage(image, for: UIControlState())
                self.overlayButtonView.frame = CGRect(x: self.frame.size.width - 150, y: 50, width: 100, height: 100)
                self.overlayButtonView.tintColor = AppConfig.cardDislikeOverlayImageColor
                self.overlayButtonView.layer.borderColor = AppConfig.cardDislikeOverlayBorderColor.cgColor
            }else{
                var image = UIImage(named: AppConfig.cardLikeOverlayImage)
                if(AppConfig.cardLikeOverlayImageOverrideColor){
                    image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                }
                self.overlayButtonView.setImage(image, for: UIControlState())
                self.overlayButtonView.frame = CGRect(x: 50, y: 50, width: 100, height: 100)
                self.overlayButtonView.tintColor = AppConfig.cardLikeOverlayImageColor
                self.overlayButtonView.layer.borderColor = AppConfig.cardLikeOverlayBorderColor.cgColor
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.overlayButtonView = UIButton()
        var image = UIImage(named: AppConfig.cardDislikeOverlayImage)
        if(AppConfig.cardDislikeOverlayImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        self.overlayButtonView.setImage(image, for: UIControlState())
        self.overlayButtonView.imageEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        self.overlayButtonView.tintColor = AppConfig.cardDislikeOverlayImageColor
        self.overlayButtonView.layer.cornerRadius = 50
        self.overlayButtonView.layer.borderWidth = AppConfig.cardDislikeOverlayBorderWidth
        self.overlayButtonView.layer.borderColor = AppConfig.cardDislikeOverlayBorderColor.cgColor
        self.addSubview(self.overlayButtonView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if(self.mode == CGOverlayViewMode.left){
            self.overlayButtonView.frame = CGRect(x: self.frame.size.width - 150, y: 50, width: 100, height: 100)
        }else{
            self.overlayButtonView.frame = CGRect(x: 50, y: 50, width: 100, height: 100)
        }
    }
    
}
