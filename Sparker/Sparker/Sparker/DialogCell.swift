//
//  DialogCell.swift
//  Sparker
//
//  Created by Samuel Harrison on 11/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import FormatterKit

class DialogCell: UITableViewCell {
    
    let profileImageView = UIImageView()
    let dialogName = UILabel()
    let lastMessageText = UILabel()
    let lastMessageTime = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.profileImageView.clipsToBounds = true
        self.profileImageView.contentMode = UIViewContentMode.scaleAspectFill
        self.contentView.addSubview(self.profileImageView)
        
        self.dialogName.font = AppConfig.chatsScreenNameFont
        self.dialogName.textColor = AppConfig.chatsScreenNameTextColor
        self.contentView.addSubview(self.dialogName)
        self.lastMessageText.textColor = AppConfig.chatsScreenLastMessageTextColor
        self.lastMessageText.font = AppConfig.chatsScreenLastMessageFont
        self.lastMessageText.numberOfLines = 1
        self.contentView.addSubview(self.lastMessageText)
        self.lastMessageTime.textColor = AppConfig.chatsScreenLastMessageTimeTextColor
        self.lastMessageTime.textAlignment = NSTextAlignment.right
        self.lastMessageTime.font = AppConfig.chatsScreenLastMessageTimeFont
        self.contentView.addSubview(self.lastMessageTime)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.profileImageView.frame = CGRect(x: 16, y: 8, width: self.bounds.size.height - 16, height: self.bounds.size.height - 16)
        self.profileImageView.layer.cornerRadius = (self.bounds.size.height - 16.0)/2.0
        self.profileImageView.layer.borderWidth = AppConfig.chatsScreenProfileImageBorderWidth
        self.profileImageView.layer.borderColor = AppConfig.chatsScreenProfileImageBorderColor.cgColor
        dialogName.frame = CGRect(x: profileImageView.frame.size.width + 32, y: self.bounds.size.height/2 - 22, width: self.bounds.size.width - profileImageView.frame.size.width - 32 - 75, height: 22)
        lastMessageText.frame = CGRect(x: profileImageView.frame.size.width + 32, y: self.bounds.size.height/2 + 11, width: self.bounds.size.width - profileImageView.frame.size.width - 32 - 16, height: 22)
        lastMessageTime.frame = CGRect(x: self.bounds.size.width - 116, y: self.bounds.size.height/2 - 22, width: 100, height: 22)
    }
    
    func configureCell(_ profile:UserProfile?, dialog:QBChatDialog){
        
        self.profileImageView.image = UIImage(named: "PlaceholderImage")
        if(profile != nil){
            self.imageView?.image = nil
            if(profile!.images.count > 0){
                if let urlString = QBCBlob.privateUrl(forID: profile!.images[0]){
                    if let url = URL(string: urlString){
                        self.profileImageView.setImageWith(url, placeholderImage: UIImage(named: "PlaceholderImage"))
                    }
                }
            }
            self.dialogName.text = profile!.name
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"
        
        self.lastMessageText.text = dialog.lastMessageText
        if(dialog.lastMessageText == nil && dialog.createdAt != nil){
            let matchDate = dateFormatter.string(from: dialog.createdAt!)
            self.lastMessageText.text = "Matched on \(matchDate)"
        }
        
        var dateString = ""
        if let time = dialog.lastMessageDate{
            
            let cal = Calendar.current
            let today = cal.date(from: (cal as NSCalendar).components([NSCalendar.Unit.era,NSCalendar.Unit.year,NSCalendar.Unit.month,NSCalendar.Unit.day], from: Date()))
            let yesterday = cal.date(from: (cal as NSCalendar).components([NSCalendar.Unit.era,NSCalendar.Unit.year,NSCalendar.Unit.month,NSCalendar.Unit.day], from: Date(timeIntervalSinceNow: -60*60*24)))
            let lastSeen = cal.date(from: (cal as NSCalendar).components([NSCalendar.Unit.era,NSCalendar.Unit.year,NSCalendar.Unit.month,NSCalendar.Unit.day], from: time))
            
            if(today == lastSeen){
                dateFormatter.dateFormat = "HH:mm"
                dateString = dateFormatter.string(from: time)
            }else if(yesterday == lastSeen){
                dateString = "Yesterday"
            }else{
                
                let thisYear = cal.date(from: (cal as NSCalendar).components([NSCalendar.Unit.era,NSCalendar.Unit.year], from: Date()))
                let dateYear = cal.date(from: (cal as NSCalendar).components([NSCalendar.Unit.era,NSCalendar.Unit.year], from: time))
                
                if(thisYear == dateYear){
                    dateFormatter.dateFormat = "EEE d MMM"
                }else{
                    dateFormatter.dateFormat = "EEE d MMM yyyy"
                }
                dateString = dateFormatter.string(from: time)
            }
        }
        self.lastMessageTime.text = dateString
        
        if(dialog.unreadMessagesCount > 0 && dialog.lastMessageUserID != QBSession.current().currentUser!.id){
            self.lastMessageText.font = AppConfig.chatsScreenLastMessageUnreadFont
            self.lastMessageText.textColor = AppConfig.chatsScreenLastMessageUnreadTextColor
            self.backgroundColor = AppConfig.chatsScreenChatCellUnreadBackgroundColor
        }else{
            self.lastMessageText.font = AppConfig.chatsScreenLastMessageFont
            self.lastMessageText.textColor = AppConfig.chatsScreenLastMessageTextColor
            self.backgroundColor = AppConfig.chatsScreenChatCellBackgroundColor
        }
    }

}
