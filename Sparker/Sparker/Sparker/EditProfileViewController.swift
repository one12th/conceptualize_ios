//
//  EditProfileViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 09/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import MBProgressHUD

class EditProfileViewController: UIViewController, CustomFlowLayoutDelegate, UICollectionViewDataSource, RFQuiltLayoutDelegate, UITextViewDelegate, UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var collectionView: UICollectionView!
    var scrollView = UIScrollView()
    var bioTextView = UITextView()
    var charCountLabel = UILabel()
    var images = [UInt]()
    var bio = ""
    var _presentingProfileViewController:ProfileViewController!
    var reorderingImages = false
    var replacingFirst = false
    var firstLaunchAlert: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = AppConfig.editProfileScreenBackgroundColor
        
        self.navigationItem.title = "Edit Profile"
        
        if(self.appDelegate.firstLaunch){
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.done, target: self, action: #selector(EditProfileViewController.didTapNextButton))
        }else{
            let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(EditProfileViewController.didTapCancelButton))
            cancelButton.tintColor = AppConfig.editProfileScreenNavigationBarCancelButtonColor
            self.navigationItem.leftBarButtonItem = cancelButton
            
            let saveButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(EditProfileViewController.didTapSaveButton))
            saveButton.tintColor = AppConfig.editProfileScreenNavigationBarSaveButtonColor
            self.navigationItem.rightBarButtonItem = saveButton
        }
        
        self.images = Utility.sharedInstance.currentUserProfile!.images
        self.bio = Utility.sharedInstance.currentUserProfile!.bio
        
        self.scrollView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height - 64)
        self.view.addSubview(self.scrollView)
        
        let layout = CustomFlowLayout()
        layout.prelayoutEverything = true
        layout.blockPixels = CGSize(width: floor((self.view.bounds.size.width - 32)/3), height: floor((self.view.bounds.size.width - 32)/3))
        layout.delegate = self
        self.collectionView = UICollectionView(frame: CGRect(x: 16, y: 8, width: self.view.bounds.size.width - 32, height: self.view.bounds.size.width - 16), collectionViewLayout: layout)
        self.collectionView.register(EditImageCollectionViewCell.self, forCellWithReuseIdentifier: "editImageCell")
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.backgroundColor = AppConfig.editProfileScreenBackgroundColor
        self.scrollView.addSubview(self.collectionView)
        
        let bioHeadingLabel = UILabel()
        bioHeadingLabel.frame = CGRect(x: 18, y: self.view.bounds.size.width - 16, width: self.view.bounds.size.width - 32, height: 33)
        bioHeadingLabel.textColor = AppConfig.editProfileScreenAboutHeadingTextColor
        bioHeadingLabel.font = AppConfig.editProfileScreenAboutHeadingFont
        bioHeadingLabel.text = "About "+Utility.sharedInstance.currentUserProfile!.name
        self.scrollView.addSubview(bioHeadingLabel)
        
        let bioHeight = ceil((Utility.sharedInstance.currentUserProfile!.bio as NSString).boundingRect(with: CGSize(width: self.view.bounds.size.width - 32, height: CGFloat.greatestFiniteMagnitude), options: [NSStringDrawingOptions.usesFontLeading, NSStringDrawingOptions.usesLineFragmentOrigin], attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 12)], context: nil).height) + 48
        self.bioTextView.frame = CGRect(x: 0, y: self.view.bounds.size.width + 22, width: self.view.bounds.size.width, height: max(44,bioHeight))
        self.bioTextView.textContainerInset = UIEdgeInsets(top: 20, left: 16, bottom: 28, right: 16)
        self.bioTextView.font = AppConfig.editProfileScreenAboutTextFont
        self.bioTextView.textColor = AppConfig.editProfileScreenAboutTextTextColor
        self.bioTextView.text = self.bio
        self.bioTextView.isEditable = true
        self.bioTextView.isScrollEnabled = false
        self.bioTextView.delegate = self
        self.scrollView.addSubview(self.bioTextView)
        
        self.charCountLabel.frame = CGRect(x: 16, y: self.bioTextView.frame.size.height - 20, width: self.view.bounds.size.width - 32, height: 16)
        self.charCountLabel.textAlignment = NSTextAlignment.right
        self.charCountLabel.font = AppConfig.editProfileScreenAboutCharacterCountFont
        self.charCountLabel.textColor = AppConfig.editProfileScreenAboutCharacterCountTextColor
        self.charCountLabel.text = "\(500-self.bioTextView.text.characters.count)"
        self.bioTextView.addSubview(self.charCountLabel)
        // Do any additional setup after loading the view.
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.scrollView.contentSize = CGSize(width: self.view.bounds.size.width, height: self.bioTextView.frame.origin.y + self.bioTextView.frame.size.height + 16)
        
        self.scrollView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(self.appDelegate.firstLaunch && self.firstLaunchAlert == nil){
            self.firstLaunchAlert = UIAlertController(title: "Your Profile", message: "Create a great profile by adding images and a short bio.", preferredStyle: UIAlertControllerStyle.alert)
            self.firstLaunchAlert!.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(self.firstLaunchAlert!, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let bioHeight = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height
        self.bioTextView.frame = CGRect(x: 0, y: self.view.bounds.size.width + 22, width: self.view.bounds.size.width, height: max(44,bioHeight))
        self.charCountLabel.frame = CGRect(x: 16, y: self.bioTextView.frame.size.height - 20, width: self.view.bounds.size.width - 32, height: 16)
        self.charCountLabel.text = "\(500-textView.text.characters.count)"
        self.bio = self.bioTextView.text
        self.scrollView.contentSize = CGSize(width: self.view.bounds.size.width, height: self.bioTextView.frame.origin.y + self.bioTextView.frame.size.height + 16)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return textView.text.characters.count + (text.characters.count - range.length) <= 500
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "editImageCell", for: indexPath) as! EditImageCollectionViewCell
        if(self.images.count <= indexPath.row){
            cell.configureCell(nil)
        }else{
            cell.configureCell(self.images[indexPath.row])
        }
        cell.imageButton.addTarget(self, action: #selector(EditProfileViewController.didTapImageButton(_:)), for: UIControlEvents.touchUpInside)
        cell.imageButton.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!, blockSizeForItemAt indexPath: IndexPath!) -> CGSize {
        if(indexPath.row == 0){
            return CGSize(width: 2,height: 2)
        }
        return CGSize(width: 1,height: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!, insetsForItemAt indexPath: IndexPath!) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //
        
    }
    
    @objc func didTapImageButton(_ sender:UITableViewCell){
        if(sender.tag >= images.count){
            self.showPhotoSelectionMenu()
        }else{
            self.deleteImage(sender.tag)
        }
    }
    
    @objc func didTapCancelButton(){
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func didTapNextButton(){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Utility.sharedInstance.currentUserProfile!.images = self.images
        Utility.sharedInstance.currentUserProfile!.bio = self.bio
        Utility.sharedInstance.uploadCurrentUser({ () -> Void in
            self.appDelegate.swipeViewController.resaveLocalProfilePicture()
            let settingsViewController = SettingsViewController(style: UITableViewStyle.grouped)
            DispatchQueue.main.async(execute: { () -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.navigationController?.pushViewController(settingsViewController, animated: true)
            })
            }) { () -> Void in
                let alert = UIAlertController(title: "Error", message: "There was an error updating your profile", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func didTapSaveButton(){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Utility.sharedInstance.currentUserProfile!.images = self.images
        Utility.sharedInstance.currentUserProfile!.bio = self.bio
        Utility.sharedInstance.uploadCurrentUser({ () -> Void in
            self._presentingProfileViewController.profile = Utility.sharedInstance.currentUserProfile!
            self._presentingProfileViewController.setupProfile()
            self.appDelegate.swipeViewController.resaveLocalProfilePicture()
            DispatchQueue.main.async(execute: { () -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.presentingViewController?.dismiss(animated: true, completion: nil)
            })
            }) { () -> Void in
                let alert = UIAlertController(title: "Error", message: "There was an error updating your profile", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification){
        let kbHeight = (notification.userInfo![UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size.height
        var frame = self.scrollView.frame
        frame.size.height -= kbHeight
        UIView.animate(withDuration: 0.3, animations: {
            self.scrollView.frame = frame
        })
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification){
        let kbHeight = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size.height
        var frame = self.scrollView.frame
        frame.size.height += kbHeight
        UIView.animate(withDuration: 0.3, animations: {
            self.scrollView.frame = frame
        })
    }
    
    func showPhotoSelectionMenu(){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            actionSheet.addAction(UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                //self.presentCamer
                self.startCamera()
            }))
        }
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)){
            actionSheet.addAction(UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                self.presentPhotoLibraryPickerController()
            }))
        }
        /*actionSheet.addAction(UIAlertAction(title: "Facebook Photos", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let fbImagePicker = OLFacebookImagePickerController()
            fbImagePicker.delegate = self
            self.presentViewController(fbImagePicker, animated: true, completion: nil)
        }))*/
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func startCamera(){
        let cameraUI = UIImagePickerController()
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) == false){
            return
        }
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) && UIImagePickerController.availableMediaTypes(for: UIImagePickerControllerSourceType.camera)?.contains(kUTTypeImage as String) != false){
            
            cameraUI.mediaTypes = [kUTTypeImage as String]
            cameraUI.sourceType = UIImagePickerControllerSourceType.camera
            
            if(UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.rear)){
                cameraUI.cameraDevice = UIImagePickerControllerCameraDevice.rear
            }else if(UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.front)){
                cameraUI.cameraDevice = UIImagePickerControllerCameraDevice.front
            }
        }else{
            return
        }
        
        cameraUI.allowsEditing = true
        cameraUI.delegate = self
        
        self.present(cameraUI, animated: true, completion: nil)
    }
    
    func presentPhotoLibraryPickerController(){
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) == false && UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) == false){
            return
        }
        
        let libraryUI = UIImagePickerController()
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) && UIImagePickerController.availableMediaTypes(for: UIImagePickerControllerSourceType.photoLibrary)?.contains(kUTTypeImage as String) != false){
            
            libraryUI.sourceType = UIImagePickerControllerSourceType.photoLibrary
            libraryUI.mediaTypes = [kUTTypeImage as String]
            
        }else if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) && UIImagePickerController.availableMediaTypes(for: UIImagePickerControllerSourceType.savedPhotosAlbum)?.contains(kUTTypeImage as String) != false){
            
            libraryUI.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
            libraryUI.mediaTypes = [kUTTypeImage as String]
        }else{
            return
        }
        
        libraryUI.allowsEditing = true
        libraryUI.delegate = self
        
        self.present(libraryUI, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //
        var image = info[UIImagePickerControllerEditedImage] as? UIImage
        if(image == nil){
            image = info[UIImagePickerControllerOriginalImage] as? UIImage
            
            var scaleFactor = image!.size.width / self.view.bounds.size.width
            var cropSquare = CGRect(x: 44.0 * scaleFactor, y: 0, width: image!.size.width, height: image!.size.width)
            if(image!.size.width > image!.size.height){
                scaleFactor = image!.size.height / self.view.bounds.size.width
                cropSquare = CGRect(x: 44.0 * scaleFactor, y: 0, width: image!.size.height, height: image!.size.height)
            }
            
            let imageRef = image!.cgImage!.cropping(to: cropSquare)
            image = UIImage(cgImage: imageRef!, scale: image!.scale, orientation: image!.imageOrientation)
        }
        image = image!.resizedImage(CGSize(width: 500, height: 500), interpolationQuality: CGInterpolationQuality.default)
        
        picker.presentingViewController?.dismiss(animated: true, completion: nil)
        if let data = UIImageJPEGRepresentation(image!, 0.8){
            self.uploadImage(data)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*func facebookImagePicker(imagePicker: OLFacebookImagePickerController!, didFinishPickingImages images: [AnyObject]!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func facebookImagePickerDidCancelPickingImages(imagePicker: OLFacebookImagePickerController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func facebookImagePicker(imagePicker: OLFacebookImagePickerController!, didFailWithError error: NSError!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func facebookImagePicker(imagePicker: OLFacebookImagePickerController!, didSelectImage image: OLFacebookImage!) {
        let url = image.bestURLForSize(CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.width))
        var data:NSData?
        do{
            try data = NSData(contentsOfURL: url, options: [])
        }catch{
            return
        }
        self.dismissViewControllerAnimated(true, completion: nil)
        if(data != nil){
            self.uploadImage(data!)
        }
    }*/
    
    func uploadImage(_ imageData:Data){
        let uploadProgressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        uploadProgressHUD?.mode = MBProgressHUDMode.determinateHorizontalBar
        QBRequest.tUploadFile(imageData, fileName: "userImage", contentType: "image/jpeg", isPublic: false, successBlock: { (response, blob) -> Void in
            self.images = Utility.sharedInstance.currentUserProfile!.images
            Utility.sharedInstance.currentUserProfile!.images.append(blob.id)
            Utility.sharedInstance.uploadCurrentUser({ () -> Void in
                self.images = Utility.sharedInstance.currentUserProfile!.images
                self.appDelegate.swipeViewController.resaveLocalProfilePicture()
                DispatchQueue.main.async(execute: { () -> Void in
                    uploadProgressHUD?.hide(true)
                    self.collectionView.reloadData()
                })
                }, errorBlock: { () -> Void in
                    Utility.sharedInstance.currentUserProfile!.images = self.images
                    DispatchQueue.main.async(execute: { () -> Void in
                        uploadProgressHUD?.hide(true)
                        self.collectionView.reloadData()
                    })
            })
            }, statusBlock: { (request, status) -> Void in
                if(status != nil){
                    uploadProgressHUD?.progress = status!.percentOfCompletion
                }
            }) { (response) -> Void in
                print("Error uploading file!")
                DispatchQueue.main.async(execute: { () -> Void in
                    uploadProgressHUD?.hide(true)
                })
        }

    }
    
    func confirmDeleteImage(_ index:Int){
        let alert = UIAlertController(title: "Confirm Delete", message: "Are you sure you want to delete this picture from your profile?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.destructive, handler: { (action) in
            self.deleteImage(index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteImage(_ index:Int){
        if(self.images.count < index){
            return
        }
        let imageID = self.images[index]
        self.images.remove(at: index)
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Utility.sharedInstance.currentUserProfile!.images = self.images
        Utility.sharedInstance.uploadCurrentUser({ () -> Void in
            self.appDelegate.swipeViewController.resaveLocalProfilePicture()
            QBRequest.deleteBlob(withID: imageID, successBlock: { (response) -> Void in
                DispatchQueue.main.async(execute: { () -> Void in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.collectionView.reloadData()
                })
                }) { (response) -> Void in
                    print("Error deleting image")
                    self.images.remove(at: index)
                    self.collectionView.reloadData()
                    DispatchQueue.main.async(execute: { () -> Void in
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
            }
            }) { () -> Void in
                self.images.insert(imageID, at: index)
                Utility.sharedInstance.currentUserProfile!.images = self.images
                DispatchQueue.main.async(execute: { () -> Void in
                    MBProgressHUD.hide(for: self.view, animated: true)
                })
        }
        
        
    }
    
    func moveDataItem(_ fromIndexPath : IndexPath, toIndexPath: IndexPath) {
        if(self.images.count <= toIndexPath.item){
            return
        }
        let name = self.images[fromIndexPath.item]
        self.images.remove(at: fromIndexPath.item)
        self.images.insert(name, at: toIndexPath.item)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
