//
//  PresentWebViewViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 23/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class WebViewViewController: UIViewController {

    init(html:String){
        super.init(nibName: nil, bundle: nil)
        
        let webView = UIWebView(frame: self.view.bounds)
        webView.loadHTMLString(html, baseURL: nil)
        self.view.addSubview(webView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(WebViewViewController.didTapDone))
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didTapDone(){
        self.navigationController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
