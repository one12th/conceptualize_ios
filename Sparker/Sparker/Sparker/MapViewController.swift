//
//  MapViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 15/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, UISearchControllerDelegate, UISearchBarDelegate, MKMapViewDelegate{
    
    var annotation = MKPointAnnotation()
    let locationMarker = UIImageView()
    let mapView = MKMapView()
    var delegate:LocationMenuViewController
    
    init(delegate:LocationMenuViewController){
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mapView.frame = self.view.bounds
        self.mapView.delegate = self
        self.view.addSubview(self.mapView)

        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: UIBarButtonItemStyle.done, target: self, action: #selector(MapViewController.didTapCloseButton))
        
        self.locationMarker.frame = CGRect(x: (self.view.bounds.size.width - 44)/2, y: (self.view.bounds.size.height-44)/2, width: 44, height: 44)
        var image = UIImage(named: AppConfig.mapScreenPinImage)
        if(AppConfig.mapScreenPinImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        self.locationMarker.image = image
        self.locationMarker.tintColor = AppConfig.mapScreenPinImageColor
        self.view.addSubview(self.locationMarker)
        
        self.definesPresentationContext = true
        // Do any additional setup after loading the view.
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.didStopDragging()
    }
    
    func didStopDragging(){
        self.annotation.coordinate = self.mapView.convert(CGPoint(x: self.view.bounds.size.width/2, y: self.view.bounds.size.height/2 + 32), toCoordinateFrom: self.view)
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude)) { (placemarks, error) -> Void in
            if(error != nil){
                print("Error getting placemarks: \(String(describing: error?.localizedDescription))")
                self.annotation.title = "Unknown Location"
            }
            if(placemarks != nil && placemarks!.count > 0){
                let placemark = placemarks![0]
                
                //print("\(placemark.locality), \(placemark.subAdministrativeArea), \(placemark.administrativeArea)")
                
                /*if(placemark.locality != nil){
                    self.annotation.title = placemark.locality
                }else*/
                if(placemark.subAdministrativeArea != nil){
                    self.annotation.title = placemark.subAdministrativeArea
                }else if(placemark.administrativeArea != nil){
                    self.annotation.title = placemark.administrativeArea
                }else{
                    self.annotation.title = "Unknown Location"
                }
            }
            self.mapView.addAnnotation(self.annotation)
            self.mapView.selectAnnotation(self.annotation, animated: false)
        }
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotationCallout")
        annotationView.calloutOffset = CGPoint(x: 0, y: -24)
        annotationView.canShowCallout = true
        let annotationButton = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        var image = UIImage(named: AppConfig.mapScreenCalloutChooseButtonImage)
        if(AppConfig.mapScreenCalloutChooseButtonImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        annotationButton.setImage(image, for: UIControlState())
        annotationButton.tintColor = AppConfig.mapScreenCalloutChooseButtonImageColor
        annotationButton.addTarget(self, action: #selector(MapViewController.didTapAnnotationButton), for: UIControlEvents.touchUpInside)
        annotationView.rightCalloutAccessoryView = annotationButton
        image = UIImage(named: AppConfig.mapScreenCalloutImage)
        if(AppConfig.mapScreenCalloutImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        let iconImage = UIImageView(image: image)
        iconImage.tintColor = AppConfig.mapScreenCalloutImageColor
        annotationView.leftCalloutAccessoryView = iconImage
        return annotationView
    }
    
    @objc func didTapAnnotationButton(){
        self.delegate.appDelegate.userLocations.append(UserLocation(name: self.annotation.title!, coordinate: self.annotation.coordinate))
        self.delegate.addedNewLocation()
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didTapCloseButton(){
        self.resignFirstResponder()
        if(self.presentedViewController != nil){
            self.dismiss(animated: true, completion: nil)
        }
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
