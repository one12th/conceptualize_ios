//
//  SwipeViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 04/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import MBProgressHUD
import JDStatusBarNotification
import SWRevealViewController
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class SwipeViewController: UIViewController, LoginViewControllerDelegate, DraggableViewDelegate, QBChatDelegate{

    var facebookResponseCount = 0
    var expectedFacebookResponseCount = 0
    var presentedLoginViewController = false
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var cardProfiles = [UserProfile]()
    var loadedCards = [DraggableView]() {
        didSet{
            let defaultFrameHeight = self.view.bounds.size.width - 32 + 66
            for (i,card) in self.loadedCards.enumerated(){
                if(i != 0){
                    card.removeGestureRecognizers()
                }else{
                    card.addGestureRecognizers()
                }
                let sf = CGFloat(Double(i)/20.0)
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    card.transform = CGAffineTransform(scaleX: 1-sf, y: 1-sf).translatedBy(x: 0, y: sf*defaultFrameHeight/2 + CGFloat(i*5))
                })
            }
        }
    }
    var hasCards = false
    var loadedIDs = [UInt]()
    var pendingUsers = [UInt]()
    var findMoreUsersTimer = Timer()
    var loadingProfiles = false
    var profileImageView = UIImageView()
    var animatedCircleView = UIView()
    var lastRejectedCard: DraggableView?
    let likeButton = UIButton()
    let dislikeButton = UIButton()
    let rewindButton = UIButton()
    let locationButton = UIButton()
    let locationMenu = UIView()
    var pushTimers = [UInt:Timer]()
    var searchingAnimationTimer:Timer?
    let searchingLabel = UILabel()
    var unreadIndicator = UIView()
    
    override func viewDidLoad() {
        
        QBChat.instance().addDelegate(self)
        
        var image = UIImage(named: AppConfig.mainScreenNavigationBarTitleImage)
        if(AppConfig.mainScreenNavigationBarTitleImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        self.navigationItem.titleView = UIImageView(image: image)
        self.navigationItem.titleView?.tintColor = AppConfig.mainScreenNavigationBarTitleImageColor
        
        self.unreadIndicator.frame = CGRect(x: 26, y: 10, width: 14, height: 14)
        let unreadIndicatorColorView = UIView(frame: CGRect(x: 1, y: 1, width: 12, height: 12))
        unreadIndicatorColorView.backgroundColor = AppConfig.mainScreenNavigationBarChatsUnreadIndicatorColor
        unreadIndicatorColorView.layer.cornerRadius = 6.0
        self.unreadIndicator.layer.cornerRadius = 7.0
        self.unreadIndicator.layer.borderWidth = 2.5
        self.unreadIndicator.layer.borderColor = UIColor.white.cgColor
        self.unreadIndicator.clipsToBounds = true
        self.unreadIndicator.addSubview(unreadIndicatorColorView)
        self.unreadIndicator.isHidden = true
        
        let chatsButton = UIButton(frame: CGRect(x: 5, y: 0, width: 44.0, height: 44.0))
        image = UIImage(named: AppConfig.mainScreenNavigationBarChatsButtonImage)
        //if(AppConfig.mainScreenNavigationBarChatsButtonImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        //}
        chatsButton.setImage(image, for: UIControlState())
        chatsButton.addTarget(self, action: #selector(SwipeViewController.toChatsScreen), for: UIControlEvents.touchUpInside)
        chatsButton.addSubview(self.unreadIndicator)
        chatsButton.tintColor = AppConfig.mainScreenNavigationBarButtonsColor
        let rightSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        rightSpace.width = -5
        self.navigationItem.rightBarButtonItems = [rightSpace, UIBarButtonItem(customView: chatsButton)];
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: AppConfig.mainScreenNavigationBarMenuButtonImage), style: UIBarButtonItemStyle.plain, target: self.appDelegate.masterViewContainer, action: #selector(SWRevealViewController.revealToggle(_:)))
        self.view.backgroundColor = AppConfig.mainScreenBackgroundColor
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "com.samuelharrison.sparker.utility.userLocationUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SwipeViewController.loadPendingContactProfiles), name: NSNotification.Name(rawValue: "com.samuelharrison.sparker.utility.userLocationUpdated"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "com.samuelharrison.sparker.utility.userSettingsChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SwipeViewController.resetCards), name: NSNotification.Name(rawValue: "com.samuelharrison.sparker.utility.userSettingsChanged"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "com.samuelharrison.utility.dialogsUpdatedNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SwipeViewController.checkForUnreadIndicator), name: NSNotification.Name(rawValue: "com.samuelharrison.utility.dialogsUpdatedNotification"), object: nil)
        
        self.animatedCircleView.frame = CGRect(x: (self.view.bounds.size.width - 50)/2, y: (self.view.bounds.size.height - 50)/2 - 64.0, width: 50, height: 50)
        self.animatedCircleView.layer.cornerRadius = 25.0
        self.animatedCircleView.backgroundColor = AppConfig.radarColor
        self.view.insertSubview(self.animatedCircleView, at: 0)
        
        let profileImageViewContainer = UIView(frame: CGRect(x: (self.view.bounds.size.width - 152)/2, y: (self.view.bounds.size.height - 152)/2 - 64.0, width: 152, height: 152))
        profileImageViewContainer.layer.cornerRadius = 75.0
        profileImageViewContainer.layer.borderWidth = AppConfig.currentUserProfileImageBorderWidth
        profileImageViewContainer.layer.borderColor = AppConfig.currentUserProfileImageBorderColor.cgColor
        profileImageViewContainer.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        profileImageViewContainer.clipsToBounds = true
        
        self.profileImageView.frame = CGRect(x: 1, y: 1, width: 150, height: 150)
        self.profileImageView.layer.cornerRadius = 75.0
        self.profileImageView.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        self.profileImageView.clipsToBounds = true
        self.profileImageView.contentMode = UIViewContentMode.scaleAspectFill
        let destinationPath = (NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profilePicture")
        if let image = UIImage(contentsOfFile: destinationPath){
            self.profileImageView.image = image
        }else{
            self.profileImageView.image = UIImage(named: "PlaceholderImage")
        }
        
        self.view.addSubview(profileImageViewContainer)
        profileImageViewContainer.addSubview(self.profileImageView)
        
        self.setupLookingForUsersView()
        self.searchingAnimationTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(SwipeViewController.setupLookingForUsersView), userInfo: nil, repeats: true)
        
        self.setupButtons()
        
        self.loginExistingUser()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkForUnreadIndicator()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(!self.hasCards && QBChat.instance().isConnected && Utility.sharedInstance.foundLocation){
            self.loadPendingContactProfiles()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func loginExistingUser(){
        if(QBSession.current().currentUser == nil || FBSDKAccessToken.current() == nil){
            self.presentLoginViewController(false)
            return
        }
        
        let loginProgressHUD = MBProgressHUD.showAdded(to: self.navigationController!.view, animated: true)
        loginProgressHUD?.labelText = "Connecting"
        QBRequest.logIn(withSocialProvider: "facebook", accessToken: FBSDKAccessToken.current().tokenString, accessTokenSecret: nil, successBlock: { (response, user) -> Void in
            if(user == nil){
                loginProgressHUD?.hide(true)
                print("No user returned")
                self.appDelegate.logOut()
                return
            }
            if let storedProfileData = UserDefaults.standard.object(forKey: "savedUserProfile") as? Data{
                if let storedProfile = NSKeyedUnarchiver.unarchiveObject(with: storedProfileData) as? UserProfile{
                    if storedProfile.userID == user!.id{
                        Utility.sharedInstance.currentUserProfile = storedProfile
                    }
                }
            }
            user?.password = QBSession.current().sessionDetails!.token
            QBChat.instance().connect(with: user!, completion: { (error) -> Void in
                if(error == nil){
                    self.registerForRemoteNotifications()
                    let params = NSMutableDictionary()
                    params.setObject("\(user!.id)", forKey: "user_id" as NSCopying)
                    QBRequest.objects(withClassName: "profile", extendedRequest: params, successBlock: { (response, object, page) -> Void in
                        if(object != nil && object!.count > 0){
                            loginProgressHUD?.hide(true)
                            Utility.sharedInstance.currentUserProfile = UserProfile(object: object![0])
                            self.refreshCurrentUser()
                        }else{
                            let object = QBCOCustomObject()
                            object.className = "profile"
                            QBRequest.createObject(object, successBlock: { (response, object) -> Void in
                                loginProgressHUD?.hide(true)
                                if(object == nil){
                                    self.appDelegate.logOut()
                                    return
                                }
                                Utility.sharedInstance.currentUserProfile = UserProfile(object: object!)
                                user!.customData = object!.id
                                self.refreshCurrentUser()
                                
                            }, errorBlock: { (response) -> Void in
                                loginProgressHUD?.hide(true)
                                print("Error creating profile, logging out")
                                self.appDelegate.logOut()
                                return
                            })
                        }
                        }, errorBlock: { (response) -> Void in
                            print("Error loading profile: \(String(describing: response.error?.description))")
                    })
                }else{
                    loginProgressHUD?.hide(true)
                    self.appDelegate.logOut()
                    print("Error connecting to chat: \(String(describing: error))")
                }
            })
        }) { (response) -> Void in
            loginProgressHUD?.hide(true)
            print("Error logging in: \(String(describing: response.error))")
            //self.presentLoginViewController(true)
            self.appDelegate.logOut()
            
        }
    }
    
    @objc func checkForUnreadIndicator(){
        self.unreadIndicator.isHidden = !ChatService.sharedInstance.shouldShowUnreadIndicator()
    }
    
    func setupButtons(){
        
        let height = self.view.bounds.size.height - 64
        let cardHeight = min(self.view.bounds.size.width + 66 - 32, height * 0.8 - 32)
        let buttonSize = min((height - (cardHeight + 11))*0.70, 110)
        
        self.likeButton.frame = CGRect(x: self.view.bounds.size.width/2 - buttonSize/20, y: height/2 + (cardHeight+11)/2 - buttonSize/2, width: buttonSize, height: buttonSize)
        self.likeButton.layer.cornerRadius = buttonSize/2
        self.likeButton.layer.borderWidth = buttonSize/10
        self.likeButton.layer.borderColor = AppConfig.likeButtonBorderColor.cgColor
        var image = UIImage(named: AppConfig.likeButtonImage)
        if(AppConfig.likeButtonImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        self.likeButton.setImage(image, for: UIControlState())
        self.likeButton.imageEdgeInsets = UIEdgeInsets(top: buttonSize/3.5, left: buttonSize/3.5, bottom: buttonSize/3.5, right: buttonSize/3.5)
        self.likeButton.tintColor = AppConfig.likeButtonImageColor
        self.likeButton.addTarget(self, action: #selector(SwipeViewController.didTapLikeButton), for: UIControlEvents.touchUpInside)
        self.view.addSubview(self.likeButton)

        self.dislikeButton.frame = CGRect(x: self.view.bounds.size.width/2 - buttonSize + buttonSize/20, y: height/2 + (cardHeight+11)/2 - buttonSize/2, width: buttonSize, height: buttonSize)
        self.dislikeButton.layer.cornerRadius = buttonSize/2
        self.dislikeButton.layer.borderWidth = buttonSize/10
        self.dislikeButton.layer.borderColor = AppConfig.dislikeButtonBorderColor.cgColor
        image = UIImage(named: AppConfig.dislikeButtonImage)
        if(AppConfig.dislikeButtonImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        self.dislikeButton.setImage(image, for: UIControlState())
        self.dislikeButton.imageEdgeInsets = UIEdgeInsets(top: buttonSize/3.5, left: buttonSize/3.5, bottom: buttonSize/3.5, right: buttonSize/3.5)
        self.dislikeButton.tintColor = AppConfig.dislikeButtonImageColor
        self.dislikeButton.addTarget(self, action: #selector(SwipeViewController.didTapDislikeButton), for: UIControlEvents.touchUpInside)
        self.view.addSubview(self.dislikeButton)
        
        self.rewindButton.frame = CGRect(x: self.view.bounds.size.width/2 - buttonSize*1.4 + buttonSize/20 , y: height/2 + (cardHeight+11)/2 - buttonSize/2, width: buttonSize/2, height: buttonSize/2)
        self.rewindButton.layer.cornerRadius = buttonSize/4
        self.rewindButton.layer.borderWidth = buttonSize/12
        self.rewindButton.layer.borderColor = AppConfig.rewindButtonBorderColor.cgColor
        image = UIImage(named: AppConfig.rewindButtonImage)
        if(AppConfig.rewindButtonImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        self.rewindButton.setImage(image, for: UIControlState())
        self.rewindButton.imageEdgeInsets = UIEdgeInsets(top: buttonSize/7, left: buttonSize/7, bottom: buttonSize/7, right: buttonSize/7)
        self.rewindButton.tintColor = AppConfig.rewindButtonImageColor
        self.rewindButton.addTarget(self, action: #selector(SwipeViewController.didTapRewind), for: UIControlEvents.touchUpInside)
        if(AppConfig.shouldEnableRewind){
            self.view.addSubview(self.rewindButton)
        }
        let x1 = self.view.bounds.size.width/2;
        let x2 = buttonSize*1.4;
        let x3 = buttonSize/20;
        let x4 = buttonSize/2;
        let x = x1 + x2 - x3 - x4;
        let y = height/2 + (cardHeight+11)/2 - buttonSize/2;
        self.locationButton.frame = CGRect(x: x, y: y, width: buttonSize/2, height: buttonSize/2)
        self.locationButton.layer.cornerRadius = buttonSize/4
        self.locationButton.layer.borderWidth = buttonSize/12
        self.locationButton.layer.borderColor = AppConfig.locationButtonBorderColor.cgColor
        image = UIImage(named: AppConfig.locationButtonCurrentLocationImage)
        if(AppConfig.likeButtonImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        self.locationButton.setImage(image, for: UIControlState())
        image = UIImage(named: AppConfig.locationButtonAwayLocationImage)
        if(AppConfig.locationButtonImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        self.locationButton.setImage(image, for: UIControlState.selected)
        if(self.appDelegate.userSettings["activeLocation"] == 0){
            self.locationButton.isSelected = false
        }else{
            self.locationButton.isSelected = true
        }
        self.locationButton.imageEdgeInsets = UIEdgeInsets(top: buttonSize/7, left: buttonSize/7, bottom: buttonSize/7, right: buttonSize/7)
        self.locationButton.tintColor = AppConfig.locationButtonImageColor
        self.locationButton.addTarget(self, action: #selector(SwipeViewController.didTapLocationButton), for: UIControlEvents.touchUpInside)
        if(AppConfig.shouldEnablePassport){
            self.view.addSubview(self.locationButton)
        }
    }
    
    @objc func didTapLocationButton(){
        if(self.appDelegate.subscriptionExpirationTimestamp < Date().timeIntervalSince1970 && AppConfig.passportIsPlusOnly){
            self.showUpgradeScreen(true)
            return
        }
        let locationMenuViewController = LocationMenuViewController()
        locationMenuViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        locationMenuViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(locationMenuViewController, animated: true, completion: nil)
    }
    
    @objc func didTapLikeButton(){
        if(self.loadedCards.count > 0){
            self.loadedCards[0].rightClickAction()
        }
    }
    
    @objc func didTapDislikeButton(){
        if(self.loadedCards.count > 0){
            self.loadedCards[0].leftClickAction()
        }
    }
    
    @objc func setupLookingForUsersView(){
        if(hasCards){
            self.searchingAnimationTimer?.invalidate()
            self.searchingAnimationTimer = nil
            return
        }
        self.animatedCircleView.transform = CGAffineTransform.identity
        self.animatedCircleView.alpha = 1.0
        UIView.animate(withDuration: 3, animations: { () -> Void in
            self.animatedCircleView.alpha = 0
            self.animatedCircleView.transform = CGAffineTransform(scaleX: self.view.bounds.size.width/50, y: self.view.bounds.size.width/50)
            }, completion: { (completed) -> Void in
                //self.setupLookingForUsersView()
        }) 
    }
    
    func presentLoginViewController(_ animated:Bool){
        self.cardProfiles.removeAll()
        self.loadedIDs.removeAll()
        for card in self.loadedCards{
            card.removeFromSuperview()
        }
        self.loadedCards.removeAll()
        self.pendingUsers.removeAll()
        self.hasCards = false
        self.findMoreUsersTimer.invalidate()
        self.presentedLoginViewController = true
        let loginViewController = LoginViewController(delegate: self)
        self.present(loginViewController, animated: animated, completion: nil)
    }
    
    func loginViewControllerDidLogUserIn(_ loginViewController: LoginViewController) {
        if(self.presentedLoginViewController){
            self.presentedLoginViewController = false
            self.dismiss(animated: true, completion: nil)
        }
        self.refreshCurrentUser()
        Utility.sharedInstance.startUpdatingLocation()
    }
    
    func processedFacebookResponse(){
        
        objc_sync_enter(self)
        self.facebookResponseCount += 1
        if(self.facebookResponseCount != self.expectedFacebookResponseCount){
            return
        }
        objc_sync_exit(self)
        
        QBRequest.update(Utility.sharedInstance.currentUserProfile!.object, successBlock: { (response, object) -> Void in
            
            if(self.appDelegate.firstLaunch){
                let editProfileViewController = EditProfileViewController()
                let navController = UINavigationController(rootViewController: editProfileViewController)
                DispatchQueue.main.async(execute: { () -> Void in
                    self.present(navController, animated: true, completion: nil)
                })
            }
            
            Utility.sharedInstance.startUpdatingLocation()
            print("Updating location")
            
            }, errorBlock: { (response) -> Void in
                print("Error updating user profile: \(String(describing: response.error))")
        })
    }
    
    func resaveLocalProfilePicture(){
        if Utility.sharedInstance.currentUserProfile!.images.count > 0{
            if let urlString = QBCBlob.privateUrl(forID: Utility.sharedInstance.currentUserProfile!.images[0]){
                if let url = URL(string: urlString){
                    self.profileImageView.setImageWith(URLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 30), placeholderImage: self.profileImageView.image, success: { (request, response, image) -> Void in
                        self.profileImageView.image = image
                        self.appDelegate.menuViewController.profileButton.customImageView.image = image
                        let destinationPath = (NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profilePicture")
                        try? UIImageJPEGRepresentation(image, 1.0)?.write(to: URL(fileURLWithPath: destinationPath), options: [.atomic])
                        }, failure: { (request, response, error) -> Void in
                            print("Error getting profile picture: \(error)")
                    })
                }
            }
        }else{
            let destinationPath = (NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profilePicture")
            do{
                try FileManager.default.removeItem(atPath: destinationPath)
            }catch{
                
            }
            self.profileImageView.image = UIImage(named: "PlaceholderImage")
            self.appDelegate.menuViewController.profileButton.customImageView.image = UIImage(named: "PlaceholderImage")
        }
        
    }
    
    func refreshCurrentUser(){
        
        ChatService.sharedInstance.getDialogs()
        
        self.resaveLocalProfilePicture()
   
        if(FBSDKAccessToken.current() == nil){
            print("Access token is nil, logging out")
            self.appDelegate.logOut()
            return
        }
        
        let currentUser = QBSession.current().currentUser
        
        if(currentUser == nil){
            print("No current user, logging out")
            self.appDelegate.logOut()
            return
        }
        
        FBSDKAccessToken.refreshCurrentAccessToken { (connection, result, error) -> Void in
            if(error != nil){
                print("Failed to refresh FB session, logging out")
                self.appDelegate.logOut()
                return
            }
            print("Refreshed Permissions")
            
            let currentAccessToken = FBSDKAccessToken.current()
            
            print(currentAccessToken?.tokenString as Any)
            
            let fbRequest = FBSDKGraphRequestConnection()
            
            if(currentAccessToken?.hasGranted("public_profile"))!{
                self.expectedFacebookResponseCount += 1
                fbRequest.add(FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"first_name,birthday"]), completionHandler: { (connection, result, error) -> Void in
                    if(error != nil){
                        print("Error getting birthday")
                        (UIApplication.shared.delegate as! AppDelegate).logOut()
                        return
                    }
                    if let result = result as? NSDictionary{
                        if let birthday = result["birthday"] as? String{
                            let dateFormatter = DateFormatter()
                            dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
                            dateFormatter.dateFormat = "MM/dd/yyyy"
                            let ageComponents = (Calendar(identifier: Calendar.Identifier.gregorian) as NSCalendar).components(NSCalendar.Unit.year, from: dateFormatter.date(from: birthday)!, to: Date(), options: [])
                            Utility.sharedInstance.currentUserProfile!.age = ageComponents.year
                        }else{
                            Utility.sharedInstance.currentUserProfile!.age = 18
                        }
                        if let first_name = result["first_name"] as? String{
                            Utility.sharedInstance.currentUserProfile!.name = first_name
                        }
                    }
                    self.processedFacebookResponse()
                })
            }
            
            fbRequest.start()
        }
    }
    
    /*func resetContacts(){
        self.pendingUsers.removeAll()
        if let contacts = QBChat.instance().contactList?.contacts{
            for contact in contacts{
                QBChat.instance().removeUserFromContactList(contact.userID, completion: { (error) -> Void in
                    if(error != nil){
                        print("Error removing from contact list")
                    }
                    print(QBChat.instance().contactList)
                })
            }
        }
        if let contacts = QBChat.instance().contactList?.pendingApproval{
            for contact in contacts{
                QBChat.instance().removeUserFromContactList(contact.userID, completion: { (error) -> Void in
                    if(error != nil){
                        print("Error removing from contact list")
                    }
                    print(QBChat.instance().contactList)
                })
            }
        }
    }*/
    
    @objc func resetCards(){
        self.cardProfiles.removeAll()
        self.pendingUsers.removeAll()
        self.loadedIDs.removeAll()
        for card in self.loadedCards{
            card.removeFromSuperview()
        }
        self.loadedCards.removeAll()
        self.hasCards = false
        if(self.searchingAnimationTimer == nil){
            self.setupLookingForUsersView()
            self.searchingAnimationTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(SwipeViewController.setupLookingForUsersView), userInfo: nil, repeats: true)
        }
        if(self.appDelegate.userSettings["activeLocation"] == 0){
            self.locationButton.isSelected = false
        }else{
            self.locationButton.isSelected = true
        }
        
        self.loadPendingContactProfiles()
    }
    
    @objc func loadPendingContactProfiles(){
        self.findMoreUsersTimer.invalidate()
        if(QBSession.current().currentUser == nil){
            return
        }
        if(!self.isViewLoaded || self.view.window == nil){
            self.setupFindingUsersTimer()
            return
        }
        self.loadingProfiles = true
        self.cardProfiles.removeAll()
        if(self.pendingUsers.count > 0){
            let queryString = self.pendingUsers.map(String.init).joined(separator: ",")
            let swipedString = Utility.sharedInstance.currentUserProfile!.swiped.map(String.init).joined(separator: ",")
            let params = NSMutableDictionary()
            params.setObject(queryString, forKey: "user_id[in]" as NSCopying)
            params.setObject(QBSession.current().currentUser!.id, forKey: "user_id[ne]" as NSCopying)
            params.setObject(swipedString, forKey: "user_id[nin]" as NSCopying)
            params.setObject("\(Utility.sharedInstance.currentUserProfile!.gender),2", forKey: "interested_in[in]" as NSCopying)
            if(Utility.sharedInstance.currentUserProfile!.interestedIn != 2){
                params.setObject(Utility.sharedInstance.currentUserProfile!.interestedIn, forKey: "gender" as NSCopying)
            }
            QBRequest.objects(withClassName: "profile", extendedRequest: params, successBlock: { (response, objects, page) -> Void in
                if(objects != nil && objects!.count > 0){
                    self.findMoreUsersTimer.invalidate()
                    for object in objects!{
                        if(self.loadedIDs.index(of: object.userID) == nil){
                            self.loadedIDs.append(object.userID)
                            self.cardProfiles.append(UserProfile(object: object))
                        }
                    }
                    if(objects!.count < 100){
                        self.loadNearbyUsers()
                    }
                    if(!self.hasCards){
                        self.loadCards()
                        self.loadingProfiles = false
                    }
                }else{
                    self.loadNearbyUsers()
                }
                }, errorBlock: { (response) -> Void in
                    print("Error getting pending request profiles: \(String(describing: response.error))")
            })
        }else{
            self.loadNearbyUsers()
        }
    }
    
    func loadNearbyUsers(){
        print("Loading nearby users")
        let swipedString = Utility.sharedInstance.currentUserProfile!.swiped.map(String.init).joined(separator: ",")
        let params = NSMutableDictionary()
        params.setObject(QBSession.current().currentUser!.id, forKey: "user_id[ne]" as NSCopying)
        
        var searchLatitude:Double = 0
        var searchLongitude:Double = 0
        let locationIndex = self.appDelegate.userSettings["activeLocation"]
        if(locationIndex == nil || locationIndex == 0 || self.appDelegate.userLocations.count < locationIndex){
            searchLatitude = Utility.sharedInstance.currentUserProfile!.location.coordinate.latitude
            searchLongitude = Utility.sharedInstance.currentUserProfile!.location.coordinate.longitude
        }else{
            searchLatitude = self.appDelegate.userLocations[locationIndex! - 1].latitude
            searchLongitude = self.appDelegate.userLocations[locationIndex! - 1].longitude
        }
        params.setObject("\(searchLongitude),\(searchLatitude);\(appDelegate.userSettings["distance"]!*1000)", forKey: "location[near]" as NSCopying)
        params.setObject(swipedString, forKey: "user_id[nin]" as NSCopying)
        params.setObject("\(Utility.sharedInstance.currentUserProfile!.gender),2", forKey: "interested_in[in]" as NSCopying)
        if(Utility.sharedInstance.currentUserProfile!.interestedIn != 2){
            params.setObject(Utility.sharedInstance.currentUserProfile!.interestedIn, forKey: "gender" as NSCopying)
        }
        params.setObject(self.appDelegate.userSettings["agemin"]!, forKey: "age[gte]" as NSCopying)
        if(self.appDelegate.userSettings["agemax"] < 60){
            params.setObject(self.appDelegate.userSettings["agemax"]!, forKey: "age[lte]" as NSCopying)
        }
        QBRequest.objects(withClassName: "profile", extendedRequest: params, successBlock: { (response,objects, page) -> Void in
            if(objects != nil && objects!.count > 0){
                self.findMoreUsersTimer.invalidate()
                for object in objects!{
                    if(self.pendingUsers.index(of: object.userID) == nil && self.loadedIDs.index(of: object.userID) == nil){
                        self.loadedIDs.append(object.userID)
                        self.cardProfiles.append(UserProfile(object: object))
                        if(self.hasCards && self.loadedCards.count < 3){
                            self.loadAnotherCard(nil, addLike:false)
                        }
                    }
                }
                if(!self.hasCards){
                    self.loadCards()
                }else if(self.loadedCards.count < 3){
                    
                }
            }else if(!self.hasCards){
                self.setupFindingUsersTimer()
            }
            self.loadingProfiles = false
            }) { (response) -> Void in
                print("Error getting nearby profiles: \(String(describing: response.error))")
        }
    }
    
    func createDraggableViewWithData(_ profile:UserProfile, rewound:Bool)->DraggableView{
        let cardHeight = min(self.view.bounds.size.width + 66 - 32, self.view.bounds.size.height * 0.8 - 32)
        let draggableView = DraggableView(frame: CGRect(x: 16, y: 11, width: self.view.bounds.size.width - 32, height: cardHeight), profile: profile, rewound:rewound)
        draggableView.delegate = self
        return draggableView
    }
    
    func loadCards(){
        if(self.cardProfiles.count > 0){
            self.hasCards = true
            let numLoadedCardsCap = min(3,self.cardProfiles.count)
            for i in 0...numLoadedCardsCap - 1{
                let newCard = self.createDraggableViewWithData(self.cardProfiles[0], rewound: false)
                self.cardProfiles.remove(at: 0)
                if(i<numLoadedCardsCap){
                    self.loadedCards.append(newCard)
                }
            }
            
            for i in 0...self.loadedCards.count-1{
                if(i > 0){
                    self.view.insertSubview(self.loadedCards[i], belowSubview: self.loadedCards[i-1])
                }else{
                    self.view.addSubview(self.loadedCards[i])
                }
            }
        }else{
            //show finding users image
            self.setupFindingUsersTimer()
        }
    }
    
    func didTapCard(_ card: DraggableView) {
        let profileViewController = ProfileViewController(profile: card.profile, alreadyMatched: false)
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    @objc func didTapRewind(){
        if(self.appDelegate.subscriptionExpirationTimestamp < Date().timeIntervalSince1970 && AppConfig.rewindIsPlusOnly){
            self.showUpgradeScreen(true)
            return
        }
        if(self.lastRejectedCard != nil){
            self.loadedCards.insert(self.lastRejectedCard!, at: 0)
            self.view.addSubview(self.loadedCards[0])
            self.loadedCards[0].rewindAction()
            self.lastRejectedCard = nil
        }
    }
    
    func cardSwipedLeft(_ card:DraggableView){
        self.lastRejectedCard = card
        self.loadedCards.remove(at: 0)
        self.loadAnotherCard(card.profile, addLike:false)
        QBChat.instance().rejectAddContactRequest(card.profile.userID, completion: { (error) -> Void in
            if(error != nil){
                print("Error rejecting contact request \(String(describing: error))")
            }
        })
    }
    
    func cardSwipedRight(_ card: DraggableView) {
        
        self.loadedCards.remove(at: 0)
        if(self.pendingUsers.index(of: card.profile.userID) == nil){
            print("Sending request")
            QBChat.instance().addUser(toContactListRequest: card.profile.userID, completion: { (error) -> Void in
                if(error != nil){
                    print("Error sending request \(String(describing: error))")
                }
            })
        }else{
            print("Confirming request...should display match screen")
            QBChat.instance().confirmAddContactRequest(card.profile.userID, completion: { (error) -> Void in
                if(error != nil){
                    print("Error sending request \(String(describing: error))")
                    return
                }
                Utility.sharedInstance.currentUserProfile?.matched.append(card.profile.userID)
                self.presentMatchScreen(card.profile.userID)
                let timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(SwipeViewController.sendMatchPushNotification(_:)), userInfo: card.profile, repeats: false)
                self.pushTimers[card.profile.userID] = timer
                let dialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
                dialog.occupantIDs = [NSNumber(value: card.profile.userID), NSNumber(value: QBSession.current().currentUser!.id)]
                QBRequest.createDialog(dialog, successBlock: { (response, dialog) -> Void in
                    if(dialog != nil){
                        ChatService.sharedInstance.updateOrInsertDialog(dialog!)
                    }
                    }, errorBlock: { (response) -> Void in
                        print("Error: \(String(describing: response.error))")
                })
            })
        }
        self.loadAnotherCard(card.profile, addLike:true)
    }
    
    @objc func sendMatchPushNotification(_ sender:Timer){
        if let user = sender.userInfo as? UserProfile{
            print("Sent push match message to \(user.userID)")
            QBRequest.sendPush(withText: "You have a new match!", toUsers: "\(user.userID)", successBlock: { (response, event) -> Void in
                //
                }, errorBlock: { (error) -> Void in
                    print(error as Any)
            })
            self.pendingUsers.append(user.userID)
        }
    }
    
    func loadAnotherCard(_ previousProfile:UserProfile?, addLike:Bool){
        
        
        // RE-ENABLE IN PRODUCTION!
        
        if(previousProfile != nil){
            Utility.sharedInstance.currentUserProfile!.swiped.append(previousProfile!.userID)
            if(addLike){
                Utility.sharedInstance.currentUserProfile!.liked += 1
            }
            QBRequest.update(Utility.sharedInstance.currentUserProfile!.object, successBlock: { (response, object) -> Void in
                if(self.cardProfiles.count < 5 && !self.loadingProfiles){
                    //self.loadPendingContactProfiles()
                }
                }, errorBlock: { (response) -> Void in
                    print("Error updating object: \(String(describing: response.error))")
            })
        }


        if(self.cardProfiles.count > 0){
            self.loadedCards.append(self.createDraggableViewWithData(self.cardProfiles[0], rewound: false))
            self.cardProfiles.remove(at: 0)
            if(self.loadedCards.count == 1){
                self.view.addSubview(self.loadedCards[self.loadedCards.count-1])
            }else{
                self.view.insertSubview(self.loadedCards[self.loadedCards.count-1], belowSubview: self.loadedCards[self.loadedCards.count-2])
            }
        }else if(self.loadedCards.count > 0){
            self.loadPendingContactProfiles()
        }else{
            self.hasCards = false
            self.loadPendingContactProfiles()
            if(self.searchingAnimationTimer == nil){
                self.setupLookingForUsersView()
                self.searchingAnimationTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(SwipeViewController.setupLookingForUsersView), userInfo: nil, repeats: true)
            }
            //self.setupFindingUsersTimer()
        }
    }
    
    func setupFindingUsersTimer(){
        print("Setup find more users timer")
        self.loadedIDs.removeAll()
        self.findMoreUsersTimer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(SwipeViewController.loadPendingContactProfiles), userInfo: nil, repeats: false)
    }
    
    func swipeRight(){
        let dragView = loadedCards.first
        dragView!.overlayView.mode = CGOverlayViewMode.right
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            dragView!.overlayView.alpha = 1
        }) 
        dragView!.rightClickAction()
    }
    
    func swipeLeft(){
        let dragView = loadedCards.first
        dragView!.overlayView.mode = CGOverlayViewMode.left
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            dragView!.overlayView.alpha = 1
        }) 
        dragView!.leftClickAction()
    }
    
    func showUpgradeScreen(_ animated:Bool){
        self.present(self.appDelegate.upgradeViewController, animated: animated, completion: nil)
    }
    
    func showLimitAlert(){
        let likeLimitView = LikeLimitViewController()
        likeLimitView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        likeLimitView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(likeLimitView, animated: true, completion: nil)
    }
    
    func chatDidReceiveRejectContactRequest(fromUser userID: UInt) {
        //print(QBChat.instance().contactList)
    }
    
    func chatDidReceiveAcceptContactRequest(fromUser userID: UInt) {
        if let timer = pushTimers[userID]{
            timer.invalidate()
        }
        if let _ = Utility.sharedInstance.currentUserProfile?.matched.index(of: userID){
            return
        }
        Utility.sharedInstance.currentUserProfile?.matched.append(userID)
        print("User \(userID) did accept request, it's a match!")
        self.presentMatchScreen(userID)
        print(QBChat.instance().contactList as Any)
    }
    
    func chatDidReceiveContactAddRequest(fromUser userID: UInt) {
        self.pendingUsers.append(userID)
        print("Received contact add request from: \(userID)")
        print(QBChat.instance().contactList as Any)
    }
    
    func presentMatchScreen(_ userID:UInt){
        Utility.sharedInstance.currentUserProfile?.matched.append(userID)
        Utility.sharedInstance.uploadCurrentUser(nil, errorBlock: nil)
        let params = NSMutableDictionary()
        params.setObject(userID, forKey: "user_id" as NSCopying)
        QBRequest.objects(withClassName: "profile", extendedRequest: params, successBlock: { (response, objects, page) -> Void in
            if(objects != nil && objects!.count > 0){
                let profile = UserProfile(object: objects![0])
                if(self.isViewLoaded && self.view.window != nil){
                    let dialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
                    dialog.occupantIDs = [NSNumber(value: userID), NSNumber(value: QBSession.current().currentUser!.id)]
                    QBRequest.createDialog(dialog, successBlock: { (response, dialog) -> Void in
                        if(dialog != nil){
                            ChatService.sharedInstance.updateOrInsertDialog(dialog!)
                            let matchViewController = MatchViewController(profile: profile, delegate: self, dialog:dialog!)
                            matchViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            matchViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.present(matchViewController, animated: true, completion: nil)
                            })
                        }
                        }, errorBlock: { (response) -> Void in
                            print("Error: \(String(describing: response.error))")
                    })
                }else{
                    JDStatusBarNotification.show(withStatus: "You Have a New Match!", dismissAfter: 3.5, styleName: "newMatchStyle")
                }
            }
            }, errorBlock: { (response) -> Void in
                print("Error getting pending request profiles: \(String(describing: response.error))")
        })
    }
    
    func toChatScreen(_ dialog:QBChatDialog?){
        if(dialog != nil){
            let chatViewController = ChatViewController(dialog: dialog!)
            self.navigationController?.pushViewController(self.appDelegate.dialogsViewController, animated: false)
            self.navigationController?.pushViewController(chatViewController, animated: true)
        }else{
            self.navigationController?.pushViewController(self.appDelegate.dialogsViewController, animated: true)
        }
    }
    
    @objc func toChatsScreen(){
        self.navigationController?.pushViewController(self.appDelegate.dialogsViewController, animated: true)
    }
    
    func registerForRemoteNotifications(){
        let settings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
    }
}
