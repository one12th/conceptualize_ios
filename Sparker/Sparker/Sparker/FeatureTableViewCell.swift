//
//  FeatureTableViewCell.swift
//  Sparker
//
//  Created by Samuel Harrison on 16/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class FeatureTableViewCell: UITableViewCell {

    let imageContainer = UIView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(self.imageContainer)
        self.imageView?.removeFromSuperview()
        self.imageContainer.addSubview(self.imageView!)
        self.imageContainer.backgroundColor = AppConfig.upgradeScreenFeatureImageBackgroundColor
        self.textLabel!.textColor = AppConfig.upgradeScreenFeatureTitleTextColor
        self.textLabel!.font = AppConfig.upgradeScreenFeatureTitleFont
        self.detailTextLabel!.textColor = AppConfig.upgradeScreenFeatureSubtitleTextColor
        self.detailTextLabel!.font = AppConfig.upgradeScreenFeatureSubtitleFont
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imageView!.frame = CGRect(x: 14, y: 14, width: self.bounds.size.height - 52, height: self.bounds.size.height - 52)
        self.imageContainer.frame = CGRect(x: 12, y: 12, width: self.bounds.size.height - 24, height: self.bounds.size.height - 24)
        self.imageContainer.layer.cornerRadius = (self.bounds.size.height - 24)/2
        self.imageContainer.layer.borderWidth = AppConfig.upgradeScreenFeatureImageBorderWidth
        self.imageContainer.layer.borderColor = AppConfig.upgradeScreenFeatureImageBorderColor.cgColor
        
        var frame = self.textLabel!.frame
        frame.origin.x = self.bounds.size.height
        frame.size.width = self.bounds.size.width - 8 - frame.origin.x
        self.textLabel!.frame = frame
        
        frame = self.detailTextLabel!.frame
        frame.origin.x = self.bounds.size.height
        frame.size.width = self.bounds.size.width - 8 - frame.origin.x
        self.detailTextLabel!.frame = frame
    }

}
