//
//  UpgradeViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 16/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import StoreKit
import MBProgressHUD

class UpgradeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SKProductsRequestDelegate, SKPaymentTransactionObserver{

    let backgroundView = UIView()
    var tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.grouped)
    var upgradeButton = UIButton()
    var dialogView = UIView()
    let productLoadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    var productsRequest: SKProductsRequest?
    var product: SKProduct?
    var transactionHUD: MBProgressHUD?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(white: 0.0, alpha: 0.7)
        
        self.backgroundView.frame = self.view.bounds
        self.view.addSubview(self.backgroundView)
        
        /*self.dialogView.backgroundColor = UIColor.whiteColor()
        self.dialogView.layer.cornerRadius = 25.0
        self.dialogView.frame = CGRect(x: 16, y: (self.view.bounds.size.height - 297.0)/2, width: self.view.bounds.size.width - 32, height: 297.0)
        self.view.addSubview(self.dialogView)*/
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let tableViewHeight = 88.0*2 + CGFloat(self.tableView.numberOfRows(inSection: 0)*77)
        self.tableView.frame = CGRect(x: 16, y: (self.view.bounds.size.height - tableViewHeight)/2, width: self.view.bounds.size.width - 32, height: tableViewHeight)
        self.tableView.backgroundColor = AppConfig.upgradeScreenBackgroundColor
        self.tableView.bounces = false
        self.tableView.layer.cornerRadius = AppConfig.upgradeScreenCornerRadius
        self.tableView.clipsToBounds = true
        self.view.addSubview(self.tableView)
        
        self.upgradeButton.frame = CGRect(x: 20, y: (88.0 - 55.0)/2, width: self.tableView.frame.size.width - 40, height: 55.0)
        self.upgradeButton.backgroundColor = AppConfig.upgradeScreenUpgradeButtonBackgroundColor
        self.upgradeButton.setTitle("Get Plus", for: UIControlState())
        self.upgradeButton.setTitleColor(AppConfig.upgradeScreenUpgradeButtonTextColor, for: UIControlState())
        self.upgradeButton.titleLabel!.font = AppConfig.upgradeScreenUpgradeButtonFont
        self.upgradeButton.titleLabel!.adjustsFontSizeToFitWidth = true
        self.upgradeButton.layer.cornerRadius = AppConfig.upgradeScreenUpgradeButtonCornerRadius
        self.upgradeButton.layer.borderColor = AppConfig.upgradeScreenUpgradeButtonBorderColor.cgColor
        self.upgradeButton.layer.borderWidth = AppConfig.upgradeScreenUpgradeButtonBorderWidth
        self.upgradeButton.addTarget(self, action: #selector(UpgradeViewController.didTapUpgradeButton), for: UIControlEvents.touchUpInside)
        
        self.validateProductIdentifier()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UpgradeViewController.didTapBackgroundView))
        self.backgroundView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(self.product == nil && self.productsRequest == nil){
            self.validateProductIdentifier()
        }
    }
    
    func validateProductIdentifier(){
        self.upgradeButton.setTitle("", for: UIControlState())
        self.productLoadingIndicator.startAnimating()
        self.productsRequest = SKProductsRequest(productIdentifiers: [AppConfig.IAPProductID])
        self.productsRequest!.delegate = self
        self.productsRequest?.start()
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        self.productsRequest = nil
        if(response.products.count > 0){
            self.product = response.products[0]
        }
        for invalidIdentifier in response.invalidProductIdentifiers{
            print("Invalid product identifier: \(invalidIdentifier)")
        }
        self.updateUpgradeButton()
    }
    
    func updateUpgradeButton(){
        self.productLoadingIndicator.stopAnimating()
        if(!SKPaymentQueue.canMakePayments() || self.product == nil){
            self.upgradeButton.setTitle("Store Unavailable", for: UIControlState())
            return
        }
        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = NumberFormatter.Behavior.behavior10_4
        numberFormatter.numberStyle = NumberFormatter.Style.currency
        numberFormatter.locale = self.product!.priceLocale
        self.upgradeButton.setTitle("Get Plus for \(numberFormatter.string(from: self.product!.price)!)/mo", for: UIControlState())
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppConfig.featureTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FeatureTableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "FeaturesCell")
        
        cell.textLabel!.text = AppConfig.featureTitles[indexPath.row]
        cell.detailTextLabel!.text = AppConfig.featureSubtitles[indexPath.row]
        
        cell.backgroundColor = AppConfig.upgradeScreenFeatureBackgroundColor
        
        var image = UIImage(named: AppConfig.featureImages[indexPath.row])
        if(AppConfig.upgradeScreenFeatureImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        cell.imageView!.image = image
        cell.imageView!.tintColor = AppConfig.featureColors[indexPath.row]
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: 88.0))
        let headerImageView = UIImageView(frame: CGRect(x: 0, y: (88.0 - 66.0)/2, width: self.view.bounds.size.width - 32, height: 66.0))
        var image = UIImage(named: AppConfig.upgradeScreenLogoImage)
        if(AppConfig.upgradeScreenLogoImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        headerImageView.image = image
        headerImageView.tintColor = AppConfig.upgradeScreenLogoImageColor
        headerImageView.contentMode = UIViewContentMode.scaleAspectFit
        view.addSubview(headerImageView)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 88.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: 88.0))
        view.addSubview(self.upgradeButton)
        self.productLoadingIndicator.center = self.upgradeButton.center
        view.addSubview(self.productLoadingIndicator)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 88.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didTapUpgradeButton(){
        if(self.product == nil){
            return
        }
        let payment = SKMutablePayment(product: self.product!)
        SKPaymentQueue.default().add(payment)
    }
    
    @objc func didTapBackgroundView(){
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions{
            switch(transaction.transactionState){
                
            case SKPaymentTransactionState.purchasing:
                self.processingTransaction(transaction)
                break
                
            case SKPaymentTransactionState.deferred:
                self.processingTransaction(transaction)
                break
                
            case SKPaymentTransactionState.failed:
                self.failedTransaction(transaction)
                SKPaymentQueue.default().finishTransaction(transaction)
                break
                
            case SKPaymentTransactionState.purchased:
                self.completedTransaction(transaction)
                //SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                break
                
            case SKPaymentTransactionState.restored:
                self.completedTransaction(transaction)
                break
            }
        }
    }
    
    func processingTransaction(_ transaction:SKPaymentTransaction){
        self.transactionHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func failedTransaction(_ transaction:SKPaymentTransaction){
        if(self.transactionHUD != nil){
            self.transactionHUD!.mode = MBProgressHUDMode.text
            self.transactionHUD!.labelText = "Purchase Failed"
            self.transactionHUD!.hide(true, afterDelay: 3.0)
        }
        
        let alert = UIAlertController(title: "Error", message: transaction.error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
        if(self.isViewLoaded && self.view.window != nil){
            DispatchQueue.main.async(execute: { () -> Void in
                self.present(alert, animated: true, completion: nil)
            })
        }
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    func completedTransaction(_ transaction:SKPaymentTransaction){
        self.appDelegate.validateReceipt(Bundle.main.appStoreReceiptURL!) { (validated) -> Void in
            if(self.transactionHUD != nil){
                DispatchQueue.main.async(execute: { () -> Void in
                    self.transactionHUD!.mode = MBProgressHUDMode.text
                    if(transaction.transactionState == SKPaymentTransactionState.restored){
                        self.transactionHUD!.labelText = "Restore Complete"
                    }else{
                        self.transactionHUD!.labelText = "Purchase Complete"
                    }
                    self.transactionHUD!.hide(true, afterDelay: 3.0)
                    self.presentingViewController?.dismiss(animated: true, completion: nil)
                })
            }
        }
        SKPaymentQueue.default().finishTransaction(transaction)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
