//
//  LikeLimitViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 14/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class LikeLimitViewController: UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let backgroundView = UIView()
    let dialogView = UIView()
    let countdownLabel = UILabel()
    let upgradeButton = UIButton()
    var timer:Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(white: 0.0, alpha: 0.7)
        
        self.backgroundView.frame = self.view.bounds
        self.view.addSubview(self.backgroundView)
        
        self.dialogView.backgroundColor = AppConfig.likeLimitScreenBackgroundColor
        self.dialogView.layer.cornerRadius = AppConfig.likeLimitScreenCornerRadius
        self.dialogView.frame = CGRect(x: 16, y: (self.view.bounds.size.height - 275.0)/2, width: self.view.bounds.size.width - 32, height: 275.0)
        self.view.addSubview(self.dialogView)
        
        let emptyHeart = UIButton(frame: CGRect(x: (self.view.bounds.size.width - 32 - 100)/2, y: -50, width: 100, height: 100))
        var image = UIImage(named: AppConfig.likeLimitScreenImage)
        if(AppConfig.likeLimitScreenImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        emptyHeart.setImage(image, for: UIControlState())
        emptyHeart.imageEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        emptyHeart.tintColor = AppConfig.likeLimitScreenImageColor
        emptyHeart.backgroundColor = AppConfig.likeLimitScreenBackgroundColor
        emptyHeart.layer.cornerRadius = 50
        emptyHeart.isEnabled = false
        self.dialogView.addSubview(emptyHeart)
        
        let headingLabel = UILabel(frame: CGRect(x: 20, y: 50, width: self.dialogView.frame.size.width - 40, height: 30.0))
        headingLabel.font = AppConfig.likeLimitScreenOutOfTextFont
        headingLabel.textColor = AppConfig.likeLimitScreenOutOfTextColor
        headingLabel.text = "You're Out of Likes"
        headingLabel.textAlignment = NSTextAlignment.center
        self.dialogView.addSubview(headingLabel)
        
        let subheadingLabel = UILabel(frame: CGRect(x: 20, y: 100, width: self.dialogView.frame.size.width - 40, height: 20.0))
        subheadingLabel.font = AppConfig.likeLimitScreenGetMoreInFont
        subheadingLabel.text = "Get more likes in:"
        subheadingLabel.textAlignment = NSTextAlignment.center
        subheadingLabel.textColor = AppConfig.likeLimitScreenGetMoreInTextColor
        self.dialogView.addSubview(subheadingLabel)
        
        self.countdownLabel.frame = CGRect(x: 20, y: 125, width: self.dialogView.frame.size.width - 40, height: 36.0)
        
        let fontFeatures = [
            [UIFontDescriptor.FeatureKey.featureIdentifier: kNumberSpacingType,
                UIFontDescriptor.FeatureKey.typeIdentifier: kMonospacedNumbersSelector]
        ]
        let descriptorWithFeatures = AppConfig.likeLimitScreenCountdownFont.fontDescriptor.addingAttributes([UIFontDescriptor.AttributeName.featureSettings: fontFeatures])
        self.countdownLabel.font = UIFont(descriptor: descriptorWithFeatures, size: 0)
        self.countdownLabel.textAlignment = NSTextAlignment.center
        self.countdownLabel.textColor = AppConfig.likeLimitScreenCountdownTextColor
        self.dialogView.addSubview(self.countdownLabel)
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(LikeLimitViewController.updateCountdownTimer), userInfo: nil, repeats: true)
        self.updateCountdownTimer()
        
        self.upgradeButton.frame = CGRect(x: 20, y: self.dialogView.frame.size.height - 86, width: self.dialogView.frame.size.width - 40, height: 66.0)
        self.upgradeButton.backgroundColor = AppConfig.likeLimitScreenGetUnlimitedButtonBackgroundColor
        self.upgradeButton.setTitle("Get Unlimited Likes", for: UIControlState())
        self.upgradeButton.setTitleColor(AppConfig.likeLimitScreenGetUnlimitedButtonTextColor, for: UIControlState())
        self.upgradeButton.titleLabel!.font = AppConfig.likeLimitScreenGetUnlimitedButtonFont
        self.upgradeButton.layer.cornerRadius = AppConfig.likeLimitScreenGetUnlimitedButtonCornerRadius
        self.upgradeButton.layer.borderColor = AppConfig.likeLimitScreenGetUnlimitedButtonBorderColor.cgColor
        self.upgradeButton.layer.borderWidth = AppConfig.likeLimitScreenGetUnlimitedButtonBorderWidth
        self.upgradeButton.addTarget(self, action: #selector(LikeLimitViewController.didTapUpgradeButton), for: UIControlEvents.touchUpInside)
        self.dialogView.addSubview(self.upgradeButton)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LikeLimitViewController.didTapBackgroundView))
        self.backgroundView.addGestureRecognizer(tapGestureRecognizer)
        // Do any additional setup after loading the view.
    }

    @objc func updateCountdownTimer(){
        let interval = Int(Utility.sharedInstance.currentUserProfile!.likeLimitLifted.timeIntervalSince(Date()))
        if(interval <= 0){
            self.didTapBackgroundView()
            self.timer!.invalidate()
            return
        }
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let hours = (interval / 3600)
        countdownLabel.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    @objc func didTapUpgradeButton(){
        self.presentingViewController?.dismiss(animated: true, completion: nil)
        self.appDelegate.swipeViewController.showUpgradeScreen(true)
    }
    
    @objc func didTapBackgroundView(){
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
