//
//  MatchViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 05/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class MatchViewController: UIViewController {

    var userProfileImage = UIImageView()
    var matchProfileImage = UIImageView()
    var matchProfile:UserProfile
    var delegate:SwipeViewController
    var dialog:QBChatDialog
    
    init(profile:UserProfile, delegate:SwipeViewController, dialog:QBChatDialog){
        self.matchProfile = profile
        self.delegate = delegate
        self.dialog = dialog
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.9)
        
        let imageSize = (self.view.bounds.size.width - 32)/3
        
        let matchText = UILabel(frame: CGRect(x: 16, y: self.view.bounds.size.height/2 - imageSize - 100, width: self.view.bounds.size.width - 32, height: 70))
        matchText.text = "You Matched!"
        matchText.font = AppConfig.matchScreenYouMatchedFont
        matchText.numberOfLines = 1
        matchText.adjustsFontSizeToFitWidth = true
        matchText.textColor = AppConfig.matchScreenYouMatchedTextColor
        matchText.textAlignment = NSTextAlignment.center
        self.view.addSubview(matchText)
        
        let matchSubtext = UILabel(frame: CGRect(x: 16, y: self.view.bounds.size.height/2 - imageSize - 32, width: self.view.bounds.size.width - 32, height: 22))
        matchSubtext.font = UIFont.systemFont(ofSize: 17.0)
        matchSubtext.text = "You and \(self.matchProfile.name!) liked each other."
        matchSubtext.textColor = AppConfig.matchScreenSubTitleTextColor
        matchSubtext.textAlignment = NSTextAlignment.center
        self.view.addSubview(matchSubtext)
        
        self.userProfileImage.frame = CGRect(x: self.view.bounds.size.width/2 - imageSize*1.1, y: self.view.bounds.size.height/2 - 0.75*imageSize, width: imageSize, height: imageSize)
        self.userProfileImage.clipsToBounds = true
        self.userProfileImage.contentMode = UIViewContentMode.scaleAspectFill
        self.userProfileImage.layer.cornerRadius = imageSize/2.0
        self.userProfileImage.layer.borderWidth = 4.0
        self.userProfileImage.layer.borderColor = AppConfig.matchScreenProfileImageBorderColor.cgColor
        self.userProfileImage.image = UIImage(named: "PlaceholderImage")
        if let url = Utility.sharedInstance.currentUserProfile?.getProfilePictureURL(){
            self.userProfileImage.setImageWith(url as URL, placeholderImage: UIImage(named: "PlaceholderImage"))
        }
        self.view.addSubview(self.userProfileImage)
        
        self.matchProfileImage.frame = CGRect(x: self.view.bounds.size.width/2 + imageSize*0.1, y: self.view.bounds.size.height/2 - 0.75*imageSize, width: imageSize, height: imageSize)
        self.matchProfileImage.clipsToBounds = true
        self.matchProfileImage.contentMode = UIViewContentMode.scaleAspectFill
        self.matchProfileImage.layer.cornerRadius = imageSize/2.0
        self.matchProfileImage.layer.borderWidth = 4.0
        self.matchProfileImage.layer.borderColor = AppConfig.matchScreenProfileImageBorderColor.cgColor
        self.matchProfileImage.image = UIImage(named: "PlaceholderImage")
        if let url = self.matchProfile.getProfilePictureURL(){
            self.matchProfileImage.setImageWith(url as URL, placeholderImage: UIImage(named: "PlaceholderImage"))
        }
        self.view.addSubview(self.matchProfileImage)
        
        let messageButton = UIButton(frame: CGRect(x: 32, y: self.view.bounds.size.height/2 + imageSize, width: self.view.bounds.size.width - 64, height: 55))
        messageButton.setTitle("Send a Message", for: UIControlState())
        messageButton.setTitleColor(AppConfig.matchScreenMessageButtonTextColor, for: UIControlState())
        messageButton.layer.borderColor = AppConfig.matchScreenMessageButtonBorderColor.cgColor
        messageButton.layer.borderWidth = 1.0
        messageButton.layer.cornerRadius = 5
        messageButton.addTarget(self, action: #selector(MatchViewController.didTapMessageButton), for: UIControlEvents.touchUpInside)
        self.view.addSubview(messageButton)
        
        let continueButton = UIButton(frame: CGRect(x: 32, y: self.view.bounds.size.height/2 + imageSize + 71, width: self.view.bounds.size.width - 64, height: 55))
        continueButton.setTitle("Keep Swiping", for: UIControlState())
        continueButton.setTitleColor(AppConfig.matchScreenContinueButtonTextColor, for: UIControlState())
        continueButton.layer.borderColor = AppConfig.matchScreenContinueButtonBorderColor.cgColor
        continueButton.layer.borderWidth = 1.0
        continueButton.layer.cornerRadius = 5
        continueButton.addTarget(self, action: #selector(MatchViewController.didTapContinueButton), for: UIControlEvents.touchUpInside)
        self.view.addSubview(continueButton)

    }
    
    @objc func didTapMessageButton(){
        self.presentingViewController?.dismiss(animated: true, completion: nil)
        self.delegate.toChatScreen(self.dialog)
    }
    
    @objc func didTapContinueButton(){
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
}
