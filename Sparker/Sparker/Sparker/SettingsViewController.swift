//
//  SettingsViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 04/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import NMRangeSlider
import MBProgressHUD
import StoreKit
import SWRevealViewController

class SettingsViewController: UITableViewController, SKRequestDelegate{
    
    let ageRangeLabel = UILabel()
    let distanceLabel = UILabel()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let menSwitch = UISwitch()
    let womenSwitch = UISwitch()
    var updatingSettings = false
    var settingsChanged = false
    var restoreProgressHUD: MBProgressHUD?
    
    override init(style: UITableViewStyle) {
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        self.navigationItem.title = "Settings"
        
        if(self.appDelegate.firstLaunch){
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Finish", style: UIBarButtonItemStyle.done, target: self, action: #selector(SettingsViewController.didTapFinishButton))
        }else{
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "IconMenu"), style: UIBarButtonItemStyle.plain, target: self.appDelegate.masterViewContainer, action: #selector(SWRevealViewController.revealToggle(_:)))
        }
        self.view.backgroundColor = AppConfig.settingsScreenBackgroundColor
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.settingsChanged = false
        super.viewDidAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if(self.settingsChanged){
            NotificationCenter.default.post(name: Notification.Name(rawValue: "com.samuelharrison.sparker.utility.userSettingsChanged"), object: nil)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if(self.appDelegate.firstLaunch){
            return 4
        }
        if(!AppConfig.enableCredits){
            return 7
        }
        return 8
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch(section){
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            return 2
        case 4:
            return 1
        case 5:
            return 1
        default:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 4 || section == 5 || section == 6 || section == 7){
            return 5.0
        }
        return 55.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5 || indexPath.section == 6 || indexPath.section == 7){
            return 55.0
        }
        return 66.0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 44.0))
        view.backgroundColor = AppConfig.settingsScreenBackgroundColor
        let headingLabel = UILabel(frame: CGRect(x: 16, y: 22.0, width: self.view.bounds.size.width/2 - 16, height: 33.0))
        headingLabel.textAlignment = NSTextAlignment.left
        headingLabel.textColor = AppConfig.settingsScreenSettingHeadingTextColor
        headingLabel.font = AppConfig.settingsScreenSettingHeadingFont
        view.addSubview(headingLabel)
        switch(section){
        case 0:
            headingLabel.text = "I Am:"
            return view
        case 1:
            headingLabel.text = "Show Ages:"
            self.ageRangeLabel.frame = CGRect(x: self.view.bounds.size.width/2, y: 22.0, width: self.view.bounds.size.width/2 - 16, height: 33.0)
            self.ageRangeLabel.textAlignment = NSTextAlignment.right
            self.ageRangeLabel.textColor = AppConfig.settingsScreenSelectedValueTextColor
            self.ageRangeLabel.font = AppConfig.settingsScreenSelectedValueFont
            view.addSubview(self.ageRangeLabel)
            return view
        case 2:
            headingLabel.text = "Search Distance:"
            self.distanceLabel.frame = CGRect(x: self.view.bounds.size.width/2, y: 22.0, width: self.view.bounds.size.width/2 - 16, height: 33.0)
            self.distanceLabel.textAlignment = NSTextAlignment.right
            self.distanceLabel.textColor = AppConfig.settingsScreenSelectedValueTextColor
            self.distanceLabel.font = AppConfig.settingsScreenSelectedValueFont
            view.addSubview(self.distanceLabel)
            return view
        case 3:
            headingLabel.text = "Show Me:"
            return view
        default:
            return view
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return ""
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell!
        
        switch(indexPath.section){
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "segmentSettingsCell") as? SegmentSettingsCell
            if(cell == nil){
                cell = SegmentSettingsCell(style: UITableViewCellStyle.default, reuseIdentifier: "segmentSettingsCell")
                (cell as! SegmentSettingsCell).segmentControl.insertSegment(withTitle: "Male", at: 0, animated: false)
                (cell as! SegmentSettingsCell).segmentControl.insertSegment(withTitle: "Female", at: 1, animated: false)
                (cell as! SegmentSettingsCell).segmentControl.tintColor = AppConfig.settingsScreenGenderSelectorColor
                (cell as! SegmentSettingsCell).segmentControl.addTarget(self, action: #selector(SettingsViewController.genderChanged(_:)), for: UIControlEvents.valueChanged)
            }
            cell!.backgroundColor = AppConfig.settingsScreenGenderSelectorCellBackgroundColor
            (cell as! SegmentSettingsCell).segmentControl.selectedSegmentIndex = Utility.sharedInstance.currentUserProfile!.gender
            break
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "rangeSliderSettingsCell") as? RangeSliderSettingsCell
            if(cell == nil){
                cell = RangeSliderSettingsCell(style: UITableViewCellStyle.default, reuseIdentifier: "rangeSliderSettingsCell")
            }
            (cell as! RangeSliderSettingsCell).slider.maximumValue = 60
            (cell as! RangeSliderSettingsCell).slider.minimumValue = 18
            (cell as! RangeSliderSettingsCell).slider.stepValue = 1.0
            (cell as! RangeSliderSettingsCell).slider.minimumRange = 1.0
            (cell as! RangeSliderSettingsCell).slider.upperValue = Float(appDelegate.userSettings["agemax"]!)
            (cell as! RangeSliderSettingsCell).slider.lowerValue = Float(appDelegate.userSettings["agemin"]!)
            (cell as! RangeSliderSettingsCell).slider.continuous = true
            (cell as! RangeSliderSettingsCell).slider.addTarget(self, action: #selector(SettingsViewController.ageValueChanged(_:)), for: UIControlEvents.valueChanged)
            (cell as! RangeSliderSettingsCell).slider.tintColor = AppConfig.settingsScreenAgesSliderColor
            cell!.backgroundColor = AppConfig.settingsScreenAgesCellBackgroundColor
            self.ageValueChanged((cell as! RangeSliderSettingsCell).slider)
            break
            
        case 2:
            cell = tableView.dequeueReusableCell(withIdentifier: "sliderSettingsCell") as? SliderSettingsCell
            if(cell == nil){
                cell = SliderSettingsCell(style: UITableViewCellStyle.default, reuseIdentifier: "sliderSettingsCell")
            }
            (cell as! SliderSettingsCell).slider.minimumValue = 5
            (cell as! SliderSettingsCell).slider.maximumValue = AppConfig.maximumSearchRadius
            (cell as! SliderSettingsCell).slider.isContinuous = true
            (cell as! SliderSettingsCell).slider.addTarget(self, action: #selector(SettingsViewController.distanceValueChanged(_:)), for: UIControlEvents.valueChanged)
            (cell as! SliderSettingsCell).slider.tintColor = AppConfig.settingsScreenDistanceSliderColor
            (cell as! SliderSettingsCell).slider.value = Float(appDelegate.userSettings["distance"]!)
            cell!.backgroundColor = AppConfig.settingsScreenDistanceCellBackgroundColor
            self.distanceValueChanged((cell as! SliderSettingsCell).slider)
            break
            
        case 3:
            cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell")
            if(cell == nil){
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "settingsCell")
            }
            if(indexPath.row == 0){
                cell.textLabel!.text = "Men"
                cell!.backgroundColor = AppConfig.settingsScreenShowMeMenCellBackgroundColor
                self.menSwitch.onTintColor = AppConfig.settingsScreenShowMeMenButtonColor
                cell.accessoryView = self.menSwitch
                (cell.accessoryView as! UISwitch).addTarget(self, action: #selector(SettingsViewController.interestedInChanged(_:)), for: UIControlEvents.valueChanged)
                (cell.accessoryView as! UISwitch).tag = 0
                if(Utility.sharedInstance.currentUserProfile!.interestedIn == 0 || Utility.sharedInstance.currentUserProfile!.interestedIn == 2){
                    (cell.accessoryView as! UISwitch).setOn(true, animated: false)
                }else{
                    (cell.accessoryView as! UISwitch).setOn(false, animated: false)
                }
            }else{
                cell.textLabel!.text = "Women"
                cell!.backgroundColor = AppConfig.settingsScreenShowMeWomenCellBackgroundColor
                self.womenSwitch.onTintColor = AppConfig.settingsScreenShowMeWomenButtonColor
                cell.accessoryView = self.womenSwitch
                (cell.accessoryView as! UISwitch).addTarget(self, action: #selector(SettingsViewController.interestedInChanged(_:)), for: UIControlEvents.valueChanged)
                (cell.accessoryView as! UISwitch).tag = 1
                if(Utility.sharedInstance.currentUserProfile!.interestedIn == 1 || Utility.sharedInstance.currentUserProfile!.interestedIn == 2){
                    (cell.accessoryView as! UISwitch).setOn(true, animated: false)
                }else{
                    (cell.accessoryView as! UISwitch).setOn(false, animated: false)
                }
            }
            break
        case 4:
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: nil)
            cell.textLabel!.text = "Logout"
            cell.textLabel!.textAlignment = NSTextAlignment.center
            cell.textLabel!.font = AppConfig.settingsScreenLogoutFont
            cell.textLabel?.textColor = AppConfig.settingsScreenLogoutTextColor
            cell.backgroundColor = AppConfig.settingsScreenLogoutBackgroundColor
            break
        case 5:
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: nil)
            cell.textLabel!.text = "Restore Purchases"
            cell.textLabel!.textAlignment = NSTextAlignment.center
            cell.textLabel!.font = AppConfig.settingsScreenRestorePurchasesFont
            cell.textLabel?.textColor = AppConfig.settingsScreenRestorePurchasesTextColor
            cell.backgroundColor = AppConfig.settingsScreenRestorePurchasesBackgroundColor
            break
        case 6:
            if(AppConfig.enableCredits){
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: nil)
                cell.textLabel!.text = "Credits"
                cell.textLabel!.textAlignment = NSTextAlignment.center
                cell.textLabel!.font = AppConfig.settingsScreenCreditsFont
                cell.textLabel?.textColor = AppConfig.settingsScreenCreditsTextColor
                cell.backgroundColor = AppConfig.settingsScreenCreditsBackgroundColor
                break
            }
            fallthrough
        case 7:
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: nil)
            cell.textLabel!.text = "Delete Account"
            cell.textLabel!.textAlignment = NSTextAlignment.center
            cell.textLabel!.font = AppConfig.settingsScreenDeleteAccountFont
            cell.textLabel?.textColor = AppConfig.settingsScreenDeleteAccountTextColor
            cell.backgroundColor = AppConfig.settingsScreenDeleteAccountBackgroundColor
            break
            
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell")
            if(cell == nil){
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "settingsCell")
            }
            break
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.section == 4){
            self.appDelegate.logOut()
        }
        if(indexPath.section == 5){
            self.restorePurchase()
        }
        if(indexPath.section == 6){
            if(!AppConfig.enableCredits){
                self.deleteAccount()
                return
            }
            let actionSheet = UIAlertController(title: "Credits", message: "All icons made by FreePik (www.freepik.com) from FlatIcon (www.flaticon.com) and licensed under CC BY 3.0", preferredStyle: UIAlertControllerStyle.alert)
            actionSheet.addAction(UIAlertAction(title: "Visit FreePik", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                UIApplication.shared.openURL(URL(string: "http://www.freepik.com")!)
            }))
            actionSheet.addAction(UIAlertAction(title: "Visit FlatIcon", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                UIApplication.shared.openURL(URL(string: "http://www.flaticon.com")!)
            }))
            actionSheet.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(actionSheet, animated: true, completion: nil)
        }
        if(indexPath.section == 7){
            self.deleteAccount()
        }
    }
    
    @objc func genderChanged(_ segmentController:UISegmentedControl){
        if(self.updatingSettings){
            return
        }
        self.updatingSettings = true
        self.settingsChanged = true
        Utility.sharedInstance.currentUserProfile!.gender = segmentController.selectedSegmentIndex
        MBProgressHUD.showAdded(to: self.navigationController!.view, animated: true)
        Utility.sharedInstance.uploadCurrentUser({ () -> Void in
            self.updatingSettings = false
            DispatchQueue.main.async(execute: { () -> Void in
                MBProgressHUD.hide(for: self.navigationController!.view, animated: true)
            })
            }) { () -> Void in
                self.updatingSettings = false
                DispatchQueue.main.async(execute: { () -> Void in
                    MBProgressHUD.hide(for: self.navigationController!.view, animated: true)
                })
                let alert = UIAlertController(title: "Oops", message: "There was an error updating your settings", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                segmentController.selectedSegmentIndex = abs(segmentController.selectedSegmentIndex - 1)
        }
    }
    
    @objc func interestedInChanged(_ aSwitch:UISwitch){
        if(self.updatingSettings){
            return
        }
        self.updatingSettings = true
        self.settingsChanged = true
        if(self.menSwitch.isOn && self.womenSwitch.isOn){
            Utility.sharedInstance.currentUserProfile!.interestedIn = 2
        }else if(self.menSwitch.isOn){
            Utility.sharedInstance.currentUserProfile!.interestedIn = 0
        }else if(self.womenSwitch.isOn){
            Utility.sharedInstance.currentUserProfile!.interestedIn = 1
        }else{
            Utility.sharedInstance.currentUserProfile!.interestedIn = -1
        }
        MBProgressHUD.showAdded(to: self.navigationController!.view, animated: true)
        Utility.sharedInstance.uploadCurrentUser({ () -> Void in
            self.updatingSettings = false
            DispatchQueue.main.async(execute: { () -> Void in
                MBProgressHUD.hide(for: self.navigationController!.view, animated: true)
            })
            }) { () -> Void in
                self.updatingSettings = false
                DispatchQueue.main.async(execute: { () -> Void in
                    MBProgressHUD.hide(for: self.navigationController!.view, animated: true)
                })
                let alert = UIAlertController(title: "Oops", message: "There was an error updating your settings", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                aSwitch.setOn(!aSwitch.isOn, animated: true)
        }
    }
    
    @objc func ageValueChanged(_ slider:NMRangeSlider){
        self.settingsChanged = true
        let lowerValue = Int(round(slider.lowerValue))
        let upperValue = Int(round(slider.upperValue))
        self.ageRangeLabel.text = "\(lowerValue) - \(upperValue)"
        if(upperValue == 60){
            self.ageRangeLabel.text! += "+"
        }
        appDelegate.userSettings["agemin"] = lowerValue
        appDelegate.userSettings["agemax"] = upperValue
    }
    
    @objc func distanceValueChanged(_ slider:UISlider){
        self.settingsChanged = true
        slider.value = round(slider.value)
        self.distanceLabel.text = "\(Int(slider.value))km"
        appDelegate.userSettings["distance"] = Int(round(slider.value))
    }
    
    func deleteAccount(){
        let alertView = UIAlertController(title: "Warning", message: "This will delete your entire account including your matches and messages", preferredStyle: UIAlertControllerStyle.alert)
        alertView.addAction(UIAlertAction(title: "Delete Account", style: UIAlertActionStyle.destructive, handler: { (ACTION) -> Void in
            self.confirmedDeleteAccount()
        }))
        alertView.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alertView, animated: true, completion: nil)
    }
    
    func confirmedDeleteAccount(){
        MBProgressHUD.showAdded(to: self.navigationController!.view, animated: true)
        QBRequest.deleteCurrentUser(successBlock: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                MBProgressHUD.hide(for: self.navigationController!.view, animated: true)
                self.appDelegate.logOut()
            })
            }) { (response) -> Void in
                DispatchQueue.main.async(execute: { () -> Void in
                    MBProgressHUD.hide(for: self.navigationController!.view, animated: true)
                })
                let alert = UIAlertController(title: "Error", message: "There was an error deleting you account, please try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
    }
    
    func restorePurchase(){
        self.restoreProgressHUD = MBProgressHUD.showAdded(to: self.navigationController!.view, animated: true)
        self.restoreProgressHUD!.labelText = "Restoring"
        let request = SKReceiptRefreshRequest()
        request.delegate = self
        request.start()
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        if(self.restoreProgressHUD != nil){
            self.restoreProgressHUD!.labelText = "Restore Failed"
            self.restoreProgressHUD!.mode = MBProgressHUDMode.text
            DispatchQueue.main.async(execute: { () -> Void in
                self.restoreProgressHUD!.hide(true, afterDelay: 3.0)
            })
        }
    }
    
    func requestDidFinish(_ request: SKRequest) {
        self.appDelegate.validateReceipt(Bundle.main.appStoreReceiptURL!) { (success) -> Void in
            if(self.restoreProgressHUD != nil){
                DispatchQueue.main.async(execute: { () -> Void in
                    self.restoreProgressHUD!.mode = MBProgressHUDMode.text
                    if(success){
                        self.restoreProgressHUD!.labelText = "Restore Succeeded"
                    }else{
                        self.restoreProgressHUD!.labelText = "Restore Failed"
                    }
                    self.restoreProgressHUD!.hide(true, afterDelay: 3.0)
                })
            }
        }
    }
    
    @objc func didTapFinishButton(){
        self.appDelegate.firstLaunch = false
        MBProgressHUD.showAdded(to: self.navigationController?.view, animated: true)
        Utility.sharedInstance.uploadCurrentUser({ () -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                MBProgressHUD.hide(for: self.navigationController?.view, animated: true)
            })
            self.navigationController?.presentingViewController?.dismiss(animated: true, completion: nil)
            }) { () -> Void in
                DispatchQueue.main.async(execute: { () -> Void in
                    MBProgressHUD.hide(for: self.navigationController?.view, animated: true)
                })
                let alert = UIAlertController(title: "Error", message: "There was an error updating your settings, please try again", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
    }
    
}
