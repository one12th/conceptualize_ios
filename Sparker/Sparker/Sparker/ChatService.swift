//
//  ChatService.swift
//  Sparker
//
//  Created by Samuel Harrison on 10/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import JDStatusBarNotification
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol ChatServiceDelegate{
    func chatDidReceiveMessage(_ message:QBChatMessage)
    //func chatDidReceivePresence(userID:UInt, isOnline:Bool)
}

class ChatService: NSObject, QBChatDelegate {

    static let sharedInstance = ChatService()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var dialogs = [QBChatDialog](){
        didSet{
            sortedDialogs = self.dialogs.sorted(by: { (dialog1, dialog2) -> Bool in
                let t1 = dialog1.lastMessageDate ?? dialog1.createdAt ?? Date.distantPast
                let t2 = dialog2.lastMessageDate ?? dialog2.createdAt ?? Date.distantPast
                return t1.compare(t2) == ComparisonResult.orderedDescending
            })
        }
    }
    var userProfiles = [UserProfile]()
    var sortedDialogs = [QBChatDialog]()
    var delegate:ChatServiceDelegate?
    var messages = [QBChatMessage]()
    
    override init() {
        super.init()
        QBChat.instance().addDelegate(self)
    }
    
    /*func getDialogsLastUpdateDate()->Double{
        return NSUserDefaults.standardUserDefaults().objectForKey("dialogsLastUpdateDate") as? NSTimeInterval ?? NSDate.distantPast().timeIntervalSince1970
    }*/
    
    func getDialogs(){
        QBRequest.dialogs(successBlock: { (response, dialogs, dialogsUsersIDs) -> Void in
            if(dialogs != nil){
                //NSUserDefaults.standardUserDefaults().setObject(NSDate().timeIntervalSince1970, forKey: "dialogsLastUpdateDate")
                for dialog in dialogs!{
                    if(dialog.lastMessageText == "[user_deleted_dialog]"){
                        self.deleteDialog(dialog)
                    }else{
                        self.updateOrInsertDialog(dialog)
                    }
                }
            }
            }) { (response) -> Void in
                print("Error getting dialogs: \(String(describing: response.error))")
        }
    }
    
    func downloadUserProfile(_ userID:UInt){
        let params = NSMutableDictionary()
        params.setObject("\(userID)", forKey: "user_id" as NSCopying)
        QBRequest.objects(withClassName: "profile", extendedRequest: params, successBlock: { (response, objects, page) -> Void in
            if(objects != nil && objects!.count > 0){
                self.userProfiles.append(UserProfile(object: objects![0]))
                NotificationCenter.default.post(name: Notification.Name(rawValue: "com.samuelharrison.utility.dialogsUpdatedNotification"), object: nil)
            }
            }, errorBlock: { (response) -> Void in
                print("Error, profile not found")
        })
    }
    
    func updateOrInsertDialog(_ dialog:QBChatDialog){
        if let index = self.dialogs.index(where: {$0.id == dialog.id}){
            self.dialogs[index] = dialog
        }else{
            self.dialogs.append(dialog)
        }
        
        
        let opponents = dialog.occupantIDs!.filter({$0 != NSNumber(value: QBSession.current().currentUser!.id)})
        if(opponents.count > 0 && self.getUserProfileByID(UInt(truncating: opponents[0])) == nil){
            self.downloadUserProfile(UInt(truncating: opponents[0]))
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: "com.samuelharrison.utility.dialogsUpdatedNotification"), object: nil)
    }
    
    func getDialogByOpponentID(_ userID:UInt)->QBChatDialog?{
        let dialog = self.dialogs.filter {$0.occupantIDs?.contains(NSNumber(value:userID)) ?? false}
        if(dialog.count > 0){
            return dialog[0]
        }else{
            return nil
        }
    }
    
    func getDialogOpponent(_ dialog:QBChatDialog)->UserProfile?{
        let opponents = dialog.occupantIDs!.filter({$0 != NSNumber(value: QBSession.current().currentUser!.id)})
        if(opponents.count > 0){
            return self.getUserProfileByID(UInt(truncating: opponents[0]))
        }
        return nil
    }
    
    func getDialogByID(_ dialogID:String)->QBChatDialog?{
        let dialog = self.dialogs.filter {$0.id == dialogID}
        if(dialog.count > 0){
            return dialog[0]
        }else{
            return nil
        }
    }
    
    func getUserProfileByID(_ userID:UInt)->UserProfile?{
        let profile = self.userProfiles.filter {$0.userID == userID}
        if(profile.count > 0){
            return profile[0]
        }else{
            return nil
        }
    }
    
    func messagesForDialogID(_ dialogID:String)->[QBChatMessage]{
        var messages = self.messages.filter {$0.dialogID == dialogID}
        messages.sort { (message1, message2) -> Bool in
            let t1 = message1.id ?? ""//message1.dateSent ?? NSDate.distantPast()
            let t2 = message2.id ?? ""//message2.dateSent ?? NSDate.distantPast()
            return t1.compare(t2) == ComparisonResult.orderedAscending
        }
        return messages
    }
    
    func addMessages(_ messages:[QBChatMessage]){
        for message in messages{
            if let index = self.messages.index(where: {$0.id == message.id}){
                self.messages[index] = message
            }else{
                self.messages.append(message)
            }
        }
    }
    
    func sendDialogDeleteMessage(_ dialog:QBChatDialog){
        let message = QBChatMessage.markable()
        message.text = "[user_deleted_dialog]"
        message.customParameters = ["save_to_history":true,"delete_dialog":true]
        dialog.send(message) { (error) -> Void in
            if(error != nil){
                print("Error sending delete message: \(String(describing: error))")
            }
        }
    }
    
    func deleteDialog(_ dialog:QBChatDialog){
        var allUsers = false
        if(dialog.userID == Utility.sharedInstance.currentUserProfile?.userID){
            allUsers = true
        }
        QBRequest.deleteDialogs(withIDs: [dialog.id!], forAllUsers: allUsers, successBlock: { (response, _, _, _) -> Void in
            self.sendDialogDeleteMessage(dialog)
            if let index = self.dialogs.index(of: dialog){
                self.dialogs.remove(at: index)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "com.samuelharrison.utility.dialogsUpdatedNotification"), object: nil)
            }
            }, errorBlock: { (response) -> Void in
                print("Error deleting dialog: \(String(describing: response.error))")
        })
    }
    
    func sendMessage(_ messageText: String, dialog:QBChatDialog, errorBlock:@escaping ()->Void){
        let message = QBChatMessage.markable()
        message.text = messageText
        message.customParameters = ["save_to_history":true]
        
        dialog.send(message) { (error) -> Void in
            if(error != nil){
                errorBlock()
            }
            self.addMessages([message])
            self.chatDidReceive(message)
            if(self.delegate != nil){
                self.delegate!.chatDidReceiveMessage(message)
            }
        }
    }
    
    func chatDidReceive(_ message: QBChatMessage) {
        if(message.customParameters != nil && message.customParameters!["delete_dialog"] != nil && message.senderID != Utility.sharedInstance.currentUserProfile!.userID){
            if let dialog = self.getDialogByID(message.dialogID!){
                self.deleteDialog(dialog)
            }
            return
        }
        self.addMessages([message])
        if message.dialogID != nil, let dialog = self.getDialogByID(message.dialogID!){
            
            if(message.senderID != Utility.sharedInstance.currentUserProfile?.userID){
                Utility.sharedInstance.playNotification()
            }
            
            dialog.updatedAt = Date()
            dialog.lastMessageUserID = message.senderID
            dialog.lastMessageText = message.text
            dialog.lastMessageDate = message.dateSent
            dialog.unreadMessagesCount += 1
            self.updateOrInsertDialog(dialog)
            if(delegate != nil){
                self.delegate?.chatDidReceiveMessage(message)
            }else{
                JDStatusBarNotification.show(withStatus: "New Message Received", dismissAfter: 3.5, styleName: "newMessageStyle")
            }
        }else{
            QBRequest.dialogs(for: nil, extendedRequest: ["_id":message.dialogID!], successBlock: { (response, dialogs, dialogUserIDs, page) -> Void in
                if(dialogs != nil && dialogs!.count > 0){
                    /*if(self.dialogs[dialogs![0].ID!] == nil){
                        let recipient = dialogs![0].occupantIDs!.filter {$0 != QBSession.currentSession().currentUser!.ID}
                        print(recipient)
                        //GET USER PROFILE
                    }*/
                    self.updateOrInsertDialog(dialogs![0])
                }
                }, errorBlock: { (response) -> Void in
                    print("Error getting dialog: \(String(describing: response.error))")
            })
        }
        
    }
    
    func shouldShowUnreadIndicator()->Bool{
        let lastViewedDate = UserDefaults.standard.object(forKey: "DialogsLastViewedDate") as? Date ?? Date.distantPast
        for dialog in dialogs{
            if((dialog.unreadMessagesCount > 0 && dialog.lastMessageUserID != QBSession.current().currentUser!.id) || (dialog.lastMessageDate == nil && dialog.createdAt?.timeIntervalSince1970 > lastViewedDate.timeIntervalSince1970)){
                return true
            }
        }
        return false
    }
    
    func chatDidConnect() {
        print("Chat did connect")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "com.samuelharrison.Sparker.chatDidReconnect"), object: nil)
    }
    
    func chatDidReconnect() {
        print("Chat did reconnect")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "com.samuelharrison.Sparker.chatDidReconnect"), object: nil)
    }
    
    func chatDidNotConnectWithError(_ error: Error?) {
        print("Chat did not connect: \(String(describing: error))")
        /*if(error?.){
            QBRequest.users(successBlock: { (response, page, users) in
                print("Session token after QBRequest: \(QBSession.current().sessionDetails?.token)")
                if let user = QBSession.current().currentUser{
                    user.password = QBSession.current().sessionDetails!.token!
                    QBChat.instance().connect(with: user, completion: nil)
                }else{
                    self.appDelegate.swipeViewController.loginExistingUser()
                }
                }, errorBlock: { (response) in
                    print("Error in session refresh: \(response.error)")
                    self.appDelegate.swipeViewController.loginExistingUser()
            })
        }else{*/
            Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.reconnectToChat), userInfo: nil, repeats: false)
        //}
    }
    
    @objc func reconnectToChat(){
        QBChat.instance().connect(with: QBSession.current().currentUser!, completion: nil)
    }
    
    func chatDidAccidentallyDisconnect() {
        print("Chat accidentally disconnected")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "com.samuelharrison.Sparker.chatDidDisconnect"), object: nil)
    }
    
    /*func chatDidReceiveContactItemActivity(userID: UInt, isOnline: Bool, status: String?) {
        if(self.delegate != nil){
            self.delegate?.chatDidReceivePresence(userID, isOnline: isOnline)
        }
    }*/
    
}
