//
//  LoginViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 04/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import MBProgressHUD
import TTTAttributedLabel

protocol LoginViewControllerDelegate{
    func loginViewControllerDidLogUserIn(_ loginViewController:LoginViewController)
    func registerForRemoteNotifications()
}

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate, TTTAttributedLabelDelegate{

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var delegate:LoginViewControllerDelegate
    var loginButton = FBSDKLoginButton()
    
    init(delegate:LoginViewControllerDelegate){
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = AppConfig.loginScreenBackgroundColor
        
        let logoView = UIImageView(frame: CGRect(x: self.view.bounds.size.width/4, y: self.view.bounds.size.height/2 - self.view.bounds.size.width/4 - 88, width: self.view.bounds.size.width/2, height: self.view.bounds.size.width/2))
        var image = UIImage(named: AppConfig.loginScreenLogoImage)
        if(AppConfig.loginScreenLogoOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        logoView.image = image
        logoView.tintColor = AppConfig.loginScreenLogoColor
        self.view.addSubview(logoView)
        
        let logoTextLarge = UIImageView(frame: CGRect(x: self.view.bounds.size.width/4, y: self.view.bounds.size.height/2 + 30, width: self.view.bounds.size.width/2, height: self.view.bounds.size.width/4))
        image = UIImage(named: AppConfig.loginScreenTextImage)
        if(AppConfig.loginScreenTextOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        logoTextLarge.image = image
        logoTextLarge.tintColor = AppConfig.loginScreenTextColor
        logoTextLarge.contentMode = UIViewContentMode.scaleAspectFill
        self.view.addSubview(logoTextLarge)
        
        let privacyTermsLabel = TTTAttributedLabel(frame: CGRect(x: 32, y: self.view.bounds.size.height/2 + self.view.bounds.size.width/2 - 60, width: self.view.bounds.size.width - 64, height: 44))
        privacyTermsLabel.numberOfLines = 0
        privacyTermsLabel.font = UIFont.boldSystemFont(ofSize: 10.0)
        privacyTermsLabel.textAlignment = NSTextAlignment.center
        privacyTermsLabel.text = "By continuing, you agree to our Terms of Use and Privacy Policy"
        privacyTermsLabel.linkAttributes = [
            NSAttributedStringKey.foregroundColor: UIColor.gray,
            NSAttributedStringKey.underlineStyle: NSNumber(value: true as Bool),
        ]
        privacyTermsLabel.activeLinkAttributes = [
            NSAttributedStringKey.foregroundColor: UIColor.gray.withAlphaComponent(0.80),
            NSAttributedStringKey.underlineStyle: NSNumber(value: true as Bool),
        ]
        
        let termsRange = (privacyTermsLabel.text! as NSString).range(of: "Terms of Use")
        let privacyRange = (privacyTermsLabel.text! as NSString).range(of: "Privacy Policy")
        privacyTermsLabel.addLink(to: URL(string: "sparkeraction://show-terms"), with: termsRange)
        privacyTermsLabel.addLink(to: URL(string: "sparkeraction://show-privacy"), with: privacyRange)
        privacyTermsLabel.delegate = self
        self.view.addSubview(privacyTermsLabel)
        
        self.loginButton.frame = CGRect(x: (self.view.bounds.size.width - 200)/2, y: self.view.bounds.size.height/2 + self.view.bounds.size.width/2, width: 200, height: 44)
        self.loginButton.delegate = self
        self.loginButton.readPermissions = ["public_profile","user_friends","user_birthday"]
        self.view.addSubview(self.loginButton)
    }
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        if(url.host == nil){
            return
        }
        if(url.host!.hasPrefix("show-terms")){
            if let htmlFile = Bundle.main.path(forResource: "termsOfUse", ofType: "html"){
                do{
                    let htmlString = try String(contentsOfFile: htmlFile)
                    let webViewVC = WebViewViewController(html: htmlString)
                    webViewVC.navigationItem.title = "Terms of Use"
                    let navController = UINavigationController(rootViewController: webViewVC)
                    self.present(navController, animated: true, completion: nil)
                }catch{
                    
                }
            }
        }else if(url.host!.hasPrefix("show-privacy")){
            if let htmlFile = Bundle.main.path(forResource: "privacyPolicy", ofType: "html"){
                do{
                    let htmlString = try String(contentsOfFile: htmlFile)
                    let webViewVC = WebViewViewController(html: htmlString)
                    webViewVC.navigationItem.title = "Privacy Policy"
                    let navController = UINavigationController(rootViewController: webViewVC)
                    self.present(navController, animated: true, completion: nil)
                }catch{
                    
                }
            }
        }
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!){
        if(error == nil){
            self.handleFacebookSession()
        }else{
            self.handleLoginError(error as NSError?)
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        //
    }
    
    func handleFacebookSession(){
        /*if(QBSession.currentSession().currentUser != nil){
            self.delegate.loginViewControllerDidLogUserIn(self)
            return
        }*/
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.loginButton.alpha = 0
        }) 
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if(FBSDKAccessToken.current() == nil){
            print("Login Failed.  FB Access token or user ID does not exist")
            self.cancelLogin(nil)
            return
        }
        
        QBRequest.logIn(withSocialProvider: "facebook", accessToken: FBSDKAccessToken.current().tokenString, accessTokenSecret: nil, successBlock: { (response, user) -> Void in
            user?.password = QBSession.current().sessionDetails!.token
            QBChat.instance().connect(with: user!, completion: { (error) -> Void in
                if(error == nil){
                    self.delegate.registerForRemoteNotifications()
                    let params = NSMutableDictionary()
                    params.setObject("\(user!.id)", forKey: "user_id" as NSCopying)
                    QBRequest.objects(withClassName: "profile", extendedRequest: params, successBlock: { (response, object, page) -> Void in
                        print(object as Any)
                        if(object != nil && object!.count > 0){
                            Utility.sharedInstance.currentUserProfile = UserProfile(object: object![0])
                            self.delegate.loginViewControllerDidLogUserIn(self)
                        }else{
                            self.appDelegate.firstLaunch = true
                            let object = QBCOCustomObject()
                            object.className = "profile"
                            QBRequest.createObject(object, successBlock: { (response, object) -> Void in
                                if(object == nil){
                                    self.cancelLogin(nil)
                                    return
                                }
                                object!.className = "profile"
                                Utility.sharedInstance.currentUserProfile = UserProfile(object: object!)
                                user!.customData = object!.id
                                DispatchQueue.main.async(execute: { () -> Void in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                })
                                self.delegate.loginViewControllerDidLogUserIn(self)
                                
                                }, errorBlock: { (response) -> Void in
                                    print("Error creating profile, logging out")
                                    self.cancelLogin(nil)
                                    return
                            })
                        }
                        }, errorBlock: { (response) -> Void in
                            print("Error loading profile: \(String(describing: response.error))")
                            self.cancelLogin(nil)
                            return
                    })
                }else{
                    print("Error connecting to chat: \(String(describing: error))")
                    self.cancelLogin(nil)
                    return
                }
            })
        }) { (response) -> Void in
            print("Error logging in: \(String(describing: response.error))")
            self.cancelLogin(nil)
            return
        }
    }
    
    func cancelLogin(_ error:NSError?){
        DispatchQueue.main.async(execute: { () -> Void in
            MBProgressHUD.hide(for: self.view, animated: true)
        })
        if(error != nil){
            self.handleLoginError(error)
        }
        FBSDKAccessToken.setCurrent(nil)
        QBChat.instance().disconnect(completionBlock: nil)
        QBRequest.logOut(successBlock: nil, errorBlock: nil)
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.loginButton.alpha = 1.0
        }) 
    }
    
    func handleLoginError(_ error:NSError?){
        if(error != nil){
            print("Error: \(String(describing: error?.userInfo["com.facebook.sdk:ErrorLoginFailedReason"]))")
        }
    }
}
