//
//  AppDelegate.swift
//  Sparker
//
//  Created by Samuel Harrison on 01/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SWRevealViewController
import JDStatusBarNotification
import StoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    
    var firstLaunch = false
    var window: UIWindow?
    var masterViewContainer:SWRevealViewController!
    var menuViewController:MenuViewController!
    var navController:UINavigationController!
    var swipeViewController:SwipeViewController!
    var dialogsViewController:DialogsViewController!
    var upgradeViewController: UpgradeViewController!
    var userSettings: [String:Int]!{
        didSet{
            let userDefaults = UserDefaults.standard
            userDefaults.set(self.userSettings, forKey: "userSettings")
        }
    }
    var userLocations = [UserLocation](){
        didSet{
            let userDefaults = UserDefaults.standard
            userDefaults.set(NSKeyedArchiver.archivedData(withRootObject: self.userLocations), forKey: "userLocations")
        }
    }
    var notificationMatches = [UInt]()
    var subscriptionExpirationTimestamp = Date.distantPast.timeIntervalSince1970
    var sessionExpiration: Date?
    var user : QBUUser?
    var token: String?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        QBSettings.setApplicationID(AppConfig.quickBloxApplicationID)
        QBSettings.setAuthKey(AppConfig.quickBloxAuthKey)
        QBSettings.setAuthSecret(AppConfig.quickBloxAuthSecret)
        QBSettings.setAccountKey(AppConfig.quickBloxAccountKey)
        QBSettings.setNetworkIndicatorManagerEnabled(true)
        QBSettings.setAutoReconnectEnabled(true)
        QBSettings.setStreamResumptionEnabled(true)
        QBSettings.setStreamManagementSendMessageTimeout(10)
        
        QBSettings.setLogLevel(QBLogLevel.errors)

        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        
        let userDefaults = UserDefaults.standard
        if(userDefaults.object(forKey: "userSettings") as? [String:Int] != nil){
            userSettings = userDefaults.object(forKey: "userSettings") as! [String:Int]
        }else{
            userSettings = ["agemin":18,"agemax":60,"distance":5000,"activeLocation":0]
        }
        
        if let data = userDefaults.object(forKey: "userLocations") as? Data{
            if let locations = NSKeyedUnarchiver.unarchiveObject(with: data) as? [UserLocation]{
                self.userLocations = locations
            }
        }
        
        self.upgradeViewController = UpgradeViewController()
        self.upgradeViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.upgradeViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.dialogsViewController = DialogsViewController()
        self.menuViewController = MenuViewController()
        self.swipeViewController = SwipeViewController()
        self.navController = UINavigationController(rootViewController: self.swipeViewController)
        
        self.masterViewContainer = SWRevealViewController(rearViewController: self.menuViewController, frontViewController: self.navController)
        //self.masterViewContainer.setRightViewController(self.dialogsViewController, animated: false)

        self.masterViewContainer.rearViewRevealWidth = 0.8*UIScreen.main.bounds.size.width
        self.masterViewContainer.rightViewRevealWidth = 0.8*UIScreen.main.bounds.size.width
        
        self.setupAppearance()
        
        self.window?.rootViewController = self.masterViewContainer
        self.window?.makeKeyAndVisible()
        
        SKPaymentQueue.default().add(self.upgradeViewController)
        self.validateReceipt(Bundle.main.appStoreReceiptURL) { (validated) -> Void in
            //
        }
        
        return true
    }
    
    func setupAppearance(){
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = AppConfig.navigationBarButtonsColor
        UINavigationBar.appearance().barTintColor = AppConfig.navigationBarBackgroundColor
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        
        JDStatusBarNotification.addStyleNamed("newMessageStyle") { (style) -> JDStatusBarStyle? in
            style?.barColor = AppConfig.newMessageNotificationBackgroundColor
            style?.textColor = AppConfig.newMessageNotificationTextColor
            style?.font = AppConfig.newMessageNotificationFont
            style?.animationType = JDStatusBarAnimationType.bounce
            return style
        }
        
        JDStatusBarNotification.addStyleNamed("newMatchStyle") { (style) -> JDStatusBarStyle? in
            style?.barColor = AppConfig.newMatchNotificationBackgroundColor
            style?.textColor = AppConfig.newMatchNotificationTextColor
            style?.font = AppConfig.newMatchNotificationFont
            style?.animationType = JDStatusBarAnimationType.bounce
            return style
        }
    }
    
    func presentLoginViewController(_ animated:Bool){
        self.swipeViewController.presentLoginViewController(animated)
    }
    
    func logOut(){
        self.userSettings = ["agemin":18,"agemax":60,"distance":50,"activeLocation":0]
        self.userLocations = [UserLocation]()
        UserDefaults.standard.removeObject(forKey: "savedUserProfile")
        let destinationPath = (NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profilePicture")
        do{
            try FileManager.default.removeItem(atPath: destinationPath)
        }catch{
            
        }
        if let deviceUDID = UIDevice.current.identifierForVendor?.uuidString{
            QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceUDID, successBlock: nil, errorBlock: nil)
        }
        self.swipeViewController.findMoreUsersTimer.invalidate()
        ChatService.sharedInstance.dialogs.removeAll()
        ChatService.sharedInstance.messages.removeAll()
        ChatService.sharedInstance.sortedDialogs.removeAll()
        QBChat.instance().disconnect(completionBlock: nil)
        QBRequest.logOut(successBlock: nil, errorBlock: nil)
        QBRequest.destroySession(successBlock: nil, errorBlock: nil)
        FBSDKAccessToken.setCurrent(nil)
        FBSDKLoginManager().logOut()
        self.navController.popToRootViewController(animated: false)
        self.presentLoginViewController(true)
        self.dialogsViewController = DialogsViewController()
        self.menuViewController = MenuViewController()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        self.user = QBSession.current().currentUser
        if(QBSession.current().sessionDetails != nil){
            self.token = QBSession.current().sessionDetails!.token
        }
        self.sessionExpiration = QBSession.current().sessionExpirationDate
        print("did enter background user == nil? \(user == nil), userId = \(String(describing: user?.externalUserID)), expire = \(String(describing: sessionExpiration))")
        QBChat.instance().disconnect(completionBlock: nil)

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        if(QBSession.current().currentUser != nil){
            QBChat.instance().connect(with: QBSession.current().currentUser!, completion: nil)
        }
        
        if let user = user {
            //user.password = token
            self.swipeViewController.registerForRemoteNotifications()
            if(sessionExpiration != nil && sessionExpiration!.timeIntervalSince1970 < Date().timeIntervalSince1970){
                print("Session token before QBRequest: \(String(describing: QBSession.current().sessionDetails?.token))")
                QBRequest.users(successBlock: { (response, page, users) in
                    print("Session token after QBRequest: \(String(describing: QBSession.current().sessionDetails?.token))")
                    user.password = QBSession.current().sessionDetails!.token!
                    QBChat.instance().connect(with: user, completion: nil)
                    }, errorBlock: { (response) in
                        print("Error in session refresh: \(String(describing: response.error))")
                        self.swipeViewController.loginExistingUser()
                })
            }else{
                QBChat.instance().connect(with: user, completion: nil)
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        QBChat.instance().disconnect(completionBlock: nil)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceIdentifier = UIDevice.current.identifierForVendor!.uuidString
        let subscription = QBMSubscription()
        
        subscription.notificationChannel = QBMNotificationChannelAPNS
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = deviceToken
        QBRequest.createSubscription(subscription, successBlock: { (response, objects) -> Void in
            //
            }) { (response) -> Void in
                //
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for push notifications with error: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("Received Push Notification: \(userInfo)")
    }
    
    func receiptData(_ appStoreReceiptURL : URL?) -> Data? {
        guard let receiptURL = appStoreReceiptURL,
            let receipt = try? Data(contentsOf: receiptURL) else {
                return nil
        }
        
        do {
            let receiptData = receipt.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let requestContents = ["receipt-data" : receiptData, "password" : AppConfig.IAPSharedSecret]
            let requestData = try JSONSerialization.data(withJSONObject: requestContents, options: [])
            return requestData
        }
        catch let error as NSError {
            print(error)
        }
        
        return nil
    }
    
    func validateReceiptInternal(_ appStoreReceiptURL : URL?, isProd: Bool , onCompletion: @escaping (Int?) -> Void) {
        
        let serverURL = isProd ? "https://buy.itunes.apple.com/verifyReceipt" : "https://sandbox.itunes.apple.com/verifyReceipt"
        
        guard let receiptData = receiptData(appStoreReceiptURL),
            let url = URL(string: serverURL)  else {
                onCompletion(nil)
                return
        }
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = receiptData
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error)  in
            
            guard let data = data, error == nil else {
                onCompletion(nil)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options:[]) as? NSDictionary
                guard let statusCode = json?["status"] as? Int else {
                    onCompletion(nil)
                    return
                }
                if let receiptInfo: NSArray = json?["latest_receipt_info"] as? NSArray{
                    if let lastReceipt = receiptInfo.lastObject as? NSDictionary{
                        if let expirationTimestamp = lastReceipt["expires_date_ms"] as? String{
                            if let timestamp = Double(expirationTimestamp){
                                self.subscriptionExpirationTimestamp = timestamp/1000
                            }else{
                                self.subscriptionExpirationTimestamp = Date.distantPast.timeIntervalSince1970
                            }
                            if(self.subscriptionExpirationTimestamp < Date().timeIntervalSince1970){
                                self.userSettings["activeLocation"] = 0
                                self.swipeViewController.resetCards()
                            }
                        }
                    }
                }
                onCompletion(statusCode)
            }
            catch let error as NSError {
                print(error)
                onCompletion(nil)
            }
        })
        task.resume()
    }
    
    func validateReceipt(_ appStoreReceiptURL : URL?, onCompletion: @escaping (Bool) -> Void) {
        
        validateReceiptInternal(appStoreReceiptURL, isProd: true) { (statusCode: Int?) -> Void in
            guard let status = statusCode else {
                onCompletion(false)
                return
            }
            // This receipt is from the test environment, but it was sent to the production environment for verification.
            if status == 21007 {
                self.validateReceiptInternal(appStoreReceiptURL, isProd: false) { (statusCode: Int?) -> Void in
                    guard let statusValue = statusCode else {
                        onCompletion(false)
                        return
                    }
                    // 0 if the receipt is valid
                    if statusValue == 0 {
                        onCompletion(true)
                    } else {
                        onCompletion(false)
                    }
                    
                }
                // 0 if the receipt is valid
            } else if status == 0 {
                onCompletion(true)
            } else {
                onCompletion(false)
            }
        }
    }

}
