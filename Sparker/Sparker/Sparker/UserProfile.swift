//
//  UserProfile.swift
//  Sparker
//
//  Created by Samuel Harrison on 04/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class UserProfile: NSObject, NSCoding{
    
    var object: QBCOCustomObject
    var userID: UInt
    var name: String! {
        didSet{
            self.object.fields!["name"] = self.name
        }
    }
    var bio: String!{
        didSet{
            self.object.fields!["bio"] = self.bio
        }
    }
    var location: CLLocation! {
        didSet{
            self.object.fields!["location"] = [self.location.coordinate.longitude,self.location.coordinate.latitude]
        }
    }
    var images = [UInt](){
        didSet{
            self.object.fields!["images"] = self.images
        }
    }
    var age: Int! = 18 {
        didSet{
            self.object.fields!["age"] = self.age
        }
    }
    var matched: [UInt] {
        didSet{
            self.object.fields!["matched"] = self.matched
        }
    }
    var swiped: [UInt] {
        didSet{
            self.object.fields!["swiped"] = self.swiped
        }
    }
    var gender: Int = -1 {
        didSet{
            self.object.fields!["gender"] = self.gender
        }
    }
    var interestedIn: Int = -1 {
        didSet{
            self.object.fields!["interested_in"] = self.interestedIn
        }
    }
    var liked: Int = 0{
        didSet{
            if(self.liked >= AppConfig.likeLimit){
                self.likeLimitLifted = Date(timeInterval: AppConfig.likeLimitTime, since: Date())
                self.liked = 0
                self.object.fields!["liked"] = self.liked
                Utility.sharedInstance.uploadCurrentUser(nil, errorBlock: nil)
            }else{
                self.object.fields!["liked"] = self.liked
                Utility.sharedInstance.uploadCurrentUser(nil, errorBlock: nil)
            }
        }
    }
    var likeLimitLifted: Date {
        didSet{
            self.object.fields!["likeLimitLifted"] = self.likeLimitLifted
        }
    }
    var updatedAt: Double
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    init(object:QBCOCustomObject){
        self.object = object
        self.userID = object.userID
        self.name = object.fields!["name"] as? String ?? ""
        self.bio = object.fields!["bio"] as? String ?? ""
        if let location = object.fields!["location"] as? [Double]{
            self.location = CLLocation(latitude: location[1], longitude: location[0])
        }else{
            self.location = CLLocation(latitude: 51.5072, longitude: 0.1275)
        }
       
        if let images = object.fields!["images"] as? [UInt]{
            for image in images{
                self.images.append(image)
            }
        }
        self.age = (object.fields!["age"] as? Int) ?? 18
        self.matched = object.fields!["matched"] as? [UInt] ?? [UInt]()
        self.swiped = object.fields!["swiped"] as? [UInt] ?? [UInt]()
        self.liked = object.fields!["liked"] as? Int ?? 0
        self.likeLimitLifted = Date.distantPast
        if let dateString = object.fields!["likeLimitLifted"] as? String{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            if let date = dateFormatter.date(from: dateString){
                self.likeLimitLifted = date
            }else{
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                if let date = dateFormatter.date(from: dateString){
                    self.likeLimitLifted = date
                }
            }
        }
        self.updatedAt = object.updatedAt?.timeIntervalSince1970 ?? 0
        super.init()
        if let gender = object.fields!["gender"] as? Int{
            self.gender = gender
        }else{
            self.gender = 0
            self.object.fields!["gender"] = 0
        }
        if let interestedIn = object.fields!["interested_in"] as? Int{
            self.interestedIn = interestedIn
        }else{
            self.interestedIn = 1
            self.object.fields!["interested_in"] = 1
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.userID, forKey: "userID")
        aCoder.encode(self.bio, forKey: "bio")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.images, forKey: "images")
        aCoder.encode(self.age, forKey: "age")
        aCoder.encode(self.matched, forKey: "matched")
        aCoder.encode(self.swiped, forKey: "swiped")
        aCoder.encode(self.updatedAt, forKey: "updatedAt")
        aCoder.encode(self.gender, forKey: "gender")
        aCoder.encode(self.interestedIn, forKey: "interestedIn")
        aCoder.encode(self.liked, forKey: "liked")
        aCoder.encode(self.likeLimitLifted, forKey: "likeLimitLifted")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.object = QBCOCustomObject()
        self.object.className = "profile"
        self.userID = aDecoder.decodeObject(forKey: "userID") as? UInt ?? 0
        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.bio = aDecoder.decodeObject(forKey: "bio") as? String ?? ""
        self.location = CLLocation(latitude: 51.5072, longitude: 0.1275)
        self.images = aDecoder.decodeObject(forKey: "images") as? [UInt] ?? [UInt]()
        self.age = aDecoder.decodeObject(forKey: "age") as? Int ?? 18
        self.matched = aDecoder.decodeObject(forKey: "matched") as? [UInt] ?? [UInt]()
        self.swiped = aDecoder.decodeObject(forKey: "swiped") as? [UInt] ?? [UInt]()
        self.updatedAt = aDecoder.decodeObject(forKey: "updatedAt") as? TimeInterval ?? 0
        self.gender = aDecoder.decodeObject(forKey: "gender") as? Int ?? 0
        self.interestedIn = aDecoder.decodeObject(forKey: "interestedIn") as? Int ?? 1
        self.liked = aDecoder.decodeObject(forKey: "liked") as? Int ?? 0
        self.likeLimitLifted = aDecoder.decodeObject(forKey: "likeLimitLifted") as? Date ?? Date.distantPast
    }
    
    func getProfilePictureURL()->URL?{
        if(self.images.count > 0){
            if let urlString = QBCBlob.privateUrl(forID: self.images[0]){
                return URL(string: urlString)
            }
        }
        return nil
    }
    
}
