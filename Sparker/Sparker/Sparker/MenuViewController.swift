//
//  MenuViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 10/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    let profileButton = MenuButton()
    let homeButton = MenuButton()
    let messagesButton = MenuButton()
    let settingsButton = MenuButton()
    let inviteButton = MenuButton()
    //let logoutButton = UIButton()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = AppConfig.sideMenuBackgroundColor
        
        profileButton.frame = CGRect(x: 24, y: 64, width: 0.8*self.view.bounds.size.width - 32, height: 66.0)
        profileButton.setTitle("Profile", for: UIControlState())
        profileButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        var image = UIImage(contentsOfFile: (NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profilePicture")) ?? UIImage(named: "PlaceholderImage")
        profileButton.customImageView.image = image
        profileButton.customImageView.layer.borderColor = AppConfig.sideMenuProfileImageBorderColor.cgColor
        profileButton.customImageView.layer.borderWidth = AppConfig.sideMenuProfileImageBorderWidth
        profileButton.addTarget(self, action: #selector(MenuViewController.didTapProfileButton), for: UIControlEvents.touchUpInside)
        self.view.addSubview(profileButton)
       
        homeButton.frame = CGRect(x: 24, y: 64 + 66 + 16, width: 0.8*self.view.bounds.size.width - 32, height: 66.0)
        homeButton.setTitle("Home", for: UIControlState())
        homeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        image = UIImage(named: AppConfig.sideMenuHomeImage)
        if(AppConfig.sideMenuHomeImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        homeButton.customImageView.image = image
        homeButton.customImageView.tintColor = AppConfig.sideMenuHomeImageColor
        homeButton.isSelected = true
        homeButton.addTarget(self, action: #selector(MenuViewController.didTapHomeButton), for: UIControlEvents.touchUpInside)
        self.view.addSubview(homeButton)
        
        messagesButton.frame = CGRect(x: 24, y: 64 + 2*(66 + 16), width: 0.8*self.view.bounds.size.width - 32, height: 66.0)
        messagesButton.setTitle("Messages", for: UIControlState())
        messagesButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        image = UIImage(named: AppConfig.sideMenuMessagesImage)
        if(AppConfig.sideMenuMessagesImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        messagesButton.customImageView.image = image
        messagesButton.customImageView.tintColor = AppConfig.sideMenuMessagesImageColor
        messagesButton.addTarget(self, action: #selector(MenuViewController.didTapMessagesButton), for: UIControlEvents.touchUpInside)
        self.view.addSubview(messagesButton)
        
        settingsButton.frame = CGRect(x: 24, y: 64 + 3*(66 + 16), width: 0.8*self.view.bounds.size.width - 32, height: 66.0)
        settingsButton.setTitle("Settings", for: UIControlState())
        settingsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        image = UIImage(named: AppConfig.sideMenuSettingsImage)
        if(AppConfig.sideMenuSettingsImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        settingsButton.customImageView.image = image
        settingsButton.customImageView.tintColor = AppConfig.sideMenuSettingsImageColor
        settingsButton.addTarget(self, action: #selector(MenuViewController.didTapSettingsButton), for: UIControlEvents.touchUpInside)
        self.view.addSubview(settingsButton)
    }
    
    func profileImageUpdated(){
        let image = UIImage(contentsOfFile: (NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profilePicture")) ?? UIImage()
        profileButton.customImageView.image = image
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setButtonSelected(_ theButton:UIButton){
        for button in [self.profileButton, self.homeButton, self.messagesButton, self.settingsButton, self.inviteButton]{
            if(button == theButton){
                button.isSelected = true
            }else{
                button.isSelected = false
            }
        }
    }
    
    @objc func didTapHomeButton(){
        self.setButtonSelected(self.homeButton)
        self.appDelegate.navController.setViewControllers([self.appDelegate.swipeViewController], animated: true)
        self.appDelegate.masterViewContainer.revealToggle(nil)
    }
    
    @objc func didTapProfileButton(){
        self.setButtonSelected(self.profileButton)
        let profileViewController = ProfileViewController(profile: Utility.sharedInstance.currentUserProfile!, alreadyMatched: false)
        self.appDelegate.navController.setViewControllers([profileViewController], animated: true)
        self.appDelegate.masterViewContainer.revealToggle(nil)
    }
    
    @objc func didTapMessagesButton(){
        self.setButtonSelected(self.messagesButton)
        self.appDelegate.navController.setViewControllers([self.appDelegate.swipeViewController,self.appDelegate.dialogsViewController], animated: true)
        self.appDelegate.masterViewContainer.revealToggle(nil)
    }
    
    @objc func didTapSettingsButton(){
        self.setButtonSelected(self.settingsButton)
        let settingsViewController = SettingsViewController(style: UITableViewStyle.grouped)
        self.appDelegate.navController.setViewControllers([self.appDelegate.swipeViewController,settingsViewController], animated: true)
        self.appDelegate.masterViewContainer.revealToggle(nil)
    }
    
    func didTapInviteButton(){
        //
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
