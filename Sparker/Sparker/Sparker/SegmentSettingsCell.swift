//
//  SegmentSettingsCell.swift
//  Sparker
//
//  Created by Samuel Harrison on 08/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class SegmentSettingsCell: UITableViewCell {
    
    var segmentControl:UISegmentedControl!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.segmentControl = UISegmentedControl(frame: CGRect(x: 16, y: (self.bounds.size.height - 44.0)/2, width: self.bounds.size.width - 32, height: 44.0))
        self.segmentControl.setTitleTextAttributes([NSAttributedStringKey.font:UIFont.systemFont(ofSize: 20.0)], for: UIControlState())
        self.contentView.addSubview(self.segmentControl)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.segmentControl.frame = CGRect(x: 16, y: (self.bounds.size.height - 44.0)/2, width: self.bounds.size.width - 32, height: 44.0)
    }

}
