//
//  config.swift
//  Sparker
//
//  Created by Samuel Harrison on 15/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

class AppConfig{
    
    
    /****************************************
     ************** QuickBlox ***************
     ****************************************/
    
    /*
     *
     * Set your Application ID, Authorization Key, Authorization Secret and Account Key.
     *
     * After you have signed up for QuickBlox at http://quickblox.com/signup/ and created a
     * new app, you have your Application ID, Authorization Key and Authorization Secret.
     *
     * To find your Account Key, navigate to your Quickblox Account Settings page 
     * https://admin.quickblox.com/account_settings
     *
     */

    static let quickBloxApplicationID:UInt  = 69633                     // Quickblox Application ID
    static let quickBloxAuthKey             = "KUdWfCVlAxRyVyNZCH0V"         // Quickblox Authorization Key
    static let quickBloxAuthSecret          = "FmRlShR4VBRMEwR0WkgH"         // Quickblox Authorization Secret
    
    static let quickBloxAccountKey          = "HEdvfiwOTHcEHQZsYFAffhYBfjQ"    // Quickblox Account Key
    
    /****************************************
    ************ In App Purchase ************
    ****************************************/
    
    /*
     *
     * Sparker uses 1 auto-renewable In App Purchase subscription
     *
     * Log in to your iTunesConnect account at http://itunesconnect.apple.com
     * and create your app if you haven't already done so, then 
     * create 1 Automatically Renewable Subscriptions
     *
     * You can name and price this any way you want to.  Provide the Product ID of the
     * IAP you have created below.  Provide the 'Shared Secret' below.
     *
     */

    static let IAPProductID         = "REPLACE_WITH_IAP_PRODUCT_ID"
    
    static let IAPSharedSecret      = "REPLACE_WITH_IAP_SHARED_SECRET"
    
    
    /****************************************
     ************ Upgrade Screen ************
     ****************************************/
    
    static let featureTitles        = ["Rewind your last swipe","Change your location","Unlimited likes"]
    
    static let featureSubtitles     = ["Go back and swipe again","Match with people anywhere","Have fun swiping"]
    
    static let featureImages        = ["IconRewind","IconPlane","IconHeart"]
    
    static let featureColors        = [UIColor.orange, blueColor, mainColor]
    
    
    /****************************************
     ************ Limiting Likes ************
     ****************************************/
    
    /*
     *
     * Sparker allows you to easily set the number of likes a free user is allowed
     * and the amount of time they must wait for this to be reset.
     *
     * To enable the limit make sure shouldLimitLikes is set to true, setting this to false
     * will allow free users unlimited likes
     *
     * Set likeLimit to the number of likes you will allow a free user before they are limited
     *
     * Set likeLimitTime to the amount of time (in seconds) a free user must wait once they have
     * reached the like limit. E.g. 24*60*60 would be a 24 hour wait
     *
     */
    
    static let shouldLimitLikes         =   true    //Set to false to allow free users unlimited likes
    static let likeLimit                =   100     //Number of likes a user can make before they must upgrade or wait
    static let likeLimitTime:Double     =   24*60*60//Time user has to wait before likes are refreshed (in seconds)
    
    
    /****************************************
    *********** Rewind & Passport ***********
    ****************************************/
    
    /*
     *
     * Rewind lets users bring back the previous user they 'swiped left' on.
     *
     * To disable rewind feature set shouldEnableRewind to false
     * To allow free users to rewind set rewindIsPlusOnly to false
     *
     * Passport lets users set a custom location to see users from
     *
     * To disable passport set shouldEnablePassport to false
     * To allow free users to user passport set passportIsPlusOnly to false
     *
     */
    
    static let shouldEnableRewind      =   true        //Set to false to completely disable the 'rewind' feature
    static let rewindIsPlusOnly        =   false        //Set to false to allow free users to rewind
    
    static let shouldEnablePassport    =   true        //Set to false to completely disable 'location change' feature
    static let passportIsPlusOnly      =   false        //Set to false to allow free users to change location
    
    
    /****************************************
    ************ Other Settings *************
    ****************************************/
    
    static let maximumSearchRadius:Float     =   10000         //The maximum search radius a user can set in their settings (in km)
    
    static let enableCredits                 =   true        //Keep this enabled if you have used any of the icons included.
    
    
    /****************************************
    ************** Appearance ***************
    ****************************************/
    
    /*
     *
     * You can easily change the entire appearance of Sparker by changing the attributes below
     *
     * All Fonts should be set to a UIFont
     *
     * All Colors should be set to a UIColor
     *
     */

    /****************************************
    *********** Master Attributes ***********
    ****************************************/
    
    /*
     *
     * Changing the master attributes will make sweeping changes to the appearance of the app
     * by changing the main colour schemes
     *
     */
    
    static let mainColor = UIColor(red: 142.0/255.0, green: 68.0/255.0, blue: 173.0/255.0, alpha: 1.0)
    static let lightGray = UIColor(white: 0.8, alpha: 1.0)
    static let lightestGray = UIColor(white: 0.9, alpha: 1.0)
    static let whiteColor = UIColor.white
    static let blueColor = UIColor(red: 52.0/255.0, green: 152.0/255.0, blue: 219.0/255.0, alpha: 1.0)
    
    static let navigationBarBackgroundColor = whiteColor
    static let navigationBarButtonsColor = lightGray
    static let navigationBarTitleColor = UIColor.black
    static let navigationBarTitleImageColor = mainColor
    
    static let screenBackgroundColor = whiteColor
    
    
    /*
    *
    * If you want finer control over the appearance, attributes for the appearance of every screen
    * can be changed below.
    *
    */
    
    /****************************************
    ********* In-App Notifications **********
    ****************************************/
    
    
    static let newMessageNotificationBackgroundColor = mainColor
    static let newMessageNotificationTextColor = whiteColor
    static let newMessageNotificationFont = UIFont.systemFont(ofSize: 14.0)
    
    static let newMatchNotificationBackgroundColor = whiteColor
    static let newMatchNotificationTextColor = mainColor
    static let newMatchNotificationFont = UIFont.systemFont(ofSize: 14.0)
    
    
    /****************************************
     ************* Login Screen *************
     ****************************************/
    
    
    static let loginScreenBackgroundColor = screenBackgroundColor
    
    static let loginScreenLogoImage = "logoLarge"
    static let loginScreenLogoOverrideColor = true
    static let loginScreenLogoColor = mainColor
    
    static let loginScreenTextImage = "logoTextLarge"
    static let loginScreenTextOverrideColor = true
    static let loginScreenTextColor = mainColor
    
    
    /****************************************
     ************* Main Screen **************
     ****************************************/
    
    
    static let mainScreenNavigationBarBackgroundColor = navigationBarBackgroundColor
    static let mainScreenNavigationBarButtonsColor = navigationBarButtonsColor
    static let mainScreenNavigationBarMenuButtonImage = "IconMenu"
    
    static let mainScreenNavigationBarChatsButtonImage = "IconChats"
    static let mainScreenNavigationBarChatsUnreadIndicatorColor = mainColor
    
    static let mainScreenNavigationBarTitleImage = "LogoNavigationBar"
    static let mainScreenNavigationBarTitleImageOverrideColor = true
    static let mainScreenNavigationBarTitleImageColor = navigationBarTitleImageColor
    
    static let mainScreenBackgroundColor = screenBackgroundColor
    
    static let rewindButtonBackgroundColor = screenBackgroundColor
    static let rewindButtonBorderColor = UIColor(white: 0.95, alpha: 1.0)
    static let rewindButtonImage = "IconRewind"
    static let rewindButtonImageOverrideColor = true
    static let rewindButtonImageColor = UIColor.orange
    
    static let dislikeButtonBackgroundColor = screenBackgroundColor
    static let dislikeButtonBorderColor = UIColor(white: 0.95, alpha: 1.0)
    static let dislikeButtonImage = "IconCross"
    static let dislikeButtonImageOverrideColor = true
    static let dislikeButtonImageColor = lightGray
    
    static let likeButtonBackgroundColor = screenBackgroundColor
    static let likeButtonBorderColor = UIColor(white: 0.95, alpha: 1.0)
    static let likeButtonImage = "IconHeart"
    static let likeButtonImageOverrideColor = true
    static let likeButtonImageColor = mainColor
    
    static let locationButtonBackgroundColor = screenBackgroundColor
    static let locationButtonBorderColor = UIColor(white: 0.95, alpha: 1.0)
    static let locationButtonCurrentLocationImage = "IconLocation"
    static let locationButtonAwayLocationImage = "IconPlane"
    static let locationButtonImageOverrideColor = true
    static let locationButtonImageColor = blueColor
    
    static let currentUserProfileImageBorderColor = UIColor(white: 0.95, alpha: 1.0)
    static let currentUserProfileImageBorderWidth:CGFloat = 12.0
    static let radarColor = mainColor
    
    
    /****************************************
     ***************** Cards ****************
     ****************************************/
    
    
    static let cardBackgroundColor = screenBackgroundColor
    static let cardBorderColor = screenBackgroundColor
    static let cardBorderWidth:CGFloat = 0.0
    static let cardCornerRadius:CGFloat = 8.0
    static let cardTextColor = UIColor.darkGray
    static let cardFont = UIFont.boldSystemFont(ofSize: 20.0)
    
    static let cardDislikeOverlayImage = "IconCross"
    static let cardDislikeOverlayImageOverrideColor = true
    static let cardDislikeOverlayImageColor = UIColor(white: 0.7, alpha: 1.0)
    static let cardDislikeOverlayBorderColor = UIColor(white: 0.7, alpha: 1.0)
    static let cardDislikeOverlayBorderWidth:CGFloat = 8.0
    
    static let cardLikeOverlayImage = "IconHeart"
    static let cardLikeOverlayImageOverrideColor = true
    static let cardLikeOverlayImageColor = mainColor
    static let cardLikeOverlayBorderColor = mainColor
    static let cardLikeOverlayBorderWidth:CGFloat = 8.0
    
    
    /****************************************
     ************* Match Screen *************
     ****************************************/
    
    static let matchScreenYouMatchedFont = UIFont.init(name: "Zapfino", size: 36.0)
    static let matchScreenYouMatchedTextColor = UIColor.white
    
    static let matchScreenSubTitleFont = UIFont.systemFont(ofSize: 17.0)
    static let matchScreenSubTitleTextColor = UIColor.white
    
    static let matchScreenProfileImageBorderColor = UIColor.white
    
    static let matchScreenMessageButtonTextColor = UIColor.white
    static let matchScreenMessageButtonBorderColor = UIColor.white
    
    static let matchScreenContinueButtonTextColor = UIColor.white
    static let matchScreenContinueButtonBorderColor = UIColor.white
    
    /****************************************
     *********** Like Limit Popup ***********
     ****************************************/
    
    
    static let likeLimitScreenBackgroundColor = screenBackgroundColor
    static let likeLimitScreenCornerRadius:CGFloat = 10.0
    static let likeLimitScreenImage = "IconHeart"
    static let likeLimitScreenImageOverrideColor = true
    static let likeLimitScreenImageColor = lightGray
    
    static let likeLimitScreenOutOfTextColor = UIColor.black
    static let likeLimitScreenOutOfTextFont = UIFont.boldSystemFont(ofSize: 30.0)
    
    static let likeLimitScreenGetMoreInTextColor = UIColor(white: 0.85, alpha: 1.0)
    static let likeLimitScreenGetMoreInFont = UIFont.systemFont(ofSize: 16.0)
    
    static let likeLimitScreenCountdownTextColor = UIColor(white: 0.7, alpha: 1.0)
    static let likeLimitScreenCountdownFont = UIFont.systemFont(ofSize: 36.0)
    
    static let likeLimitScreenGetUnlimitedButtonBackgroundColor = mainColor
    static let likeLimitScreenGetUnlimitedButtonBorderColor = mainColor
    static let likeLimitScreenGetUnlimitedButtonBorderWidth:CGFloat = 0.0
    static let likeLimitScreenGetUnlimitedButtonCornerRadius:CGFloat = 5.0
    static let likeLimitScreenGetUnlimitedButtonTextColor = whiteColor
    static let likeLimitScreenGetUnlimitedButtonFont = UIFont.boldSystemFont(ofSize: 16.0)
    
    
    /****************************************
     ************* Upgrade Popup ************
     ****************************************/
    
    
    static let upgradeScreenBackgroundColor = screenBackgroundColor
    static let upgradeScreenBorderColor = screenBackgroundColor
    static let upgradeScreenBorderWidth:CGFloat = 0.0
    static let upgradeScreenCornerRadius:CGFloat = 10.0
    
    static let upgradeScreenLogoImage = "PlusLogo"
    static let upgradeScreenLogoImageOverrideColor = false
    static let upgradeScreenLogoImageColor = mainColor
    
    static let upgradeScreenFeatureBackgroundColor = UIColor(white: 0.95, alpha: 1.0)
    static let upgradeScreenFeatureImageBackgroundColor = screenBackgroundColor
    static let upgradeScreenFeatureImageBorderWidth:CGFloat = 6.0
    static let upgradeScreenFeatureImageBorderColor = lightestGray
    
    static let upgradeScreenFeatureImageOverrideColor = true
    
    static let upgradeScreenFeatureTitleTextColor = UIColor(white: 0.2, alpha: 1.0)
    static let upgradeScreenFeatureTitleFont = UIFont.boldSystemFont(ofSize: 14.0)
    
    static let upgradeScreenFeatureSubtitleTextColor = UIColor(white: 0.5, alpha: 1.0)
    static let upgradeScreenFeatureSubtitleFont = UIFont.systemFont(ofSize: 14.0)
    
    static let upgradeScreenUpgradeButtonBackgroundColor = mainColor
    static let upgradeScreenUpgradeButtonBorderColor = mainColor
    static let upgradeScreenUpgradeButtonBorderWidth:CGFloat = 0.0
    static let upgradeScreenUpgradeButtonCornerRadius:CGFloat = 5.0
    static let upgradeScreenUpgradeButtonTextColor = whiteColor
    static let upgradeScreenUpgradeButtonFont = UIFont.boldSystemFont(ofSize: 16.0)
    
    /****************************************
     ************* Passport Menu ************
     ****************************************/
    
    
    static let currentLocationBackgroundColor = screenBackgroundColor
    static let currentLocationImage = "IconLocation"
    static let currentLocationImageOverrideColor = true
    static let currentLocationImageColor = blueColor
    static let currentLocationTextColor = UIColor.black
    static let currentLocationFont = UIFont.systemFont(ofSize: 14.0)
    
    static let otherLocationBackgroundColor = screenBackgroundColor
    static let otherLocationImage = "IconPlane"
    static let otherLocationImageOverrideColor = true
    static let otherLocationImageColor = blueColor
    static let otherLocationTextColor = UIColor.black
    static let otherLocationFont = UIFont.systemFont(ofSize: 14.0)
    
    static let addLocationButtonBackgroundColor = blueColor
    static let addLocationButtonTextColor = whiteColor
    static let addLocationButtonFont = UIFont.boldSystemFont(ofSize: 16.0)
    
    static let closeButtonImage = "IconCross"
    static let closeButtonImageOverrideColor = true
    static let closeButtonImageColor = screenBackgroundColor
    static let closeButtonBackgroundColor = blueColor
    static let closeButtonBorderColor = screenBackgroundColor
    
    
    /****************************************
     ************** Map Screen **************
     ****************************************/
    
    
    static let mapScreenNavigationBarBackgroundColor = blueColor
    static let mapScreenNavigationBarButtonColor = whiteColor
    
    static let mapScreenPinImage = "IconLocation"
    static let mapScreenPinImageOverrideColor = true
    static let mapScreenPinImageColor = blueColor
    
    static let mapScreenCalloutImage = "IconPlane"
    static let mapScreenCalloutImageOverrideColor = true
    static let mapScreenCalloutImageColor = blueColor
    
    static let mapScreenCalloutChooseButtonImage = "IconArrow"
    static let mapScreenCalloutChooseButtonImageOverrideColor = true
    static let mapScreenCalloutChooseButtonImageColor = blueColor
    
    
    /****************************************
     *************** Side Menu **************
     ****************************************/
    
    
    static let sideMenuBackgroundColor = UIColor.darkGray
    static let sideMenuTextColor = whiteColor
    static let sideMenuFont = UIFont.systemFont(ofSize: 14.0)
    
    static let sideMenuProfileImageBorderColor = whiteColor
    static let sideMenuProfileImageBorderWidth:CGFloat = 2.0
    
    static let sideMenuHomeImage = "menuHome"
    static let sideMenuHomeImageOverrideColor = true
    static let sideMenuHomeImageColor = whiteColor
    
    static let sideMenuMessagesImage = "menuChats"
    static let sideMenuMessagesImageOverrideColor = true
    static let sideMenuMessagesImageColor = whiteColor
    
    static let sideMenuSettingsImage = "menuSettings"
    static let sideMenuSettingsImageOverrideColor = true
    static let sideMenuSettingsImageColor = whiteColor
    
    
    /****************************************
     ************* Chats Screen *************
     ****************************************/
    
    
    static let chatsScreenNavigationBarBackgroundColor = mainScreenNavigationBarBackgroundColor
    
    static let chatsScreenNavigationBarTitleImage = "IconChats"
    static let chatsScreenNavigationBarTitleImageOverrideColor = true
    static let chatsScreenNavigationBarTitleImageColor = mainScreenNavigationBarTitleImageColor
    
    static let chatsScreenNavigationBarBackImage = "IconSpark"
    static let chatsScreenNavigationBarBackColor = navigationBarButtonsColor
    
    static let chatsScreenChatBackgroundColor = whiteColor
    
    static let chatsScreenChatCellBackgroundColor = whiteColor
    static let chatsScreenChatCellUnreadBackgroundColor = UIColor(white: 0.9, alpha: 1.0)
    
    static let chatsScreenProfileImageBorderColor = mainColor
    static let chatsScreenProfileImageBorderWidth:CGFloat = 2.0
    
    static let chatsScreenNameTextColor = UIColor.black
    static let chatsScreenNameFont = UIFont.systemFont(ofSize: 20.0)
    
    static let chatsScreenLastMessageTimeTextColor = UIColor(white: 0.7, alpha: 1.0)
    static let chatsScreenLastMessageTimeFont = UIFont.systemFont(ofSize: 12.0)
    
    static let chatsScreenLastMessageTextColor = UIColor(white: 0.7, alpha: 1.0)
    static let chatsScreenLastMessageFont = UIFont.systemFont(ofSize: 12.0)
    
    static let chatsScreenLastMessageUnreadTextColor = UIColor(white: 0.7, alpha: 1.0)
    static let chatsScreenLastMessageUnreadFont = UIFont.boldSystemFont(ofSize: 12.0)
    
    
    /****************************************
     *********** Individual Chat ************
     ****************************************/
    
    
    static let chatScreenNavigationBarBackgroundColor = mainScreenNavigationBarBackgroundColor
    
    static let chatScreenNavigationBarTitleColor = UIColor.black
    
    static let chatScreenNavigationBarBackButtonColor = navigationBarButtonsColor
    
    static let chatScreenNavigationBarProfileImageBorderColor = mainColor
    static let chatScreenNavigationBarProfileImageBorderWidth:CGFloat = 1.0
    
    static let chatScreenBackgroundColor = whiteColor
    
    static let chatScreenMessageBubbleCornerRadius:CGFloat = 20.0
    static let chatScreenMessageBubbleFont = UIFont.systemFont(ofSize: 14.0)
    
    static let chatScreenOpponentMessageBackgroundColor = UIColor(white: 0.95, alpha: 1.0)
    static let chatScreenOpponentMessageTextColor = mainColor
    
    static let chatScreenCurrentUserMessageBackgroundColor = mainColor
    static let chatScreenCurrentUserMessageTextColor = whiteColor
    
    static let chatScreenNewMessageAreaBackgroundColor = UIColor(white: 0.95, alpha: 1.0)
    
    static let chatScreenNewMessageFieldBackgroundColor = whiteColor
    static let chatScreenNewMessageFieldTextColor = UIColor.black
    static let chatScreenNewMessageFieldFont = UIFont.systemFont(ofSize: 12.0)
    static let chatScreenNewMessageFieldCornerRadius:CGFloat = 3.0
    
    static let chatScreenSendButtonBackgroundColor = mainColor
    static let chatScreenSendButtonTextColor = whiteColor
    static let chatScreenSendButtonFont = UIFont.systemFont(ofSize: 12.0)
    static let chatScreenSendButtonCornerRadius:CGFloat = 5.0
    
    static let chatScreenConnectingMessageBackgroundColor = mainColor
    static let chatScreenConnectingMessageFont = UIFont.systemFont(ofSize: 12.0)
    static let chatScreenConnectingMessageTextColor = whiteColor
    
    
    /****************************************
     ************ Profile Screen ************
     ****************************************/
    
    
    static let profileScreenNavigationBarBackgroundColor = mainScreenNavigationBarBackgroundColor
    
    static let profileScreenNavigationBarBackButtonColor = navigationBarButtonsColor
    static let profileScreenNavigationBarMenuButtonColor = navigationBarButtonsColor
    static let profileScreenNavigationBarEditButtonColor = navigationBarButtonsColor
    
    static let profileScreenDislikeButtonImage = "IconCross"
    static let profileScreenDislikeButtonImageOverrideColor = true
    static let profileScreenDislikeButtonImageColor = UIColor(white: 0.8, alpha: 1.0)
    static let profileScreenDislikeButtonBorderColor = UIColor(white: 0.8, alpha: 1.0)
    static let profileScreenDislikeButtonBorderWidth:CGFloat = 4.0
    
    static let profileScreenLikeButtonImage = "IconHeart"
    static let profileScreenLikeButtonImageOverrideColor = true
    static let profileScreenLikeButtonImageColor = mainColor
    static let profileScreenLikeButtonBorderColor = mainColor
    static let profileScreenLikeButtonBorderWidth:CGFloat = 4.0
    
    static let profileScreenImagesPageControlActiveColor = mainColor
    static let profileScreenImagesPageControlInactiveColor = UIColor(white: 0.9, alpha: 1.0)
    
    static let profileScreenBackgroundColor = whiteColor
    
    static let profileScreenNameTextColor = UIColor.darkGray
    static let profileScreenNameFont = UIFont.boldSystemFont(ofSize: 18.0)
    
    static let profileScreenDistanceTextColor = UIColor.darkGray
    static let profileScreenDistanceFont = UIFont.boldSystemFont(ofSize: 12.0)
    
    static let profileScreenLastActiveTextColor = UIColor.lightGray
    static let profileScreenLastActiveFont = UIFont.systemFont(ofSize: 12.0)
    
    static let profileScreenAboutHeadingTextColor = UIColor.lightGray
    static let profileScreenAboutHeadingFont = UIFont.boldSystemFont(ofSize: 16.0)
    
    static let profileScreenAboutTextTextColor = UIColor.black
    static let profileScreenAboutTextFont = UIFont.systemFont(ofSize: 12.0)
    
    
    /****************************************
     ************* Edit Profile *************
     ****************************************/
    
    
    static let editProfileScreenNavigationBarBackgroundColor = mainScreenNavigationBarBackgroundColor
    static let editProfileScreenNavigationBarCancelButtonColor = navigationBarButtonsColor
    static let editProfileScreenNavigationBarSaveButtonColor = navigationBarButtonsColor
    static let editProfileScreenNavigationBarTitleColor = UIColor.black
    
    static let editProfileScreenBackgroundColor = UIColor(white: 0.95, alpha: 1.0)
    
    static let editProfileScreenBlankImageColor = UIColor(white: 0.8, alpha: 1.0)
    
    static let editProfileScreenRemoveImageButtonBackgroundColor = whiteColor
    static let editProfileScreenRemoveImageButtonImage = "IconCross"
    static let editProfileScreenRemoveImageButtonImageOverrideColor = true
    static let editProfileScreenRemoveImageButtonImageColor = mainColor
    
    static let editProfileScreenAddImageButtonBackgroundColor = whiteColor
    static let editProfileScreenAddImageButtonImage = "IconPlus"
    static let editProfileScreenAddImageButtonImageOverrideColor = true
    static let editProfileScreenAddImageButtonImageColor = mainColor
    
    static let editProfileScreenAboutHeadingTextColor = UIColor.darkGray
    static let editProfileScreenAboutHeadingFont = UIFont.boldSystemFont(ofSize: 22.0)
    
    static let editProfileScreenAboutTextBackground = whiteColor
    static let editProfileScreenAboutTextTextColor = UIColor.darkGray
    static let editProfileScreenAboutTextFont = UIFont.systemFont(ofSize: 12.0)
    
    static let editProfileScreenAboutCharacterCountTextColor = UIColor.black
    static let editProfileScreenAboutCharacterCountFont = UIFont.boldSystemFont(ofSize: 12.0)
    
    
    /****************************************
     *********** Settings Screen ************
     ****************************************/
    
    
    static let settingsScreenNavigationBarBackgroundColor = mainScreenNavigationBarBackgroundColor
    static let settingsScreenNavigationBarButtonsColor = navigationBarButtonsColor
    static let settingsScreenNavigationBarTitleColor = UIColor.black
    
    static let settingsScreenBackgroundColor = UIColor.groupTableViewBackground
    
    static let settingsScreenSettingHeadingTextColor = UIColor.darkGray
    static let settingsScreenSettingHeadingFont = UIFont.boldSystemFont(ofSize: 18)
    
    static let settingsScreenSelectedValueFont = UIFont.boldSystemFont(ofSize: 18)
    static let settingsScreenSelectedValueTextColor = UIColor.black
    
    static let settingsScreenGenderSelectorCellBackgroundColor = whiteColor
    static let settingsScreenGenderSelectorColor = mainColor
    
    static let settingsScreenAgesCellBackgroundColor = whiteColor
    static let settingsScreenAgesSliderColor = mainColor
    
    static let settingsScreenDistanceCellBackgroundColor = whiteColor
    static let settingsScreenDistanceSliderColor = mainColor
    
    static let settingsScreenShowMeMenCellBackgroundColor = whiteColor
    static let settingsScreenShowMeMenButtonColor = mainColor
    
    static let settingsScreenShowMeWomenCellBackgroundColor = whiteColor
    static let settingsScreenShowMeWomenButtonColor = mainColor
    
    static let settingsScreenLogoutBackgroundColor = whiteColor
    static let settingsScreenLogoutTextColor = UIColor.darkGray
    static let settingsScreenLogoutFont = UIFont.boldSystemFont(ofSize: 20.0)
    
    static let settingsScreenRestorePurchasesBackgroundColor = whiteColor
    static let settingsScreenRestorePurchasesTextColor = UIColor.darkGray
    static let settingsScreenRestorePurchasesFont = UIFont.boldSystemFont(ofSize: 20.0)
    
    static let settingsScreenCreditsBackgroundColor = whiteColor
    static let settingsScreenCreditsTextColor = UIColor.darkGray
    static let settingsScreenCreditsFont = UIFont.boldSystemFont(ofSize: 20.0)
    
    static let settingsScreenDeleteAccountBackgroundColor = whiteColor
    static let settingsScreenDeleteAccountTextColor = UIColor.red
    static let settingsScreenDeleteAccountFont = UIFont.boldSystemFont(ofSize: 20.0)
    
}
