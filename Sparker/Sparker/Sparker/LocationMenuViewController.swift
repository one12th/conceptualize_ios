//
//  LocationMenuViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 15/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class LocationMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    let containerView = UIView()
    let tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.grouped)
    let closeButton = UIButton()
    let addLocationButton = UIButton()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(white: 0.0, alpha: 0.8)
        
        var frame = appDelegate.swipeViewController.locationButton.frame
        frame.origin.y += 64
        self.closeButton.frame = frame
        self.closeButton.backgroundColor = AppConfig.closeButtonBackgroundColor
        var image = UIImage(named: AppConfig.closeButtonImage)
        if(AppConfig.closeButtonImageOverrideColor){
            image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        self.closeButton.setImage(image, for: UIControlState())
        self.closeButton.tintColor = AppConfig.closeButtonImageColor
        self.closeButton.imageEdgeInsets = UIEdgeInsets(top: 18, left: 18, bottom: 18, right: 18)
        self.closeButton.layer.cornerRadius = self.closeButton.frame.size.height/2
        self.closeButton.layer.borderWidth = appDelegate.swipeViewController.locationButton.layer.borderWidth
        self.closeButton.layer.borderColor = AppConfig.closeButtonBorderColor.cgColor
        self.closeButton.addTarget(self, action: #selector(LocationMenuViewController.didTapCloseButton), for: UIControlEvents.touchUpInside)
        self.view.addSubview(self.closeButton)
        
        let dialogArrow = UIView(frame: CGRect(x: frame.origin.x + frame.size.width/2 - 18, y: frame.origin.y - 22, width: 36, height: 20))
        dialogArrow.backgroundColor = AppConfig.addLocationButtonBackgroundColor
        
        let path = CGMutablePath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addArc(tangent1End: CGPoint(x: 18, y: 20), tangent2End: CGPoint(x: 36, y: 0), radius: 5)
        path.addLine(to: CGPoint(x: 36, y: 0))
        //CGPathMoveToPoint(path, nil, 0, 0)
        //CGPathAddArcToPoint(path, nil, 18, 20, 36, 0, 5)
        //CGPathAddLineToPoint(path, nil, 36, 0)
        path.closeSubpath()
        
        let mask = CAShapeLayer()
        mask.frame = dialogArrow.bounds
        mask.path = path//.CGPath
        
        dialogArrow.layer.mask = mask
        
        self.view.addSubview(dialogArrow)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.backgroundColor = UIColor.white
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        let tableViewHeight = min(270, CGFloat(self.tableView.numberOfRows(inSection: 0))*55 + 33.0)
        self.tableView.frame = CGRect(x:0, y: 0, width: self.view.bounds.size.width - 32, height: tableViewHeight)
        self.containerView.addSubview(self.tableView)
        
        self.containerView.frame = CGRect(x: 16, y: frame.origin.y - tableViewHeight - 22.0 - 55.0, width: self.view.bounds.size.width - 32, height: tableViewHeight + 55.0)
        self.containerView.layer.cornerRadius = 5.0
        self.containerView.clipsToBounds = true
        self.view.addSubview(self.containerView)
        
        self.addLocationButton.frame = CGRect(x: 0, y: tableViewHeight, width: self.view.bounds.size.width - 32, height: 55.0)
        self.addLocationButton.backgroundColor = AppConfig.addLocationButtonBackgroundColor
        self.addLocationButton.setTitle("Add a New Location", for: UIControlState())
        self.addLocationButton.setTitleColor(AppConfig.addLocationButtonTextColor, for: UIControlState())
        self.addLocationButton.titleLabel!.font = AppConfig.addLocationButtonFont
        self.addLocationButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
        self.addLocationButton.addTarget(self, action: #selector(LocationMenuViewController.showMapView), for: UIControlEvents.touchUpInside)
        self.containerView.addSubview(self.addLocationButton)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.appDelegate.userLocations.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "locationCell")
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "locationCell")
        }
        cell!.selectionStyle = UITableViewCellSelectionStyle.none
        /*if(indexPath.row == self.tableView.numberOfRowsInSection(0) - 1){
            cell!.backgroundColor = blueColor
            //cell!.imageView!.image = UIImage(named: "IconPlane")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            //cell!.imageView!.tintColor = UIColor.whiteColor()
            cell!.textLabel?.textAlignment = NSTextAlignment.Center
            cell!.textLabel?.text = "Add a New Location"
            cell!.textLabel?.textColor = UIColor.whiteColor()
        }else{*/
            cell!.backgroundColor = UIColor.white
            if(indexPath.row == 0){
                cell!.backgroundColor = AppConfig.currentLocationBackgroundColor
                var image = UIImage(named: AppConfig.currentLocationImage)
                if(AppConfig.currentLocationImageOverrideColor){
                    image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                }
                cell!.imageView!.image = image
                cell!.textLabel!.text = "My Current Location"
                cell!.textLabel!.textColor = AppConfig.currentLocationTextColor
                cell!.textLabel!.font = AppConfig.currentLocationFont
                cell!.imageView!.tintColor = AppConfig.currentLocationImageColor
            }else{
                cell!.backgroundColor = AppConfig.otherLocationBackgroundColor
                var image = UIImage(named: AppConfig.otherLocationImage)
                if(AppConfig.otherLocationImageOverrideColor){
                    image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                }
                cell!.imageView!.image = image
                cell!.textLabel!.text = self.appDelegate.userLocations[indexPath.row-1].name
                cell!.textLabel!.textColor = AppConfig.otherLocationTextColor
                cell!.textLabel!.font = AppConfig.otherLocationFont
                cell!.imageView!.tintColor = AppConfig.otherLocationImageColor
            }
            cell!.textLabel?.textAlignment = NSTextAlignment.left
            cell!.accessoryType = UITableViewCellAccessoryType.none
            if(indexPath.row == self.appDelegate.userSettings["activeLocation"]){
                cell!.accessoryType = UITableViewCellAccessoryType.checkmark
            }
        //}
        
        return cell!
    }
    
    func addedNewLocation(){
        var frame = appDelegate.swipeViewController.locationButton.frame
        frame.origin.y += 64
        let tableViewHeight = min(270, CGFloat(self.tableView(self.tableView, numberOfRowsInSection: 0))*55 + 33.0)
        self.containerView.frame = CGRect(x: 16, y: frame.origin.y - tableViewHeight - 22.0 - 55.0, width: self.view.bounds.size.width - 32, height: tableViewHeight + 55.0)
        self.tableView.frame = CGRect(x:0, y: 0, width: self.view.bounds.size.width - 32, height: tableViewHeight)
        self.addLocationButton.frame = CGRect(x: 0, y: tableViewHeight, width: self.view.bounds.size.width - 32, height: 55.0)
        self.tableView.reloadData()
    }
    
    @objc func showMapView(){
        let mapViewController = MapViewController(delegate: self)
        mapViewController.delegate = self
        let navController = UINavigationController(rootViewController: mapViewController)
        navController.navigationBar.barTintColor = AppConfig.mapScreenNavigationBarBackgroundColor
        navController.navigationBar.tintColor = AppConfig.mapScreenNavigationBarButtonColor
        self.present(navController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.checkmark
        self.appDelegate.userSettings["activeLocation"] = indexPath.row
        DispatchQueue.main.async { () -> Void in
            self.appDelegate.swipeViewController.resetCards()
            self.presentingViewController!.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if(indexPath.row != 0){
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == UITableViewCellEditingStyle.delete){
            if(self.appDelegate.userSettings["activeLocation"] == indexPath.row){
                self.appDelegate.userSettings["activeLocation"] = 0
            }
            self.appDelegate.userLocations.remove(at: indexPath.row - 1)
            self.addedNewLocation()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 33.0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Discover People In"
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    @objc func didTapCloseButton(){
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
}
