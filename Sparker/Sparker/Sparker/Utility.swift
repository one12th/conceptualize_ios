//
//  Utility.swift
//  Sparker
//
//  Created by Samuel Harrison on 04/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setBackgroundColor(_ color: UIColor, forState: UIControlState) {
        
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.clipsToBounds = true
        self.setBackgroundImage(colorImage, for: forState)
    }
}

class Utility: NSObject, CLLocationManagerDelegate{

    static let sharedInstance = Utility()
    var currentUserProfile: UserProfile?{
        didSet{
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: self.currentUserProfile!), forKey: "savedUserProfile")
        }
    }
    var locationManager: CLLocationManager
    var foundLocation = false
    var currentlyUploading = false
    var uploadAgain = false
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var soundID: SystemSoundID = 0
    
    override init(){
        self.locationManager = CLLocationManager()
        super.init()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        self.locationManager.distanceFilter = 1000
        self.locationManager.delegate = self
        
        self.preloadNotificationSound()
    }
    
    func preloadNotificationSound(){
        let soundID = createSoundIDWithName("notification", fileExtension: "mp3")
        self.soundID = soundID
    }
    
    func createSoundIDWithName(_ filename:String,fileExtension:String)->SystemSoundID{
        let fileURL = Bundle.main.url(forResource: filename, withExtension: fileExtension)
        if(fileURL != nil && FileManager.default.fileExists(atPath: fileURL!.path)){
            var soundID:SystemSoundID = 0
            let error = AudioServicesCreateSystemSoundID(fileURL! as CFURL, &soundID)
            if(error != kAudioServicesNoError){
                print("Error! SystemSoundID could not be created")
                return 0
            }else{
                return soundID
            }
        }
        print("Error: audio file not found: \(String(describing: fileURL))")
        return 0
    }
    
    func startUpdatingLocation(){
        print("Started updating location")
        let status = CLLocationManager.authorizationStatus()
        if(status == CLAuthorizationStatus.notDetermined){
            self.locationManager.requestWhenInUseAuthorization()
        }
        self.foundLocation = false
        self.locationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation(){
        print("Stopped updating location")
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(self.foundLocation || locations.count == 0 || Date().timeIntervalSince(locations.last!.timestamp) > 30){
            return
        }
        self.foundLocation = true
        self.stopUpdatingLocation()
        self.currentUserProfile!.location = locations.last
        Utility.sharedInstance.uploadCurrentUser({ () -> Void in
            NotificationCenter.default.post(name: Notification.Name(rawValue: "com.samuelharrison.sparker.utility.userLocationUpdated"), object: nil)
            }, errorBlock: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error updating location: \(error.localizedDescription)")
    }
    
    func uploadCurrentUser(_ completionBlock:(()->Void)?,errorBlock:(()->Void)?){
        if(Utility.sharedInstance.currentUserProfile == nil){
            return
        }
        if(self.currentlyUploading){
            self.uploadAgain = true
            return
        }
        self.uploadAgain = false
        self.currentlyUploading = true
        QBRequest.update(Utility.sharedInstance.currentUserProfile!.object, successBlock: { (response, object) -> Void in
            /*if(object != nil){
                Utility.sharedInstance.currentUserProfile = UserProfile(object: object!)
            }*/
                self.currentlyUploading = false
                if(self.uploadAgain){
                    self.uploadCurrentUser(nil, errorBlock: nil)
                }
                completionBlock?()
            }, errorBlock: { (response) -> Void in
                self.currentlyUploading = false
                print("Error updating user profile: \(String(describing: response.error))")
                errorBlock?()
        })
    }
    
    func allowedToLike()->Bool{
        if(!AppConfig.shouldLimitLikes || self.appDelegate.subscriptionExpirationTimestamp > Date().timeIntervalSince1970 || Utility.sharedInstance.currentUserProfile!.likeLimitLifted.timeIntervalSince1970 < Date().timeIntervalSince1970){
            return true
        }
        return false
    }
    
    func playNotification(){
        AudioServicesPlayAlertSound(self.soundID)
    }
    
}
