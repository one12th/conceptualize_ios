//
//  CustomFlowLayout.swift
//  Sparker
//
//  Created by Samuel Harrison on 16/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

protocol CustomFlowLayoutDelegate : UICollectionViewDelegate {
    func moveDataItem(_ fromIndexPath : IndexPath, toIndexPath: IndexPath) -> Void
}

class CustomFlowLayout: RFQuiltLayout, UIGestureRecognizerDelegate{

    struct Bundle {
        var offset : CGPoint = CGPoint.zero
        var sourceCell : UICollectionViewCell
        var representationImageView : UIView
        var currentIndexPath : IndexPath
    }
    var bundle : Bundle?
    var canvas : UIView?
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        if self.canvas == nil{
            self.canvas = self.collectionView!.superview
            if let collectionView = self.collectionView{
                let gesture = UILongPressGestureRecognizer(target: self, action: #selector(CustomFlowLayout.handleGesture(_:)))
                gesture.minimumPressDuration = 0.2
                gesture.delegate = self
                collectionView.addGestureRecognizer(gesture)
            }
        }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let ca = self.canvas{
            if let cv = self.collectionView{
                let pointPressedInCanvas = gestureRecognizer.location(in: ca)
                
                for cell in cv.visibleCells as [UICollectionViewCell]{
                    let cellInCanvasFrame = ca.convert(cell.frame, from: cv)
                    
                    if cellInCanvasFrame.contains(pointPressedInCanvas){
                        let representationImage = cell.snapshotView(afterScreenUpdates: true)
                        representationImage!.frame = cellInCanvasFrame
                        
                        let offset = CGPoint(x: pointPressedInCanvas.x - cellInCanvasFrame.origin.x, y: pointPressedInCanvas.y - cellInCanvasFrame.origin.y)
                        
                        let indexPath: IndexPath = cv.indexPath(for: cell as UICollectionViewCell)!
                        
                        if(indexPath.item >= (self.collectionView!.delegate as! EditProfileViewController).images.count){
                            return false
                        }
                        
                        self.bundle = Bundle(offset: offset, sourceCell: cell, representationImageView: representationImage!, currentIndexPath: indexPath)
                    }
                }
            }
        }
        return (self.bundle != nil)
    }
    
    @objc func handleGesture(_ gesture:UILongPressGestureRecognizer){
        
        if let bundle = self.bundle{
            
            let dragPointOnCanvas = gesture.location(in: self.canvas)
            
            if gesture.state == UIGestureRecognizerState.began {
                /*(self.collectionView?.visibleCells() as! [EditImageCollectionViewCell]).forEach{ cell in
                    cell.startWobbling()
                }*/
                bundle.sourceCell.isHidden = true
                self.canvas?.addSubview(bundle.representationImageView)
                (self.collectionView!.delegate as! EditProfileViewController).reorderingImages = true
            }
            
            if gesture.state == UIGestureRecognizerState.changed {
                
                // Update the representation image
                var imageViewFrame = bundle.representationImageView.frame
                var point = CGPoint.zero
                point.x = dragPointOnCanvas.x - bundle.offset.x
                point.y = dragPointOnCanvas.y - bundle.offset.y
                imageViewFrame.origin = point
                bundle.representationImageView.frame = imageViewFrame
                
                if let indexPath = self.collectionView?.indexPathForItem(at: gesture.location(in: self.collectionView)) {
                    
                    if(indexPath.item >= (self.collectionView!.delegate as! EditProfileViewController).images.count){
                        return
                    }
                    
                    if (indexPath == bundle.currentIndexPath) == false {
                        
                        if(indexPath.item == 0){
                            imageViewFrame.size.width = self.blockPixels.width * 2
                            imageViewFrame.size.height = self.blockPixels.height * 2
                        }else{
                            imageViewFrame.size.width = self.blockPixels.width
                            imageViewFrame.size.height = self.blockPixels.height
                        }
                        
                        UIView.animate(withDuration: 0.2, animations: { () -> Void in
                            bundle.representationImageView.frame = imageViewFrame
                        })
                        
                        if let delegate = self.collectionView!.delegate as? CustomFlowLayoutDelegate {
                            delegate.moveDataItem(bundle.currentIndexPath, toIndexPath: indexPath)
                        }
                        
                        self.collectionView!.moveItem(at: bundle.currentIndexPath, to: indexPath)
                        
                        self.bundle!.currentIndexPath = indexPath
                        
                    }          
                }            
            }
            
            if (gesture.state == UIGestureRecognizerState.ended || gesture.state == UIGestureRecognizerState.cancelled || gesture.state == UIGestureRecognizerState.failed) {
                
                /*(self.collectionView?.visibleCells() as! [EditImageCollectionViewCell]).forEach{ cell in
                    cell.stopWobbling()
                }*/
                bundle.sourceCell.isHidden = false
                bundle.representationImageView.removeFromSuperview()
                if let cv = self.collectionView, cv.delegate is CustomFlowLayoutDelegate {
                    self.collectionView!.reloadData()
                }
                self.bundle = nil
                
            }
        }
    }
    
}
