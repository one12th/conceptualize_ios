//
//  TextMessageCell.swift
//  Sparker
//
//  Created by Samuel Harrison on 11/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class TextMessageCell: UICollectionViewCell {
    
    let padding:CGFloat = 12.0
    
    let messageContainer = UIView()
    var messageText = UILabel()
    var profileImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.addSubview(self.messageContainer)
        self.messageContainer.addSubview(self.messageText)
        self.messageContainer.layer.cornerRadius = AppConfig.chatScreenMessageBubbleCornerRadius
        self.messageText.numberOfLines = 0
        self.messageText.font = AppConfig.chatScreenMessageBubbleFont
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func configure(_ dialog:QBChatDialog, message:QBChatMessage){
        
        let messageText = message.text ?? ""
        let messageSize = (messageText as NSString).boundingRect(with: CGSize(width: 0.8*self.bounds.size.width - 4*self.padding, height: CGFloat.greatestFiniteMagnitude), options: [NSStringDrawingOptions.truncatesLastVisibleLine, NSStringDrawingOptions.usesLineFragmentOrigin], attributes: [NSAttributedStringKey.font:AppConfig.chatScreenMessageBubbleFont], context: nil).size
        self.messageText.frame = CGRect(x: self.padding, y: self.padding, width: max(messageSize.width,20), height: messageSize.height)
        self.messageText.text = messageText
        self.messageText.textAlignment = NSTextAlignment.left
        if(messageText.characters.count <= 3){
            self.messageText.textAlignment = NSTextAlignment.center
        }
        if(message.senderID == QBSession.current().currentUser!.id){
            self.messageContainer.frame = CGRect(x: self.bounds.size.width - self.padding - (max(messageSize.width,20) + 2*self.padding), y: 0, width: max(messageSize.width,20) + 2*self.padding, height: messageSize.height + 2*self.padding)
            self.messageContainer.backgroundColor = AppConfig.chatScreenCurrentUserMessageBackgroundColor
            self.messageText.textColor = AppConfig.chatScreenCurrentUserMessageTextColor
        }else{
            if let profile = ChatService.sharedInstance.getDialogOpponent(dialog){
                if profile.images.count > 0{
                    if let urlString = QBCBlob.privateUrl(forID: profile.images[0]){
                        if let url = URL(string: urlString){
                            self.profileImageView.setImageWith(url, placeholderImage: UIImage(named: "PlaceholderImage"))
                        }
                    }
                }
            }
            self.messageContainer.frame = CGRect(x:self.padding, y: 0, width: max(messageSize.width,20) + 2*self.padding, height: messageSize.height + 2*self.padding)
            self.messageContainer.backgroundColor = AppConfig.chatScreenOpponentMessageBackgroundColor
            self.messageText.textColor = AppConfig.chatScreenOpponentMessageTextColor
        }
    }
}
