//
//  SliderSettingsCell.swift
//  Sparker
//
//  Created by Samuel Harrison on 08/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class SliderSettingsCell: UITableViewCell {
    
    var slider: UISlider!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.slider = UISlider(frame: CGRect(x: 26, y: 0, width: self.bounds.size.width - 52, height: self.bounds.size.height))
        self.contentView.addSubview(self.slider)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.slider.frame = CGRect(x: 26, y: 0, width: self.bounds.size.width - 52, height: self.bounds.size.height)
    }

}
