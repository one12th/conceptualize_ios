//
//  ProfileViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 04/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import FormatterKit
import SWRevealViewController

class ProfileViewController: UIViewController, UIScrollViewDelegate{
    
    let scrollView = UIScrollView()
    let imagesController = UIScrollView()
    var profile:UserProfile
    let nameAgeLabel = UILabel()
    let imagePageControl = CustomPageControl()
    let lastActiveLabel = UILabel()
    let bioHeadingLabel = UILabel()
    let bioLabel = UILabel()
    let distanceLabel = UILabel()
    let reportButton = UIButton()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let alreadyMatched:Bool
    
    init(profile:UserProfile, alreadyMatched:Bool){
        self.profile = profile
        self.alreadyMatched = alreadyMatched
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        self.navigationItem.backBarButtonItem?.tintColor = AppConfig.profileScreenNavigationBarBackButtonColor
        
        self.reportButton.backgroundColor = UIColor.white
        self.reportButton.setTitle("Report User", for: UIControlState())
        self.reportButton.setTitleColor(UIColor.red, for: UIControlState())
        self.reportButton.addTarget(self, action: #selector(ProfileViewController.didTapReportButton), for: UIControlEvents.touchUpInside)
        
        if(alreadyMatched){
            self.scrollView.addSubview(self.reportButton)
            //
        }else if(profile.userID == Utility.sharedInstance.currentUserProfile!.userID){
            let menuButton = UIBarButtonItem(image: UIImage(named: "IconMenu"), style: UIBarButtonItemStyle.plain, target: self.appDelegate.masterViewContainer, action: #selector(SWRevealViewController.revealToggle(_:)))
            menuButton.tintColor = AppConfig.profileScreenNavigationBarMenuButtonColor
            self.navigationItem.leftBarButtonItem = menuButton
            
            let editButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.edit, target: self, action: #selector(ProfileViewController.didTapEditButton))
            editButton.tintColor = AppConfig.profileScreenNavigationBarEditButtonColor
            self.navigationItem.rightBarButtonItem = editButton
        }else{
            self.scrollView.addSubview(self.reportButton)
            //let dislikeButton = UIBarButtonItem(image: , style: UIBarButtonItemStyle.Plain, target: self, action: "didTapDislikeButton")
            let dislikeButton = UIButton()
            dislikeButton.addConstraints([
                    NSLayoutConstraint(item: dislikeButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 34.0),
                    NSLayoutConstraint(item: dislikeButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 34.0)
            ])
            var image = UIImage(named: AppConfig.profileScreenDislikeButtonImage)
            if(AppConfig.profileScreenDislikeButtonImageOverrideColor){
                image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            }
            dislikeButton.setImage(image, for: UIControlState())
            dislikeButton.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            dislikeButton.layer.borderWidth = AppConfig.profileScreenDislikeButtonBorderWidth
            dislikeButton.layer.borderColor = AppConfig.profileScreenDislikeButtonBorderColor.cgColor
            dislikeButton.layer.cornerRadius = 17.0
            dislikeButton.addTarget(self, action: #selector(ProfileViewController.didTapDislikeButton), for: UIControlEvents.touchUpInside)
            dislikeButton.tintColor = AppConfig.profileScreenDislikeButtonImageColor
            
            let likeButton = UIButton()
            likeButton.addConstraints([
                NSLayoutConstraint(item: likeButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 34.0),
                NSLayoutConstraint(item: likeButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 34.0)
                ])
            likeButton.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            likeButton.layer.borderWidth = AppConfig.profileScreenLikeButtonBorderWidth
            likeButton.layer.borderColor = AppConfig.profileScreenLikeButtonBorderColor.cgColor
            likeButton.layer.cornerRadius = 17.0
            image = UIImage(named: AppConfig.profileScreenLikeButtonImage)
            if(AppConfig.profileScreenLikeButtonImageOverrideColor){
                image = image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            }
            likeButton.setImage(image, for: UIControlState())
            likeButton.addTarget(self, action: #selector(ProfileViewController.didTapLikeButton), for: UIControlEvents.touchUpInside)
            likeButton.tintColor = AppConfig.profileScreenLikeButtonImageColor
            self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: likeButton),UIBarButtonItem(customView: dislikeButton)]
        }
        
        self.view.backgroundColor = UIColor.white
        
        self.scrollView.frame = self.view.bounds
        self.scrollView.showsVerticalScrollIndicator = false
        self.view.addSubview(self.scrollView)
        
        self.imagesController.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.width)
        self.imagesController.delegate = self
        self.imagesController.isPagingEnabled = true
        self.imagesController.showsHorizontalScrollIndicator = false
        self.imagesController.showsVerticalScrollIndicator = false
        //self.view.addSubview(self.imagesController)
        self.scrollView.addSubview(self.imagesController)
        
        self.imagePageControl.currentPage = 0
        self.imagePageControl.currentPageIndicatorTintColor = AppConfig.profileScreenImagesPageControlActiveColor
        self.imagePageControl.pageIndicatorTintColor = AppConfig.profileScreenImagesPageControlInactiveColor
        self.navigationItem.titleView = self.imagePageControl
        self.navigationItem.titleView?.sizeToFit()
        
        self.nameAgeLabel.frame = CGRect(x: 16, y: self.view.bounds.size.width + 16, width: self.view.bounds.size.width - 32, height: 36.0)
        self.nameAgeLabel.textColor = AppConfig.profileScreenNameTextColor
        self.nameAgeLabel.font = AppConfig.profileScreenNameFont
        self.nameAgeLabel.text = ""
        if(profile.name != ""){
            self.nameAgeLabel.text! += profile.name+", "
        }
        self.nameAgeLabel.text! += "\(profile.age!)"
        self.scrollView.addSubview(self.nameAgeLabel)
        
        self.distanceLabel.frame = CGRect(x: 16, y: self.view.bounds.size.width + 8 + 44, width: self.view.bounds.size.width, height: 22)
        self.distanceLabel.textColor = AppConfig.profileScreenDistanceTextColor
        self.distanceLabel.font = AppConfig.profileScreenDistanceFont
        self.distanceLabel.text = "\(Int(Utility.sharedInstance.currentUserProfile!.location.distance(from: self.profile.location)/1000)) kilometers away"
        self.distanceLabel.sizeToFit()
        self.scrollView.addSubview(self.distanceLabel)
        
        let distanceLabelWidth = (self.distanceLabel.text! as NSString).boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude), options: [], attributes: [NSAttributedStringKey.font:UIFont.boldSystemFont(ofSize: 12.0)], context: nil).width
        self.lastActiveLabel.frame = CGRect(x: 16 + distanceLabelWidth + 8, y: self.view.bounds.size.width + 8 + 44, width: self.view.bounds.size.width, height: 22)
        self.lastActiveLabel.textColor = UIColor.lightGray
        self.lastActiveLabel.font = UIFont.systemFont(ofSize: 12)
        let timeFormatter = TTTTimeIntervalFormatter()
        timeFormatter.usesIdiomaticDeicticExpressions = true
        self.lastActiveLabel.text = "Active "+timeFormatter.string(forTimeInterval: self.profile.updatedAt - Date().timeIntervalSince1970)
        self.lastActiveLabel.sizeToFit()
        self.scrollView.addSubview(self.lastActiveLabel)
        
        self.bioHeadingLabel.frame = CGRect(x: 16, y: self.view.bounds.size.width + 44 + 22 + 16, width: self.view.bounds.size.width - 32, height: 33)
        self.bioHeadingLabel.textColor = AppConfig.profileScreenAboutHeadingTextColor
        self.bioHeadingLabel.font = AppConfig.profileScreenAboutHeadingFont
        self.bioHeadingLabel.text = "About "+profile.name
        self.scrollView.addSubview(self.bioHeadingLabel)
        
        self.bioLabel.numberOfLines = 0
        self.bioLabel.textColor = AppConfig.profileScreenAboutTextTextColor
        self.bioLabel.font = AppConfig.profileScreenAboutTextFont
        self.scrollView.addSubview(self.bioLabel)
        
        self.setupProfile()
    }
    
    func setupProfile(){
        self.imagesController.subviews.forEach({view in
            view.removeFromSuperview()
        })
        var imageIndex = 0
        if(profile.images.count == 0){
            let imageView = UIImageView(frame: CGRect(x: CGFloat(imageIndex)*self.view.bounds.size.width, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.width))
            imageView.contentMode = UIViewContentMode.scaleAspectFill
            imageView.clipsToBounds = true
            imageView.image = UIImage(named: "PlaceholderImage")
            self.imagesController.addSubview(imageView)
            imageIndex += 1

        }
        for image in profile.images{
            if let urlString = QBCBlob.privateUrl(forID: image){
                if let url = URL(string: urlString){
                    let imageView = UIImageView(frame: CGRect(x: CGFloat(imageIndex)*self.view.bounds.size.width, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.width))
                    imageView.contentMode = UIViewContentMode.scaleAspectFill
                    imageView.clipsToBounds = true
                    imageView.setImageWith(url, placeholderImage: UIImage(named: "PlaceholderImage"))
                    self.imagesController.addSubview(imageView)
                    imageIndex += 1
                }
            }
        }
        self.imagesController.contentSize = CGSize(width: self.view.bounds.size.width * CGFloat(imageIndex), height: self.view.bounds.size.width)
        self.imagePageControl.numberOfPages = imageIndex
        
        let bioHeight = (profile.bio as NSString).boundingRect(with: CGSize(width: self.view.bounds.size.width - 32, height: CGFloat.greatestFiniteMagnitude), options: [NSStringDrawingOptions.truncatesLastVisibleLine, NSStringDrawingOptions.usesLineFragmentOrigin], attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 12.0)], context: nil).height + 24
        self.bioLabel.frame = CGRect(x: 16, y: self.view.bounds.size.width + 44 + 22 + 16 + 33, width: self.view.bounds.size.width - 32, height: bioHeight)
        self.bioLabel.text = profile.bio
        
        self.reportButton.frame = CGRect(x: 0, y: self.bioLabel.frame.origin.y + self.bioLabel.frame.size.height + 32, width: self.view.bounds.size.width, height: 44)
    }
    
    override func viewDidLayoutSubviews() {
        self.scrollView.frame = self.view.bounds
        var contentRect = CGRect.zero;
        for view in self.scrollView.subviews{
            contentRect = contentRect.union(view.frame);
        }
        contentRect.size.width = self.view.bounds.size.width
        self.scrollView.contentSize = contentRect.size;
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.imagePageControl.currentPage = lround(Double(scrollView.contentOffset.x / self.view.bounds.size.width))
    }
    
    @objc func didTapLikeButton(){
        if(self.appDelegate.swipeViewController.loadedCards[0].profile.userID == self.profile.userID){
            self.appDelegate.swipeViewController.didTapLikeButton()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapDislikeButton(){
        if(self.appDelegate.swipeViewController.loadedCards[0].profile.userID == self.profile.userID){
            self.appDelegate.swipeViewController.didTapDislikeButton()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapEditButton(){
        if(profile.userID != Utility.sharedInstance.currentUserProfile!.userID){
            return
        }
        let editProfileViewController = EditProfileViewController()
        editProfileViewController._presentingProfileViewController = self
        let navController = UINavigationController(rootViewController: editProfileViewController)
        self.present(navController, animated: true, completion: nil)
    }
    
    @objc func didTapReportButton(){
        let alert = UIAlertController(title: "Report User", message: "Reporting this user will also block them from contacting you.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Report", style: UIAlertActionStyle.destructive, handler: { (action) in
            self.reportUser()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func reportUser(){
        let reportObject = QBCOCustomObject()
        reportObject.className = "reports"
        reportObject.fields!["reported_user"] = self.profile.userID
        QBRequest.createObject(reportObject, successBlock: { (response, object) -> Void in
            let alert = UIAlertController(title: "User Reported", message: "Thank you for your report", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                if(!self.alreadyMatched){
                    self.didTapDislikeButton()
                }
            }))
            DispatchQueue.main.async(execute: { () -> Void in
                self.present(alert, animated: true, completion: nil)
            })
        }) { (response) -> Void in
            let alert = UIAlertController(title: "Error", message: "An error ocurred, please try again", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
            DispatchQueue.main.async(execute: { () -> Void in
                self.present(alert, animated: true, completion: nil)
            })
        }
    }

}
