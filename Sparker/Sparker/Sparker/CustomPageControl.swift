//
//  CustomPageControl.swift
//  Sparker
//
//  Created by Samuel Harrison on 17/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class CustomPageControl: UIControl {
    
    var currentPage:Int = 0{
        didSet{
            self.updateDots()
        }
    }
    var dots = [UIView]()
    var numberOfPages:Int = 0{
        didSet{
            self.setupDots()
        }
    }
    var currentPageIndicatorTintColor:UIColor = UIColor.black
    var pageIndicatorTintColor:UIColor = UIColor.lightGray
    let dotSize:CGFloat = 8.0
    let dotGap:CGFloat = 12.0
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDots(){
        self.dots.forEach({view in
            view.removeFromSuperview()
        })
        self.dots.removeAll()
        let width = self.frame.size.width
        var xPos:CGFloat = 0
        let dist = dotGap + dotSize
        if(numberOfPages % 2 == 0){
            xPos = width/2 - ((CGFloat(numberOfPages)/2)-1)*(dist) - dist/2
        }else{
            xPos = width/2 - (dist)/2 - ((CGFloat(numberOfPages)/2)-1)*(dist)
        }
        for _ in 0...numberOfPages-1{
            let view = UIView(frame: CGRect(x: xPos, y: self.frame.size.height - dotSize/2, width: dotSize, height: dotSize))
            view.backgroundColor = self.pageIndicatorTintColor
            view.layer.cornerRadius = self.dotSize/2
            self.addSubview(view)
            dots.append(view)
            xPos += dotSize + dotGap
        }
        self.updateDots()
    }
    
    func updateDots(){
        for (i,dot) in dots.enumerated(){
            if(i == currentPage){
                dot.backgroundColor = self.currentPageIndicatorTintColor
            }else{
                dot.backgroundColor = self.pageIndicatorTintColor
            }
        }
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
