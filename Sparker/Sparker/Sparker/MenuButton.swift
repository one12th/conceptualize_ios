//
//  MenuButton.swift
//  Sparker
//
//  Created by Samuel Harrison on 10/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class MenuButton: UIButton {
    
    let customImageView = UIImageView()
    override var isSelected: Bool {
        get {
            return super.isSelected
        }
        set {
            /*if newValue {
                self.customImageView.tintColor = mainColor
                self.customImageView.layer.borderColor = mainColor.CGColor
            }
            else {
                self.customImageView.tintColor = UIColor.whiteColor()
                self.customImageView.layer.borderColor = UIColor.whiteColor().CGColor
            }*/
            super.isSelected = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.customImageView.frame = CGRect(x: 0, y: 0, width: frame.size.height, height: frame.size.height)
        self.customImageView.contentMode = UIViewContentMode.scaleAspectFill
        self.customImageView.clipsToBounds = true
        self.customImageView.layer.cornerRadius = frame.size.height/2
        self.addSubview(self.customImageView)
        self.titleLabel?.frame = CGRect(x: frame.size.height + 20, y: 0, width: frame.size.width - frame.size.height + 16, height: frame.size.height)
        self.setTitleColor(UIColor.white, for: UIControlState())
        //self.setTitleColor(mainColor, forState: UIControlState.Selected)
    }
    
    override func layoutSubviews() {
        self.customImageView.frame = CGRect(x: 0, y: 0, width: self.frame.size.height, height: self.frame.size.height)
        self.customImageView.layer.cornerRadius = self.frame.size.height/2
        self.titleLabel?.frame = CGRect(x: self.frame.size.height + 20, y: 0, width: self.frame.size.width - self.frame.size.height + 16, height: self.frame.size.height)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
