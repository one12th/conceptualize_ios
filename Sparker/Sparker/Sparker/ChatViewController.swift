//
//  ChatViewController.swift
//  Sparker
//
//  Created by Samuel Harrison on 04/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import MBProgressHUD
import FormatterKit
import JDStatusBarNotification

class ChatViewController: UIViewController, ChatServiceDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextViewDelegate{

    var collectionView:UICollectionView!
    var messageContainerBottomConstraint:NSLayoutConstraint!
    let dialog:QBChatDialog!
    let messages = [Date:[QBChatMessage]]()
    let messageFieldContainer = UIView()
    let messageField = UITextView()
    let sendButton = UIButton()
    let lastSeenLabel = UILabel()
    let titleView = UILabel()
    var messagesPage = QBResponsePage()
    
    init(dialog:QBChatDialog){
        self.dialog = dialog
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        self.view.backgroundColor = AppConfig.chatScreenBackgroundColor
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.view.bounds.size.width, height: 44.0)
        //self.collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height - 44 - 64.0), collectionViewLayout: layout)
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.collectionView.backgroundColor = AppConfig.chatScreenBackgroundColor
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(TextMessageCell.self, forCellWithReuseIdentifier: "textMessageCell")
        self.collectionView.bounces = true
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 12.0, right: 0)
        self.view.addSubview(self.collectionView)
        
        //self.messageFieldContainer.frame = CGRect(x: 0, y: self.view.bounds.size.height - 46.5 - 64.0, width: self.view.bounds.size.width, height: 46.5)
        self.messageFieldContainer.backgroundColor = AppConfig.chatScreenNewMessageAreaBackgroundColor
        self.view.addSubview(self.messageFieldContainer)
        //self.messageField.frame = CGRect(x: 8, y: 8.0, width: self.view.bounds.size.width - 74.0, height: 30.5)
        self.messageField.layer.cornerRadius = AppConfig.chatScreenNewMessageFieldCornerRadius
        self.messageField.returnKeyType = UIReturnKeyType.default
        self.messageField.delegate = self
        self.messageField.backgroundColor = AppConfig.chatScreenNewMessageFieldBackgroundColor
        self.messageField.textColor = AppConfig.chatScreenNewMessageFieldTextColor
        self.messageField.font = AppConfig.chatScreenNewMessageFieldFont
        self.messageField.isScrollEnabled = false
        self.messageFieldContainer.addSubview(self.messageField)
        //self.sendButton.frame = CGRect(x: self.view.bounds.size.width - 56.0, y: 8.0, width: 48.0, height: 30.5)
        self.sendButton.setBackgroundColor(AppConfig.chatScreenSendButtonBackgroundColor, forState: UIControlState())
        self.sendButton.setBackgroundColor(AppConfig.lightGray, forState: UIControlState.disabled)
        self.sendButton.layer.cornerRadius = AppConfig.chatScreenSendButtonCornerRadius
        self.sendButton.setTitle("Send", for: UIControlState())
        self.sendButton.setTitleColor(AppConfig.chatScreenSendButtonTextColor, for: UIControlState())
        self.sendButton.titleLabel!.font = AppConfig.chatScreenSendButtonFont
        self.sendButton.addTarget(self, action: #selector(ChatViewController.didTapSend), for: UIControlEvents.touchUpInside)
        self.sendButton.isEnabled = false
        self.messageFieldContainer.addSubview(self.sendButton)
        
        self.messageFieldContainer.translatesAutoresizingMaskIntoConstraints = false
        self.sendButton.translatesAutoresizingMaskIntoConstraints = false
        self.messageField.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraints([
            NSLayoutConstraint(item: self.collectionView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.collectionView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.collectionView, attribute: .top, relatedBy: .equal, toItem: topLayoutGuide, attribute: .bottom, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.collectionView, attribute: .bottom, relatedBy: .equal, toItem: self.messageFieldContainer, attribute: .top, multiplier: 1.0, constant: 0),
        
            NSLayoutConstraint(item: self.messageFieldContainer, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.messageFieldContainer, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.messageFieldContainer, attribute: .bottom, relatedBy: .equal, toItem: bottomLayoutGuide, attribute: .top, multiplier: 1.0, constant: 0),
            
            NSLayoutConstraint(item: self.sendButton, attribute: .trailing, relatedBy: .equal, toItem: self.messageFieldContainer, attribute: .trailing, multiplier: 1.0, constant: -12.0),
            NSLayoutConstraint(item: self.sendButton, attribute: .top, relatedBy: .equal, toItem: self.messageFieldContainer, attribute: .top, multiplier: 1.0, constant: 12.0),
            NSLayoutConstraint(item: self.sendButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 30.5),
            NSLayoutConstraint(item: self.sendButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 48.0),
            
            NSLayoutConstraint(item: self.messageField, attribute: .top, relatedBy: .equal, toItem: self.messageFieldContainer, attribute: .top, multiplier: 1.0, constant: 12.0),
            NSLayoutConstraint(item: self.messageField, attribute: .bottom, relatedBy: .equal, toItem: self.messageFieldContainer, attribute: .bottom, multiplier: 1.0, constant: -12.0),
            NSLayoutConstraint(item: self.messageField, attribute: .leading, relatedBy: .equal, toItem: self.messageFieldContainer, attribute: .leading, multiplier: 1.0, constant: 12.0),
            
            NSLayoutConstraint(item: self.messageField, attribute: .trailing, relatedBy: .equal, toItem: self.sendButton, attribute: .leading, multiplier: 1.0, constant: -12.0),
            
            NSLayoutConstraint(item: self.messageField, attribute: .height, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .height, multiplier: 1.0, constant: 30.5)
        ])
        
        self.lastSeenLabel.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 18)
        self.lastSeenLabel.backgroundColor = AppConfig.chatScreenConnectingMessageBackgroundColor
        self.lastSeenLabel.text = "Connecting..."
        self.lastSeenLabel.textAlignment = NSTextAlignment.center
        self.lastSeenLabel.textColor = AppConfig.chatScreenConnectingMessageTextColor
        self.lastSeenLabel.font = AppConfig.chatScreenConnectingMessageFont
        self.view.addSubview(self.lastSeenLabel)
        
        self.setupProfileImage()
        
        if let profile = ChatService.sharedInstance.getDialogOpponent(self.dialog){
            self.navigationItem.title = profile.name
        }
        
        if(self.dialog.data != nil){
            if let seen = self.dialog.data!["\(Utility.sharedInstance.currentUserProfile!.userID)_seen"] as? Bool{
                if(seen == false){
                    self.dialog.data!["\(Utility.sharedInstance.currentUserProfile!.userID)_seen"] = true
                    QBRequest.update(self.dialog, successBlock: { (response, dialog) -> Void in
                        if(dialog != nil){
                            ChatService.sharedInstance.updateOrInsertDialog(dialog!)
                        }
                        }, errorBlock: { (response) -> Void in
                            print("Error marking dialog as seen \(String(describing: response.error))")
                    })
                }
            }
        }
        self.syncMessages(nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ChatService.sharedInstance.delegate = self
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "com.samuelharrison.Sparker.chatDidDisconnect"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.chatDidDisconnect), name: NSNotification.Name(rawValue: "com.samuelharrison.Sparker.chatDidDisconnect"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "com.samuelharrison.Sparker.chatDidReconnect"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(QBChatDelegate.chatDidReconnect), name: NSNotification.Name(rawValue: "com.samuelharrison.Sparker.chatDidReconnect"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if(!QBChat.instance().isConnected){
            self.showConnectingHeader()
        }else{
            self.hideConnectingHeader()
            self.syncMessages(nil)
        }
        
        self.scrollToBottom(false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ChatService.sharedInstance.delegate = nil
    }
    
    func setupProfileImage(){
        let profileImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 34.0, height: 34.0))
        profileImageView.contentMode = UIViewContentMode.scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.layer.cornerRadius = 17.0
        profileImageView.layer.borderWidth = AppConfig.chatScreenNavigationBarProfileImageBorderWidth
        profileImageView.layer.borderColor = AppConfig.chatScreenNavigationBarProfileImageBorderColor.cgColor
        let profileImageButton = UIButton(frame: CGRect(x: 0, y: 0, width: 34.0, height: 34.0))
        profileImageButton.addSubview(profileImageView)
        profileImageButton.addTarget(self, action: #selector(ChatViewController.didTapProfileButton), for: UIControlEvents.touchUpInside)
        profileImageView.image = UIImage(named: "PlaceholderImage")
        if let profile = ChatService.sharedInstance.getDialogOpponent(self.dialog){
            if profile.images.count > 0{
                if let urlString = QBCBlob.privateUrl(forID: profile.images[0]){
                    if let url = URL(string: urlString){
                        profileImageView.setImageWith(url, placeholderImage: UIImage(named: "PlaceholderImage"))
                    }
                }
            }
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: profileImageButton)
    }
    
    @objc func didTapProfileButton(){
        if let profile = ChatService.sharedInstance.getDialogOpponent(self.dialog){
            let profileViewController = ProfileViewController(profile: profile, alreadyMatched: true)
            self.navigationController?.pushViewController(profileViewController, animated: true)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ChatService.sharedInstance.messagesForDialogID(self.dialog.id!).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "textMessageCell", for: indexPath) as! TextMessageCell
        cell.configure(self.dialog, message: ChatService.sharedInstance.messagesForDialogID(self.dialog.id!)[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let message = ChatService.sharedInstance.messagesForDialogID(self.dialog.id!)[indexPath.row]
        if(message.text == nil){
            return CGSize(width: 0, height: 0)
        }
        let cellHeight = (message.text! as NSString).boundingRect(with: CGSize(width: self.view.bounds.size.width*0.8 - 48.0, height: CGFloat.greatestFiniteMagnitude), options: [NSStringDrawingOptions.truncatesLastVisibleLine, NSStringDrawingOptions.usesLineFragmentOrigin], attributes: [NSAttributedStringKey.font:AppConfig.chatScreenMessageBubbleFont], context: nil).size.height
        return CGSize(width: self.view.bounds.size.width, height: cellHeight + 24)
    }
    
    /*func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        let menuController = UIMenuController.sharedMenuController()
        let cell = self.collectionView(collectionView, cellForItemAtIndexPath: indexPath)
        menuController.setTargetRect(cell.frame, inView: cell.superview!)
        return true
    }
    
    func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        if (action == #selector(copy(_:))){
            return true
        }
        return false
    }
    
    func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
        if (action == #selector(copy(_:))){
            let text = ChatService.sharedInstance.messagesForDialogID(self.dialog.ID!)[indexPath.row].text
            if(text != nil && text != ""){
                let pasteBoard = UIPasteboard.generalPasteboard()
                pasteBoard.setValue(text!, forPasteboardType: kUTTypeUTF8PlainText as String)
            }
        }
    }
*/
    
    func syncMessages(_ completion:(()->Void)?){
        _ = ChatService.sharedInstance.messagesForDialogID(self.dialog.id!)
        var params = [String: String]()
        /*if(messages.count > 0){
            params["date_sent[gte]"] = "\(messages.last!.dateSent!.timeIntervalSince1970)"
        }*/
        params["sort_desc"] = "date_sent"
        QBRequest.messages(withDialogID: self.dialog.id!, extendedRequest: params, for: self.messagesPage, successBlock: { (response, messages, page) -> Void in
            if(messages != nil){
                ChatService.sharedInstance.addMessages(messages!)
                self.collectionView.reloadData()
                self.scrollToBottom(false)
            }
            completion?()
            QBRequest.markMessages(asRead: [], dialogID: self.dialog.id!, successBlock: { (response) -> Void in
                //
                self.dialog.unreadMessagesCount = 0
                ChatService.sharedInstance.updateOrInsertDialog(self.dialog)
                }, errorBlock: { (response) -> Void in
                    print("Error marking messages as read: \(String(describing: response.error))")
            })
            }) { (response) -> Void in
                print("Error loading messages: \(String(describing: response.error))")
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let maxHeight:CGFloat = 250.0
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        let newHeight = min(max(newSize.height,30.5), maxHeight)
        
        var frame = self.messageFieldContainer.frame
        frame.origin.y -= newHeight - textView.frame.size.height
        frame.size.height += newHeight - textView.frame.size.height
        self.messageFieldContainer.frame = frame
        
        frame = textView.frame
        frame.size.height = newHeight
        textView.frame = frame
        
        frame = self.collectionView.frame
        frame.size.height -= newHeight - textView.frame.size.height
        self.collectionView.frame = frame
        
        self.configureSendButton()
        
        self.scrollToBottom(false)
    }
    
    @objc func didTapSend(){
        if(self.messageField.text == ""){
            return
        }
        ChatService.sharedInstance.sendMessage(self.messageField.text, dialog: self.dialog) { () -> Void in
            print("Error sending message")
            let alert = UIAlertController(title: "Error", message: "There was an error sending your message, please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        self.messageField.text = ""
        //if(self.messageField.textInputMode != nil){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.messageField.resignFirstResponder()
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.messageField.becomeFirstResponder()
        //}
        self.textViewDidChange(self.messageField)
        self.scrollToBottom(true)
    }
    
    func chatDidReceiveMessage(_ message: QBChatMessage) {
        if(message.dialogID == self.dialog.id){
            if(message.id != nil && self.dialog.id != nil){
                QBRequest.markMessages(asRead: [message.id!], dialogID: self.dialog.id!, successBlock: nil, errorBlock: nil)
                self.dialog.unreadMessagesCount = 0
                ChatService.sharedInstance.updateOrInsertDialog(self.dialog)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "com.samuelharrison.utility.dialogsUpdatedNotification"), object: nil)
            }
            self.collectionView.reloadData()
        }else{
            JDStatusBarNotification.show(withStatus: "New Message Received", dismissAfter: 3.5, styleName: "newMessageStyle")
        }
        self.scrollToBottom(true)
    }
    
    /*func chatDidReceivePresence(userID: UInt, isOnline: Bool) {
        if let opponent = ChatService.sharedInstance.getDialogOpponent(self.dialog){
            if(opponent.userID == userID){
                if(isOnline){
                    self.lastSeenLabel.text = "Online"
                }else{
                    let formatter = TTTTimeIntervalFormatter()
                    self.lastSeenLabel.text = "Active "+formatter.stringForTimeInterval(opponent.updatedAt - NSDate().timeIntervalSince1970)
                }
            }
        }
    }*/
    
    func scrollToBottom(_ animated:Bool){
        if(self.collectionView.numberOfItems(inSection: 0) > 0){
            let lastSection = self.collectionView.numberOfSections - 1
            self.collectionView.scrollToItem(at: IndexPath(row: self.collectionView.numberOfItems(inSection: lastSection)-1, section: lastSection), at: UICollectionViewScrollPosition.bottom, animated: animated)
        }
    }
    
    /*func keyboardWillShow(notification: NSNotification){
        let kbHeight = notification.userInfo![UIKeyboardFrameEndUserInfoKey]!.CGRectValue.size.height
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: 12 + kbHeight, right: 0);
        UIView.animateWithDuration(0.3, animations: {
            self.messageFieldContainer.transform = CGAffineTransformMakeTranslation(0, -kbHeight)
            self.collectionView.contentInset = insets;
        }) { (completed) in
            self.scrollToBottom(true);
        }
    }
    
    func keyboardWillHide(notification: NSNotification){
        UIView.animateWithDuration(0.3, animations: {
            self.messageFieldContainer.transform = CGAffineTransformIdentity
            self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 12, right: 0);
        })
    }*/
    
    @objc func keyboardWillShow(_ notification: Notification){
        let kbHeight = (notification.userInfo![UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size.height - bottomLayoutGuide.length
        let duration = (notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0.3
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: 16.0 + kbHeight, right: 0);
        let offsetChange = max(0,insets.bottom - self.collectionView.contentInset.bottom)
        self.collectionView.contentInset = insets;
        UIView.animate(withDuration: duration, animations: {
            self.messageFieldContainer.transform = CGAffineTransform(translationX: 0, y: -kbHeight)
            self.collectionView.contentOffset = CGPoint(x: 0, y: self.collectionView.contentOffset.y + offsetChange)
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification){
        let duration = (notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0.3
        UIView.animate(withDuration: duration, animations: {
            self.messageFieldContainer.transform = CGAffineTransform.identity
            self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16.0, right: 0);
        })
    }
    
    func configureSendButton(){
        if(QBChat.instance().isConnected && self.messageField.text != ""){
            self.sendButton.isEnabled = true
        }else{
            self.sendButton.isEnabled = false
        }
    }
    
    func showConnectingHeader(){
        self.configureSendButton()
        self.lastSeenLabel.isHidden = false
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityIndicator.startAnimating()
        self.navigationItem.titleView = activityIndicator
    }
    
    func hideConnectingHeader(){
        self.configureSendButton()
        self.lastSeenLabel.isHidden = true
        self.navigationItem.titleView = nil
    }    
    
    @objc func chatDidDisconnect(){
        self.showConnectingHeader()
    }
    
    func chatDidReconnect(){
        self.syncMessages { () -> Void in
            self.hideConnectingHeader()
        }
    }
}
