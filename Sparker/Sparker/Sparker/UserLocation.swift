//
//  UserLocation.swift
//  Sparker
//
//  Created by Samuel Harrison on 15/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit

class UserLocation: NSObject {

    let name:String
    let latitude:Double
    let longitude:Double
    
    init(name:String, coordinate:CLLocationCoordinate2D){
        self.name = name
        self.latitude = coordinate.latitude
        self.longitude = coordinate.longitude
        super.init()
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.latitude, forKey: "latitude")
        aCoder.encode(self.longitude, forKey: "longitude")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.latitude = aDecoder.decodeObject(forKey: "latitude") as? Double ?? 0
        self.longitude = aDecoder.decodeObject(forKey: "longitude") as? Double ?? 0
    }
}
