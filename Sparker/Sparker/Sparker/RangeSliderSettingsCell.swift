//
//  RangeSliderSettingsCell.swift
//  Sparker
//
//  Created by Samuel Harrison on 08/02/2016.
//  Copyright © 2016 Samuel Harrison. All rights reserved.
//

import UIKit
import NMRangeSlider

class RangeSliderSettingsCell: UITableViewCell {

    var slider:NMRangeSlider!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.slider = NMRangeSlider(frame: CGRect(x: 21, y: 0, width: self.bounds.size.width - 42, height: self.bounds.size.height))
        self.contentView.addSubview(self.slider)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.slider.frame = CGRect(x: 21, y: 0, width: self.bounds.size.width - 42, height: self.bounds.size.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
